'use strict'

import LinkContract from './Contracts/Link';


let linkContract;
let abi;
let bytecode;
let gasEstimate;
let web3;

const links 	= [];

const init 	= ( ) => {
	web3 				= EtherVoxService.web3;
	linkContract 		= EtherVoxService.ContractsSource.Link.contract;
	abi 				= EtherVoxService.ContractsSource.Link.abi;
	bytecode 			= EtherVoxService.ContractsSource.Link.bytecode;
	gasEstimate 		= EtherVoxService.ContractsSource.Link.gasEstimate;
 
	initFilters();
};

const initFilters = () => {
	const startBlock              	= EtherVoxService.startBlockNumberContracts;
    const sampleContract 			= '0x0000000000000000000000000000000000000000';
    const sampleContractInstance 	= web3.eth.contract(abi).at(sampleContract);

	//NewLink( address ownerContract, address owner, address farparty, bytes32 ownerProduct, string comments);
    const newLinkTopic           = web3.sha3('NewLink(address,address,address,bytes32,string)');
    const newLinkFilter 		 = web3.eth.filter({fromBlock: web3.eth.blockNumber, toBlock: 'latest', topics:[ newLinkTopic ] });
    const getLinkFilter 		 = web3.eth.filter({fromBlock: startBlock, toBlock: 'latest', topics:[ newLinkTopic ] });

    getLinkFilter.get( (error, results) => {
		for (let result of results){
			const event = sampleContractInstance.NewLink().formatter(result);
			const link = parseLink(event);
		};
    });

    newLinkFilter.watch( (error, result) => {
    	if (result){
    		const event = sampleContractInstance.NewLink().formatter(result);
    		const link = parseLink(event);
    	};
    });

	/*setTimeout(()=>{
		newLink('0x1c433562b711f4fec837e0d6b9b2c4143399bf51', '0x424c44e198e54ab09550bbc333f94a1ab4437fab', '0xe79fd990c9a83fb75afcdc4e670832160cf99789', 'NA', "Link3");
	}, 4000)*/
   	

};

const parseLink = (event) => {
	const contract 		= linkContract.at(event.address);
	const link 			= new LinkContract(event, contract);
	links.push ( link );

	return link;
};

// address ownerContract, address owner, address farparty, bytes32 ownerProduct, string comments
const newLink = ( owner, ownerContract, farparty, ownerProduct, comments ) => new Promise((resolve, reject) => {
	EtherVoxService.findAndUnlock(null, owner)
		.then( ( resp ) =>  {
			ownerProduct		= web3.fromAscii(ownerProduct, 32);
			var newContract 	= linkContract.new( 
				ownerContract, farparty, ownerProduct, comments,
				{ 
					from 		: owner, 
 					data    	: bytecode,
					gas 		: gasEstimate,
				},
				(err, contr) => {
					if (err)
						return reject(err);
					return resolve(contr);
				});

		})
		.catch( (error) => {
			sails.log.error(error);
		})
});

const agreeLink = (link, farpartyContract, ownerProduct, comments ) => new Promise((resolve, reject) => {
	
});

const getLinks = ( address ) => {
	const arr = _.where(links, {owner: address}).concat( _.where(links, {farparty: address}) );
	sails.log('getLinks arr', arr);
	return arr;
};

module.exports.init  		=  	init;
module.exports.getLinks 	= 	getLinks;
module.exports.newLink 		= 	newLink;