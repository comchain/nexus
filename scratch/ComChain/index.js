//ComChain.js
'use strict'

import ComChainContract from './Contracts/ComChain';

let comChainContract;
let abi;
let bytecode;
let gasEstimate;
let web3;

const contracts 	= [];

const init 	= ( ) => {
	web3 				= EtherVoxService.web3;
	comChainContract 	= EtherVoxService.ContractsSource.ComChain.contract;
	abi 				= EtherVoxService.ContractsSource.ComChain.abi;
	bytecode 			= EtherVoxService.ContractsSource.ComChain.bytecode;
	gasEstimate 		= EtherVoxService.ContractsSource.ComChain.gasEstimate;

	initFilters();
};

const initFilters = () => {

	const startBlock              	= EtherVoxService.startBlockNumberContracts;

	sails.log('comchain initFilters', startBlock);

    const sampleContract 			= '0x0000000000000000000000000000000000000000';
    const sampleContractInstance 	= web3.eth.contract(abi).at(sampleContract);

    const newComChainTopic           = web3.sha3('NewComChain(address)');
    const newComChainFilter 		 = web3.eth.filter({fromBlock: web3.eth.blockNumber, toBlock: 'latest', topics:[ newComChainTopic ] });
    const getComChainFilter 		 = web3.eth.filter({fromBlock: startBlock, toBlock: 'latest', topics:[ newComChainTopic ] });

    getComChainFilter.get( (error, results) => {
		for (let result of results){
			const event = sampleContractInstance.NewComChain().formatter(result);
			const cch = parseComChain(event);
		};
    });

    newComChainFilter.watch( (error, result) => {
    	if (result){
    		const event = sampleContractInstance.NewComChain().formatter(result);
    		const cch = parseComChain(event);
    	};
    });

    //0x1c433562b711f4fec837e0d6b9b2c4143399bf51 - pkownacki
    //0x0a62b745d172cf8ed4d1e4edb751ab8cd00987b4 - pinczo 
    /*setTimeout(() => {
    	newComChain( '0xe79fd990c9a83fb75afcdc4e670832160cf99789', 'Interest1'); // Channel 1, AudioBridge 0x0000
    	newComChain( '0x1c433562b711f4fec837e0d6b9b2c4143399bf51', 'Interest2'); // Channel 2, AudioBridge 0x0000	
    }, 6000);*/
};

const parseComChain = ( event ) => {
	/*const contract 		= comChainContract.at(event.address);
	if (contract.getOwner() === '0x') // << Means contract is killed
		return;
	*/
	const comChain 		= new ComChainContract(event, null);
	contracts.push( comChain );

	sails.emit('etherVox:contract:new:comChain', comChain );
	return comChain;
};

//address _owner, bytes32 _identifier
const newComChain = ( _creator, _owner, _identifier ) => new Promise((resolve, reject) => {
	//sails.log("NEW ComChain", _creator, _owner, _identifier);
	//return;
	//debug: NEW ComChain 0xa256bf764fbf69954ab6b9349a86f928d790cbda 0xa256bf764fbf69954ab6b9349a86f928d790cbda 59ba5a3f1c8f15f9061f32c2
	//debug: NEW ComChain 0xb40f9a9323c7f1525565ede0a266444abec759ea 0xb40f9a9323c7f1525565ede0a266444abec759ea 59ba5be000835c8f077c7d5e


	EtherVoxService.findAndUnlock(null, _creator)
		.then( ( resp ) =>  {
			_identifier	= web3.fromAscii(_identifier, 32);
			var newContract 	= comChainContract.new( 
				_owner, _identifier, 
				{ 
					from 	: _creator, 
 					data    : bytecode,
					gas 	: gasEstimate 
				},
				(err, contr) => {
					sails.log("NEW COM ComChain RESP", err);
					if (err)
						return reject(err);
					return resolve(contr);
				});

		})
		.catch( (error) => {
			sails.log.error(error);
		})
});


const getContracts = ( address, owner ) => {
	const query 		= {};
	if (address)
		query.address = address
	else if (owner)
		query.owner = owner;
	else 
		return contracts;

	const arr = _.where(contracts, query);
	for (let i = 0; i < arr.length; i++){
		arr[i].getData();
	};
	return arr;
};

module.exports.init  			= init;
module.exports.newComChain 		= newComChain;
module.exports.getContracts 	= getContracts;
