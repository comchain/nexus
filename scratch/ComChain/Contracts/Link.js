//Link.js
class LinkContract {
	// _ownerContract, owner, _farparty, _product, _comments
    constructor(_event, _contract) {

    	this.events                     = [];
        this.contract                   = _contract;       

        this.address                    = _event.address;
        this.owner                      = _event.args.owner;
        this.ownerContract 				= _event.args.ownerContract;
        this.farparty                   = _event.args.farparty;

        this.comments                   = _event.args.comments;

        this.agreed 					= _contract.agreed();
        this.farpartyContract 			= _contract.farpartyContract();

        this.transactionHash            = _event.transactionHash;
        this.blockNumber                = _event.blockNumber;

        this.events.push(_event); 

        /*sails.log('\nLINK');
        sails.log(  'this.address',                this.address);
        sails.log(  'this.owner',                  this.owner);
        sails.log(  'this.ownerContract',          this.ownerContract);
        sails.log(  'this.farparty',               this.farparty);*/


        //sails.log( this.farparty ,'this.farparty this.farparty  this.farparty ');
        //this.agree( this.farparty, '0x05e6720a12e729b2b259ec06f88a09d3a403d463', 'NA', 'ok');

        const agreeTopic                = EtherVoxService.web3.sha3('LinkAgree(address,bytes32,string)');
        const agreeFilter               = EtherVoxService.web3.eth.filter({fromBlock: _event.blockNumber, toBlock: 'latest', address: [_event.address], topics:[ agreeTopic ] });
      
        const sampleContract            = '0x0000000000000000000000000000000000000000';
        const sampleContractInstance    = EtherVoxService.web3.eth.contract(EtherVoxService.ContractsSource.Link.abi).at(sampleContract);

        agreeFilter.watch((error, result) => {
            if (result){
                var event          = sampleContractInstance.LinkAgree().formatter(result);
                this.agree = true;
                this.farpartyContract = event.args.farpartyContract;
            };
        });
    };
   
    agree( accountAddress, farpartyContract, product, comments ){
    	 return new Promise( (resolve, reject) => {
            EtherVoxService.findAndUnlock(null, accountAddress)
	            .then( ( unlockResp ) => {
					this.contract.agree(
							farpartyContract,
							product,
							comments, 
						{ from: accountAddress, gas: 286010 },
						(err,resp) => {
							sails.log('resp', resp);
							sails.log('err', err);
						});
	            });
	    });
    	
    };

    toJSON() {
        return {
            'address'           : this.address,
            'owner'             : this.owner,
            'ownerContract'     : this.ownerContract,

            'agreed'            : this.agreed,
            'comments'          : this.comments,

            'farparty'          : this.farparty,
            'farpartyContract'  : this.farpartyContract,

            'transactionHash'   : this.transactionHash,
            'blockNumber'       : this.blockNumber
        };
    }
}

export default LinkContract;