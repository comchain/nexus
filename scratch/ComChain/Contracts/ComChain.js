//ComChain.js

class ComChainContract {
    constructor(_event, _contract) {

        this.events                     = [_event];
        this.address                    = _event.address;
        
        if (_contract){
            this.contract                   = _contract;
            this.owner                      = _contract.getOwner();//_event.args.owner;
            this.identifier                 = EtherVoxService.Utils.getClearString( _contract.getIdentifier(), true );
        } else  {
            this.owner                      = _event.args.owner;
        }

        this.transactionHash                = _event.transactionHash;
        this.blockNumber                    = _event.blockNumber;

        this.properties                 = {};
        this.meta                       = {};
        this.links                      = [];

        this.killed                     = false;

    };

    getData() {
        if (!this.contract)
            this.contract = EtherVoxService.ContractsSource.ComChain.contract.at(this.address);

        this.owner      = this.contract.getOwner(); 
        if (this.owner === '0x'){
            this.killed = true;
            sails.log("KILLED");
            return
        };


        this.identifier = EtherVoxService.Utils.getClearString( this.contract.getIdentifier(), true );
        this.links      =  this.contract.getLinks();

        let keys        =  this.contract.getPropertiesKeys();

        for (let _key of keys){
            let key =  EtherVoxService.Utils.getClearString(_key, true);
            this.properties[key] = this.contract.getProperty(_key);
        };

        keys            =  this.contract.getMetaDataKeys();

        for (let _key of keys){
            let md = this.contract.pullMetaData(_key);
            this.meta[_key] = {
                hash        : md[0],
                location    : md[1]
            };
        }; 
    };

    addProperty(_key, _val) {   
        if (!this.contract)
            this.getData();


    	return new Promise( (resolve, reject) => {
            EtherVoxService.findAndUnlock(null, this.owner)
            .then( (unlockResp ) => {
        		_key = EtherVoxService.web3.fromAscii(_key);
        		this.contract.addProperty(
    	    		 _key, _val,
    	    		 { from: this.owner, gas: 63366 + 85918 },
    	    		 (err, resp) => {
                       // console.log('addProperty err, resp', err, resp );
    	    			if (err)
    	    				return reject(err);
    	    			return resolve( resp );
    	    		});
            })
            .catch( ( error ) => {
                reject(error);
            })
    	});
    };

    addLink( _address ){
        if (!this.contract)
            this.getData();
        return new Promise( (resolve, reject) => {
            EtherVoxService.findAndUnlock(null,  this.owner)
            .then( (unlockResp ) => {
                this.contract.addLink(
                    _address,
                     { from: this.owner, gas: 216010 },
                     (err, resp) => {
                       // console.log('addProperty err, resp', err, resp);
                        if (err)
                            return reject(err);
                        return resolve( resp );
                    });

            })
            .catch( ( error ) => {
                reject(error);
            })
        });
    };

    removeLink( _address ){
        if (!this.contract)
            this.getData();
        return new Promise( (resolve, reject) => {
            EtherVoxService.findAndUnlock(null,  this.owner)
            .then( (unlockResp ) => {
                this.contract.removeLink(
                    _address,
                     { from: this.owner, gas: 216010 },
                     (err, resp) => {
                       // console.log('addProperty err, resp', err, resp);
                        if (err)
                            return reject(err);
                        return resolve( resp );
                    });

            })
            .catch( ( error ) => {
                reject(error);
            })
        });
    };

    removeLinks(){
        if (!this.contract)
            this.getData();

        return new Promise( (resolve, reject) => {
            sails.log('Remove Links');
            EtherVoxService.findAndUnlock(null,  this.owner)
            .then( (unlockResp ) => {
                for (let i =0; i <  this.links.length; i++){
                    sails.log( 'this.links[i]',  this.links[i]);
                    this.contract.removeLink(
                        this.links[i],
                         { from: this.owner, gas: 216010 },
                         (err, resp) => {
                            if (err)
                                return reject(err);
                            return resolve( resp );
                        });
                };
                resolve('ok')
            })
            .catch( ( error ) => {
                reject(error);
            })
        });
    };


    pushMetaData( _from, _key, _hash, _location ) {
 
    if (!this.contract)
            this.getData();

        if (!_from)
            _from = this.owner;

    	//pushMetaData
        return new Promise( (resolve, reject) => {
            EtherVoxService.findAndUnlock(null, _from)
             .then( (unlockResp ) => {
                _key = EtherVoxService.web3.fromAscii(_key);
            	this.contract.pushMetaData( _key, _hash, _location,
                    { from: _from, gas: 226010 },
                    (err, resp) => {
                        sails.log('pushMetaData RESP:');
                        sails.log('err', err);
                        sails.log('resp', resp);
                    });
            });
        });
    };

    pullMetaData() {
        if (!this.contract)
            this.getData();
    	//pullMetaData
    	this.contract.addProperty( );
    };

    kill() {
        if (!this.contract)
            this.getData();
        return new Promise( (resolve, reject) => {
            EtherVoxService.findAndUnlock(null, this.owner)
            .then( (unlockResp ) => {
                this.contract.kill(
                     { from: this.owner, gas: 216010 },
                     (err, resp) => {
                        if (err)
                            return reject(err);
                        return resolve( resp );
                    });

            })
            .catch( ( error ) => {
                reject(error);
            })
        });
    };
};

export default ComChainContract;