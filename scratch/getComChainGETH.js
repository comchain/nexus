// Make it available for all methods - it will be helpfull 
const getClearString  = (value, toAscii) => {

    if (toAscii)
        value =  web3.toAscii(value);

    let str = "";
    for (let i = 0; i < value.length; i++){
       if (value.charCodeAt(i) > 0)
        str += value[i];
    };

    return str;

};

exports.createComChain = function(args, res, next) {
  /**
   * Get comchain contract by address
   * 
   *
   * body address
   * returns ComChain
   **/
  console.log('args=' + JSON.stringify(args));
  console.log('args address=' + args.body.value.address);
 
  const comchainContract  = web3.eth.contract(comchainAbi);
  const addrsss           = args.body.value.address;
  const comchain          = comchainContract.at( addrsss );

  // We can add here condition if contract exist, for example we can getOwner and check if its not empty 
  
  comchain.console.log('comchain:', comchain);
  
  let response = {};

  let owner;
  let identifier;
  let properties = {};
  let mifid2;

  // Get Owner
  owner = comchain.getOwner();

  // Get Identifier and conver to string
  identifier = getClearString( comchain.getIdentifier(), true );
  
  // Get All Properties, convert to string and build an key/value object
  for (let key of comchain.getPropertiesKeys()){
    let keyString         = getClearString(key, true);
    properties[keyString] = comchain.getProperty(key);
  };

  // Get MiFiD2 Address
  mifid2 = comchain.getMiFiD2();


  response['application/json'] = {
      "code"        : 200,
      "address"     : address,
      "block"       : contract._eth.blockNumber,
      "transaction" : contract.transactionHash,

      "owner"       : owner,
      "identifier"  : identifier,
      "properties"  : properties,
      "mifid2"      : mifid2
  };


  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(response[Object.keys(response)[0]] || {}, null, 2));

}