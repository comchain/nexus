> var registryAbi = [{"constant":true,"inputs":[],"name":"getName","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"to","type":"address"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_contract","type":"address"}],"name":"addContract","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getInitiator","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getOwner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getContracts","outputs":[{"name":"","type":"address[]"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_name","type":"bytes32"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"name","type":"bytes32"}],"name":"NewRegistry","type":"event"},{"anonymous":false,"inputs":[],"name":"Killed","type":"event"}];
undefined
> var registryBytes = '0x6060604052341561000f57600080fd5b6040516020806106a583398101604052808051906020019091905050326000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555080600381600019169055507fe87ba6b36eeb279210d01af988c6f368f744e823eddad1b1a2688055d718ceb060035460405180826000191660001916815260200191505060405180910390a1506105de806100c76000396000f300606060405260043610610083576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806317d7de7c146100885780631a695230146100b957806341c0e1b51461010a5780635f539d691461011f57806384de1ea514610158578063893d20e8146101ad578063c3a2a93a14610202575b600080fd5b341561009357600080fd5b61009b61026c565b60405180826000191660001916815260200191505060405180910390f35b34156100c457600080fd5b6100f0600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610276565b604051808215151515815260200191505060405180910390f35b341561011557600080fd5b61011d61036f565b005b341561012a57600080fd5b610156600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610400565b005b341561016357600080fd5b61016b610466565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34156101b857600080fd5b6101c061048f565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561020d57600080fd5b6102156104b9565b6040518080602001828103825283818151815260200191508051906020019060200280838360005b8381101561025857808201518184015260208101905061023d565b505050509050019250505060405180910390f35b6000600354905090565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163273ffffffffffffffffffffffffffffffffffffffff1614806103205750600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163273ffffffffffffffffffffffffffffffffffffffff16145b1561036a57816000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550600190505b919050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163273ffffffffffffffffffffffffffffffffffffffff1614156103fe576000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16ff5b565b60048054806001018281610414919061054d565b9160005260206000209001600083909190916101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505050565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b6000600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b6104c1610579565b600480548060200260200160405190810160405280929190818152602001828054801561054357602002820191906000526020600020905b8160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190600101908083116104f9575b5050505050905090565b81548183558181151161057457818360005260206000209182019101610573919061058d565b5b505050565b602060405190810160405280600081525090565b6105af91905b808211156105ab576000816000905550600101610593565b5090565b905600a165627a7a72305820ecf1118a60e9a5de561726c5e00aad4d31cbccca0b514699fa813d3409f234f20029';
undefined
> var registryContract = web3.eth.contract(registryAbi);
undefined
> var registry = registryContract.new("ComChainRegistry", {from: '0x2e0b8fea9376d8e72784c8ff266452863ab7be10',data: registryBytes, gas: '4700000'});
undefined
> registry
{
  abi: [{
      constant: true,
      inputs: [],
      name: "getName",
      outputs: [{...}],
      payable: false,
      stateMutability: "view",
      type: "function"
  }, {
      constant: false,
      inputs: [{...}],
      name: "transfer",
      outputs: [{...}],
      payable: false,
      stateMutability: "nonpayable",
      type: "function"
  }, {
      constant: false,
      inputs: [],
      name: "kill",
      outputs: [],
      payable: false,
      stateMutability: "nonpayable",
      type: "function"
  }, {
      constant: false,
      inputs: [{...}],
      name: "addContract",
      outputs: [],
      payable: false,
      stateMutability: "nonpayable",
      type: "function"
  }, {
      constant: true,
      inputs: [],
      name: "getInitiator",
      outputs: [{...}],
      payable: false,
      stateMutability: "view",
      type: "function"
  }, {
      constant: true,
      inputs: [],
      name: "getOwner",
      outputs: [{...}],
      payable: false,
      stateMutability: "view",
      type: "function"
  }, {
      constant: true,
      inputs: [],
      name: "getContracts",
      outputs: [{...}],
      payable: false,
      stateMutability: "view",
      type: "function"
  }, {
      inputs: [{...}],
      payable: false,
      stateMutability: "nonpayable",
      type: "constructor"
  }, {
      anonymous: false,
      inputs: [{...}],
      name: "NewRegistry",
      type: "event"
  }, {
      anonymous: false,
      inputs: [],
      name: "Killed",
      type: "event"
  }],
  address: "0xbf6c180a657f92a8fe5e815b97b9499bb4eb2602",
  transactionHash: "0x4738d2877ff26ef1198502ecc78cfe3f6c9f0e7be806c4b2a98f528bc8df75ef",
  Killed: function(),
  NewRegistry: function(),
  addContract: function(),
  allEvents: function(),
  getContracts: function(),
  getInitiator: function(),
  getName: function(),
  getOwner: function(),
  kill: function(),
  transfer: function()
}
> registry1.getName();
"0x4d69666964325265676973747279000000000000000000000000000000000000"
> web3.fromAscii("0x4d69666964325265676973747279000000000000000000000000000000000000");
"0x307834643639363636393634333235323635363736393733373437323739303030303030303030303030303030303030303030303030303030303030303030303030"
> web3.toAscii("0x4d69666964325265676973747279000000000000000000000000000000000000");
"Mifid2Registry\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
> web3.toAscii("0x4d69666964325265676973747279000000000000000000000000000000000000");
"Mifid2Registry\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
> registry.getName();
"0x436f6d436861696e526567697374727900000000000000000000000000000000"
> web3.toAscii("0x436f6d436861696e526567697374727900000000000000000000000000000000");
"ComChainRegistry\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
> registry.getContracts();
["0xe93bcaeb23f3d166051b4d1022b0318913a38fa0"]
> registry1.getContracts();
["0x0e60bf6c3e93e56d789f325828fe93d2753d5ef7"]
> var comchainRegistry = registryContract.at("0xbf6c180a657f92a8fe5e815b97b9499bb4eb2602");
undefined
> comchainRegistry.getContracts();
["0x36b7817479b6eb8fc45460900b9d6f2281e71e79", "0x5e236f5dce2564f57a7f4cc6a4a5d3c929eaa029", "0x36d561c60f622cf5282cf07569737b2744b56714"]

> community.addGroup(web3.fromAscii("TestGroup1"), 0, {from:'0x2e0b8fea9376d8e72784c8ff266452863ab7be10', data: communityBytes, gas: '4700000'});
"0xd589643df027980fd7460c8f5f0db2e6049589fcb51ef6615182f4650c0d6bba"