var SwaggerComChain = require('swagger_com_chain');

var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure OAuth2 access token for authorization: petstore_auth
var petstore_auth = defaultClient.authentications['petstore_auth'];
petstore_auth.accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiMHhiNTNiMTM0NzllMjg5NTI0NWQyOTc4OGM4YWZlZWFhNzI5MDFhMjk5IiwicGFzc3BocmFzZSI6ImUzYTFkNzk4MmEyN2EyNTYxY2IyYTZmMTRmNDgyYzRhNTk2NzgzYTM0YjdkYmQ5NGNjMjgwYjIwYjViNjg4Y2ZlMjYzZjEyZmU2ODU1MDhmNWJkY2Q1ODIxZTEwYTkxZURZV3dqZmhKcXNtOURya0VVR3hvTWc9PSIsImlhdCI6MTUyMjkyODQ3NiwiZXhwIjoxNTU0NDg2MDc2fQ.eLlSeWqMmogsS5QgFriCABKUy58cNMX_w8sXBYOYa90";

var api = new SwaggerComChain.ComchainApi()

var callback = function(error, data, response) {
  if (error) {
  	console.error("an error has occurred");
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ', data);
    console.log('API called successfully. Returned data: ', JSON.stringify(data));
  }
};

var body = new SwaggerComChain.ComChain("0x4c99f3dd07e9bc5ff966444fdb767cfd67e7fd93", "C3014Test-ptSEP74DE2B73BBE8"); // ComChain contract that needs to be added to the BlockChain
api.createComChain(body, callback);