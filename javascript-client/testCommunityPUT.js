var SwaggerComChain = require('swagger_com_chain');

var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure OAuth2 access token for authorization: petstore_auth
var petstore_auth = defaultClient.authentications['petstore_auth'];
petstore_auth.accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiMHhiNTNiMTM0NzllMjg5NTI0NWQyOTc4OGM4YWZlZWFhNzI5MDFhMjk5IiwicGFzc3BocmFzZSI6ImUzYTFkNzk4MmEyN2EyNTYxY2IyYTZmMTRmNDgyYzRhNTk2NzgzYTM0YjdkYmQ5NGNjMjgwYjIwYjViNjg4Y2ZlMjYzZjEyZmU2ODU1MDhmNWJkY2Q1ODIxZTEwYTkxZURZV3dqZmhKcXNtOURya0VVR3hvTWc9PSIsImlhdCI6MTUyMjkyODQ3NiwiZXhwIjoxNTU0NDg2MDc2fQ.eLlSeWqMmogsS5QgFriCABKUy58cNMX_w8sXBYOYa90";

var api = new SwaggerComChain.CommunityApi();

var callback = function(error, data, response) {
  if (error) {
  	console.error("an error has occurred");
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ', data);
    console.log('API called successfully. Returned data: ', JSON.stringify(data));
  }
};

/*var body = {groupName:"BlaBlaGroup", regulated: "false"}; // data to be PUT
console.log('body', body);
api.updateCommunityGroup("0xa1c64f3b874fabd9dec442ddb0d106bdc0bc7ecd", body, callback); //Update contract with group
*/


//var groups = ['0xb4b2aff80b2f0dd99134e208b6c1db414730512c','0x4e08a43a5e931b2a753a1a2291224b12214a9680'];
var groups = ['0x4e08a43a5e931b2a753a1a2291224b12214a9680'];
var body = {account:"0x2e0b8fea9376d8e72784c8ff266452863ab7be16", internalId: "internalIdBla1", retentionHold: true, groups:groups, memberName:"memberNameBla1"}; // data to be PUT
console.log('body', body);
api.updateCommunityMember("0xa1c64f3b874fabd9dec442ddb0d106bdc0bc7ecd", body, callback); //Update contract with member
