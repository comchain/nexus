# SwaggerComChain.MIFID2fields

## Enum


* `callid` (value: `"callid"`)

* `tscid` (value: `"tscid"`)

* `profileid` (value: `"profileid"`)

* `interestid` (value: `"interestid"`)

* `state` (value: `"state"`)

* `direction` (value: `"direction"`)

* `starttimestamp` (value: `"starttimestamp"`)

* `duration` (value: `"duration"`)

* `callername` (value: `"callername"`)

* `calleraddress` (value: `"calleraddress"`)

* `calledname` (value: `"calledname"`)

* `calledaddress` (value: `"calledaddress"`)

* `platform` (value: `"platform"`)

* `site` (value: `"site"`)

* `universalcallid` (value: `"universalcallid"`)

* `participantid` (value: `"participantid"`)

* `participantstarttimestamp` (value: `"participantstarttimestamp"`)

* `participantduration` (value: `"participantduration"`)

* `vrreclocation` (value: `"vrreclocation"`)

* `vrcallid` (value: `"vrcallid"`)

* `vrrecstarttimestamp` (value: `"vrrecstarttimestamp"`)

* `vrrecduration` (value: `"vrrecduration"`)

* `vrarchivedate` (value: `"vrarchivedate"`)

* `vrinvestigatingholddate` (value: `"vrinvestigatingholddate"`)

* `vrsite` (value: `"vrsite"`)

* `vrcalleraddress` (value: `"vrcalleraddress"`)

* `vrcalledaddress` (value: `"vrcalledaddress"`)

* `vrrecfilehash` (value: `"vrrecfilehash"`)

* `vrauditcallid` (value: `"vrauditcallid"`)

* `vrauditmessage` (value: `"vrauditmessage"`)

* `vraudittimestamp` (value: `"vraudittimestamp"`)

* `extrametadataid` (value: `"extrametadataid"`)

* `extrametadatalocation` (value: `"extrametadatalocation"`)


