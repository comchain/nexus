# SwaggerComChain.GroupSchemaReference

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**id** | **String** |  | [optional] 


