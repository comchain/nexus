# SwaggerComChain.MemberApi

All URIs are relative to *http://localhost:8080/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMember**](MemberApi.md#createMember) | **POST** /member | Add a new member contract
[**getMemberRegulated**](MemberApi.md#getMemberRegulated) | **GET** /member/{id}/regulated | Is member regulated


<a name="createMember"></a>
# **createMember**
> MemberSchema createMember(body)

Add a new member contract



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.MemberApi();

var body = new SwaggerComChain.Member(); // Member | Member contract that needs to be added to the BlockChain


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createMember(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Member**](Member.md)| Member contract that needs to be added to the BlockChain | 

### Return type

[**MemberSchema**](MemberSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="getMemberRegulated"></a>
# **getMemberRegulated**
> InlineResponse200 getMemberRegulated(id)

Is member regulated



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.MemberApi();

var id = "id_example"; // String | contract whose regulated status to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getMemberRegulated(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract whose regulated status to retrieve | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

