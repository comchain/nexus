# SwaggerComChain.MIFID2ProvisionSchemaItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**provider** | [**PersonSchema**](PersonSchema.md) |  | [optional] 
**fields** | [**[MIFID2fields]**](MIFID2fields.md) |  | [optional] 


