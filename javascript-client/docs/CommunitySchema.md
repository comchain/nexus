# SwaggerComChain.CommunitySchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**funder** | [**PersonSchema**](PersonSchema.md) |  | [optional] 
**groups** | [**[GroupSchemaReference]**](GroupSchemaReference.md) |  | [optional] 


