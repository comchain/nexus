# SwaggerComChain.MemberSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**identifier** | **String** |  | [optional] 
**familyName** | **String** |  | [optional] 
**givenName** | **String** |  | [optional] 
**address** | [**PlaceSchema**](PlaceSchema.md) |  | [optional] 
**memberOf** | [**[GroupSchemaReference]**](GroupSchemaReference.md) |  | [optional] 
**retentionHold** | **Boolean** |  | [optional] 
**regulated** | **Boolean** |  | [optional] 


