# SwaggerComChain.AdditionalPropertySchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**value** | **String** |  | [optional] 


