# SwaggerComChain.MIFID2Object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**storagePeriod** | **Number** |  | [optional] 
**refreshPeriod** | **String** | refreshPeriod values | [optional] 


<a name="RefreshPeriodEnum"></a>
## Enum: RefreshPeriodEnum


* `PerCall` (value: `"PerCall"`)

* `Daily` (value: `"Daily"`)

* `Weekly` (value: `"Weekly"`)

* `Monthly` (value: `"Monthly"`)




