# SwaggerComChain.ComChainSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**category** | **String** |  | [optional] 
**identifier** | **String** |  | [optional] 
**owns** | [**PersonSchema**](PersonSchema.md) |  | [optional] 
**additionalProperty** | [**[AdditionalPropertySchema]**](AdditionalPropertySchema.md) |  | [optional] 
**mifid2** | [**Reference**](Reference.md) |  | [optional] 


