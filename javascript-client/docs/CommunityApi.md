# SwaggerComChain.CommunityApi

All URIs are relative to *http://localhost:8080/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCommunityMember**](CommunityApi.md#addCommunityMember) | **PUT** /community/{id}/member | Update contract with member
[**createCommunity**](CommunityApi.md#createCommunity) | **POST** /community | Add a new community contract
[**deleteCommunity**](CommunityApi.md#deleteCommunity) | **DELETE** /community/{id} | Deletes a community contract
[**getCommunityGroups**](CommunityApi.md#getCommunityGroups) | **GET** /community/{id}/group | Get an array of group objects in the specified Community
[**getCommunityMembers**](CommunityApi.md#getCommunityMembers) | **GET** /community/{id}/member | Get an array of member objects in the specified Community
[**getCommunityObj**](CommunityApi.md#getCommunityObj) | **GET** /community/{id} | Returns single community contract object at the supplied id
[**getCommunityObjAll**](CommunityApi.md#getCommunityObjAll) | **GET** /community | Returns all community contract objects
[**getCommunityRegulatedGroup**](CommunityApi.md#getCommunityRegulatedGroup) | **GET** /community/{id}/group/{group}/regulated | Is group regulated
[**updateCommunityGroup**](CommunityApi.md#updateCommunityGroup) | **PUT** /community/{id}/group | Update contract with group


<a name="addCommunityMember"></a>
# **addCommunityMember**
> MemberSchema addCommunityMember(id, body)

Update contract with member



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.CommunityApi();

var id = "id_example"; // String | contract to be updated with member

var body = new SwaggerComChain.Member(); // Member | data to update in contract


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addCommunityMember(id, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract to be updated with member | 
 **body** | [**Member**](Member.md)| data to update in contract | 

### Return type

[**MemberSchema**](MemberSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="createCommunity"></a>
# **createCommunity**
> CommunitySchema createCommunity(body)

Add a new community contract



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.CommunityApi();

var body = new SwaggerComChain.Community(); // Community | Community contract that needs to be added to the BlockChain


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createCommunity(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Community**](Community.md)| Community contract that needs to be added to the BlockChain | 

### Return type

[**CommunitySchema**](CommunitySchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="deleteCommunity"></a>
# **deleteCommunity**
> GenericResponse deleteCommunity(id)

Deletes a community contract



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.CommunityApi();

var id = "id_example"; // String | contract to delete


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.deleteCommunity(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract to delete | 

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getCommunityGroups"></a>
# **getCommunityGroups**
> [GroupSchema] getCommunityGroups(id, opts)

Get an array of group objects in the specified Community



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.CommunityApi();

var id = "id_example"; // String | contract whose groups to retrieve

var opts = { 
  'regulated': true // Boolean | regulated flag
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCommunityGroups(id, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract whose groups to retrieve | 
 **regulated** | **Boolean**| regulated flag | [optional] 

### Return type

[**[GroupSchema]**](GroupSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getCommunityMembers"></a>
# **getCommunityMembers**
> [MemberSchema] getCommunityMembers(id, opts)

Get an array of member objects in the specified Community



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.CommunityApi();

var id = "id_example"; // String | contract whose members to retrieve

var opts = { 
  'regulated': true // Boolean | returns only reguletad or not reguleted members
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCommunityMembers(id, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract whose members to retrieve | 
 **regulated** | **Boolean**| returns only reguletad or not reguleted members | [optional] 

### Return type

[**[MemberSchema]**](MemberSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getCommunityObj"></a>
# **getCommunityObj**
> CommunitySchema getCommunityObj(id)

Returns single community contract object at the supplied id



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.CommunityApi();

var id = "id_example"; // String | contract to whose object data to get


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCommunityObj(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract to whose object data to get | 

### Return type

[**CommunitySchema**](CommunitySchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getCommunityObjAll"></a>
# **getCommunityObjAll**
> [CommunitySchema] getCommunityObjAll()

Returns all community contract objects



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.CommunityApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCommunityObjAll(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[CommunitySchema]**](CommunitySchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getCommunityRegulatedGroup"></a>
# **getCommunityRegulatedGroup**
> InlineResponse200 getCommunityRegulatedGroup(id, group)

Is group regulated



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.CommunityApi();

var id = "id_example"; // String | contract whose regulated status to retrieve

var group = "group_example"; // String | contract whose regulated status to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCommunityRegulatedGroup(id, group, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract whose regulated status to retrieve | 
 **group** | **String**| contract whose regulated status to retrieve | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="updateCommunityGroup"></a>
# **updateCommunityGroup**
> GroupSchema updateCommunityGroup(id, body)

Update contract with group



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.CommunityApi();

var id = "id_example"; // String | contract to be updated with group

var body = new SwaggerComChain.Group(); // Group | data to update in contract


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateCommunityGroup(id, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract to be updated with group | 
 **body** | [**Group**](Group.md)| data to update in contract | 

### Return type

[**GroupSchema**](GroupSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

