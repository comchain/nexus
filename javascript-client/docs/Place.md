# SwaggerComChain.Place

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressLocality** | **String** |  | [optional] 
**addressRegion** | **String** |  | [optional] 
**postalCode** | **String** |  | [optional] 
**streetAddress** | **String** |  | [optional] 


