# SwaggerComChain.Group

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**regulated** | **Boolean** |  | [optional] 


