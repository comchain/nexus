# SwaggerComChain.PlaceSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**addressLocality** | **String** |  | [optional] 
**addressRegion** | **String** |  | [optional] 
**postalCode** | **String** |  | [optional] 
**streetAddress** | **String** |  | [optional] 


