# SwaggerComChain.GenericErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **Boolean** |  | [optional] 
**error** | **String** |  | [optional] 


