# SwaggerComChain.Member

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **String** |  | [optional] 
**familyName** | **String** |  | [optional] 
**givenName** | **String** |  | [optional] 
**address** | [**Place**](Place.md) |  | [optional] 
**memberOf** | [**Groups**](Groups.md) |  | [optional] 
**retentionHold** | **Boolean** |  | [optional] 


