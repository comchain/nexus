# SwaggerComChain.MetaDataSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**provider** | [**PersonSchema**](PersonSchema.md) |  | [optional] 
**data** | [**MIFID2Schema**](MIFID2Schema.md) |  | [optional] 


