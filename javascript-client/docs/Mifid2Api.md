# SwaggerComChain.Mifid2Api

All URIs are relative to *http://localhost:8080/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMifid2**](Mifid2Api.md#createMifid2) | **POST** /mifid2 | Add a new mifid2template contract
[**getMifid2ObjAll**](Mifid2Api.md#getMifid2ObjAll) | **GET** /mifid2 | Returns all mifid2 contract objects
[**getMifid2ProvTemplMap**](Mifid2Api.md#getMifid2ProvTemplMap) | **GET** /mifid2/{id}/provision | Returns mappings of provider address to template fields
[**updateMifid2Provision**](Mifid2Api.md#updateMifid2Provision) | **PUT** /mifid2/{id}/provision | Update contract with who is responsible for providing data for fields


<a name="createMifid2"></a>
# **createMifid2**
> MIFID2ObjectSchema createMifid2(body)

Add a new mifid2template contract



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.Mifid2Api();

var body = new SwaggerComChain.MIFID2Object(); // MIFID2Object | Mifid2 contract that needs to be added to the BlockChain


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createMifid2(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MIFID2Object**](MIFID2Object.md)| Mifid2 contract that needs to be added to the BlockChain | 

### Return type

[**MIFID2ObjectSchema**](MIFID2ObjectSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="getMifid2ObjAll"></a>
# **getMifid2ObjAll**
> [MIFID2ObjectSchema] getMifid2ObjAll()

Returns all mifid2 contract objects



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.Mifid2Api();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getMifid2ObjAll(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[MIFID2ObjectSchema]**](MIFID2ObjectSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getMifid2ProvTemplMap"></a>
# **getMifid2ProvTemplMap**
> [MIFID2ProvisionSchemaItem] getMifid2ProvTemplMap(id)

Returns mappings of provider address to template fields



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.Mifid2Api();

var id = "id_example"; // String | contract whose mapping of provider address to template fields to get


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getMifid2ProvTemplMap(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract whose mapping of provider address to template fields to get | 

### Return type

[**[MIFID2ProvisionSchemaItem]**](MIFID2ProvisionSchemaItem.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="updateMifid2Provision"></a>
# **updateMifid2Provision**
> MIFID2ProvisionSchemaItem updateMifid2Provision(id, body)

Update contract with who is responsible for providing data for fields



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.Mifid2Api();

var id = "id_example"; // String | contract to be updated with who is responsible for providing data for fields

var body = new SwaggerComChain.MIFID2ProvisionItem(); // MIFID2ProvisionItem | data to update in contract


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateMifid2Provision(id, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract to be updated with who is responsible for providing data for fields | 
 **body** | [**MIFID2ProvisionItem**](MIFID2ProvisionItem.md)| data to update in contract | 

### Return type

[**MIFID2ProvisionSchemaItem**](MIFID2ProvisionSchemaItem.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

