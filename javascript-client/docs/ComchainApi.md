# SwaggerComChain.ComchainApi

All URIs are relative to *http://localhost:8080/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createComChain**](ComchainApi.md#createComChain) | **POST** /comchain | Add a new comchain contract
[**deleteComChain**](ComchainApi.md#deleteComChain) | **DELETE** /comchain/{id} | Deletes a comchain contract
[**getComChainAllMetadata**](ComchainApi.md#getComChainAllMetadata) | **GET** /comchain/{id}/metadata/ | Get all metadata from contract
[**getComChainObj**](ComchainApi.md#getComChainObj) | **GET** /comchain/{id} | Get single comchain contract object
[**getComChainObjAll**](ComchainApi.md#getComChainObjAll) | **GET** /comchain | Get all comchain contract objects
[**updateComChainMetadata**](ComchainApi.md#updateComChainMetadata) | **PUT** /comchain/{id}/metadata/ | Update contract with metadata
[**updateComChainProperty**](ComchainApi.md#updateComChainProperty) | **PUT** /comchain/{id}/additionalproperty | Update contract with property


<a name="createComChain"></a>
# **createComChain**
> ComChainSchema createComChain(body)

Add a new comchain contract



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.ComchainApi();

var body = new SwaggerComChain.ComChain(); // ComChain | ComChain contract that needs to be added to the BlockChain


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createComChain(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ComChain**](ComChain.md)| ComChain contract that needs to be added to the BlockChain | 

### Return type

[**ComChainSchema**](ComChainSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="deleteComChain"></a>
# **deleteComChain**
> GenericResponse deleteComChain(id)

Deletes a comchain contract



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.ComchainApi();

var id = "id_example"; // String | contract to delete


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.deleteComChain(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract to delete | 

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getComChainAllMetadata"></a>
# **getComChainAllMetadata**
> [MetaDataSchema] getComChainAllMetadata(id, opts)

Get all metadata from contract



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.ComchainApi();

var id = "id_example"; // String | contract whose metadata to retrieve

var opts = { 
  'provider': "provider_example" // String | provider whose metadata to retrieve in contract
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getComChainAllMetadata(id, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract whose metadata to retrieve | 
 **provider** | **String**| provider whose metadata to retrieve in contract | [optional] 

### Return type

[**[MetaDataSchema]**](MetaDataSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getComChainObj"></a>
# **getComChainObj**
> ComChainSchema getComChainObj(id)

Get single comchain contract object



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.ComchainApi();

var id = "id_example"; // String | contract whose data to get


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getComChainObj(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract whose data to get | 

### Return type

[**ComChainSchema**](ComChainSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="getComChainObjAll"></a>
# **getComChainObjAll**
> [ComChainSchema] getComChainObjAll()

Get all comchain contract objects



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.ComchainApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getComChainObjAll(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[ComChainSchema]**](ComChainSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="updateComChainMetadata"></a>
# **updateComChainMetadata**
> MetaDataSchema updateComChainMetadata(id, body)

Update contract with metadata



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.ComchainApi();

var id = "id_example"; // String | contract to be updated with metadata

var body = new SwaggerComChain.MIFID2Schema(); // MIFID2Schema | data to update in contract


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateComChainMetadata(id, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract to be updated with metadata | 
 **body** | [**MIFID2Schema**](MIFID2Schema.md)| data to update in contract | 

### Return type

[**MetaDataSchema**](MetaDataSchema.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="updateComChainProperty"></a>
# **updateComChainProperty**
> GenericResponse updateComChainProperty(id, body)

Update contract with property



### Example
```javascript
var SwaggerComChain = require('swagger_com_chain');
var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new SwaggerComChain.ComchainApi();

var id = "id_example"; // String | contract to be updated with property

var body = [new SwaggerComChain.AdditionalProperty()]; // [AdditionalProperty] | data to update in contract


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateComChainProperty(id, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| contract to be updated with property | 
 **body** | [**[AdditionalProperty]**](AdditionalProperty.md)| data to update in contract | 

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

