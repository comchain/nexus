var SwaggerComChain = require('swagger_com_chain');

var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure OAuth2 access token for authorization: petstore_auth
var petstore_auth = defaultClient.authentications['petstore_auth'];
petstore_auth.accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiMHhiNTNiMTM0NzllMjg5NTI0NWQyOTc4OGM4YWZlZWFhNzI5MDFhMjk5IiwicGFzc3BocmFzZSI6ImUzYTFkNzk4MmEyN2EyNTYxY2IyYTZmMTRmNDgyYzRhNTk2NzgzYTM0YjdkYmQ5NGNjMjgwYjIwYjViNjg4Y2ZlMjYzZjEyZmU2ODU1MDhmNWJkY2Q1ODIxZTEwYTkxZURZV3dqZmhKcXNtOURya0VVR3hvTWc9PSIsImlhdCI6MTUyMjkyODQ3NiwiZXhwIjoxNTU0NDg2MDc2fQ.eLlSeWqMmogsS5QgFriCABKUy58cNMX_w8sXBYOYa90";

var api = new SwaggerComChain.ComchainApi();

var callback = function(error, data, response) {
  if (error) {
  	console.error("an error has occurred");
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ', data);
    console.log('API called successfully. Returned data: ', JSON.stringify(data));
  }
};

/*var body = new SwaggerComChain.Body("prop88", "val88"); // key-value property data to be PUT into existing ComChain contract
console.log('body', body);
api.updateComChainProperty("0xe941e6a8a653571a69c8fa551a90cda8eb4eebdf", body, callback);*/

var body = {hash:"0x71597bf09b9f69b2ef2c70eb1397cd69bb29887f1bd2b5cebea5d4e630e3f237", location:"/opt/nfs/comchain/C3013Test-ptSEP003094C3E049-16870498.json"}; // hash, location: PUT metadata into existing ComChain contract
console.log('body', body);
api.updateComChainMetadata("0xe941e6a8a653571a69c8fa551a90cda8eb4eebdf", body, callback);

/* this one isn't finished or tested
var body = new SwaggerComChain.Access("prop1", "val1"); // address, access: Update ComChain contract with MDE Access
console.log('body', body);
api.updateComChainMdeAccess("0xe941e6a8a653571a69c8fa551a90cda8eb4eebdf", body, callback);*/