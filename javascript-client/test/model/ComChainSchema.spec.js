/**
 * Swagger ComChain
 * ComChain API for telephony and audit.
 *
 * OpenAPI spec version: 1.0.1
 * Contact: sales@gltd.net
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.SwaggerComChain);
  }
}(this, function(expect, SwaggerComChain) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new SwaggerComChain.ComChainSchema();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('ComChainSchema', function() {
    it('should create an instance of ComChainSchema', function() {
      // uncomment below and update the code to test ComChainSchema
      //var instane = new SwaggerComChain.ComChainSchema();
      //expect(instance).to.be.a(SwaggerComChain.ComChainSchema);
    });

    it('should have the property context (base name: "@context")', function() {
      // uncomment below and update the code to test the property context
      //var instane = new SwaggerComChain.ComChainSchema();
      //expect(instance).to.be();
    });

    it('should have the property type (base name: "@type")', function() {
      // uncomment below and update the code to test the property type
      //var instane = new SwaggerComChain.ComChainSchema();
      //expect(instance).to.be();
    });

    it('should have the property id (base name: "id")', function() {
      // uncomment below and update the code to test the property id
      //var instane = new SwaggerComChain.ComChainSchema();
      //expect(instance).to.be();
    });

    it('should have the property category (base name: "category")', function() {
      // uncomment below and update the code to test the property category
      //var instane = new SwaggerComChain.ComChainSchema();
      //expect(instance).to.be();
    });

    it('should have the property identifier (base name: "identifier")', function() {
      // uncomment below and update the code to test the property identifier
      //var instane = new SwaggerComChain.ComChainSchema();
      //expect(instance).to.be();
    });

    it('should have the property owns (base name: "owns")', function() {
      // uncomment below and update the code to test the property owns
      //var instane = new SwaggerComChain.ComChainSchema();
      //expect(instance).to.be();
    });

    it('should have the property additionalProperty (base name: "additionalProperty")', function() {
      // uncomment below and update the code to test the property additionalProperty
      //var instane = new SwaggerComChain.ComChainSchema();
      //expect(instance).to.be();
    });

    it('should have the property mifid2 (base name: "mifid2")', function() {
      // uncomment below and update the code to test the property mifid2
      //var instane = new SwaggerComChain.ComChainSchema();
      //expect(instance).to.be();
    });

  });

}));
