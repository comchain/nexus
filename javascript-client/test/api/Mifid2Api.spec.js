/**
 * Swagger ComChain
 * ComChain API for telephony and audit.
 *
 * OpenAPI spec version: 1.0.1
 * Contact: sales@gltd.net
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.SwaggerComChain);
  }
}(this, function(expect, SwaggerComChain) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new SwaggerComChain.Mifid2Api();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('Mifid2Api', function() {
    describe('createMifid2', function() {
      it('should call createMifid2 successfully', function(done) {
        //uncomment below and update the code to test createMifid2
        //instance.createMifid2(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getMifid2ObjAll', function() {
      it('should call getMifid2ObjAll successfully', function(done) {
        //uncomment below and update the code to test getMifid2ObjAll
        //instance.getMifid2ObjAll(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getMifid2ProvTemplMap', function() {
      it('should call getMifid2ProvTemplMap successfully', function(done) {
        //uncomment below and update the code to test getMifid2ProvTemplMap
        //instance.getMifid2ProvTemplMap(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('updateMifid2Provision', function() {
      it('should call updateMifid2Provision successfully', function(done) {
        //uncomment below and update the code to test updateMifid2Provision
        //instance.updateMifid2Provision(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
