/**
 * Swagger ComChain
 * ComChain API for telephony and audit.
 *
 * OpenAPI spec version: 1.0.1
 * Contact: sales@gltd.net
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/MIFID2Object', 'model/MIFID2ObjectSchema', 'model/MIFID2ProvisionItem', 'model/MIFID2ProvisionSchemaItem'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/MIFID2Object'), require('../model/MIFID2ObjectSchema'), require('../model/MIFID2ProvisionItem'), require('../model/MIFID2ProvisionSchemaItem'));
  } else {
    // Browser globals (root is window)
    if (!root.SwaggerComChain) {
      root.SwaggerComChain = {};
    }
    root.SwaggerComChain.Mifid2Api = factory(root.SwaggerComChain.ApiClient, root.SwaggerComChain.MIFID2Object, root.SwaggerComChain.MIFID2ObjectSchema, root.SwaggerComChain.MIFID2ProvisionItem, root.SwaggerComChain.MIFID2ProvisionSchemaItem);
  }
}(this, function(ApiClient, MIFID2Object, MIFID2ObjectSchema, MIFID2ProvisionItem, MIFID2ProvisionSchemaItem) {
  'use strict';

  /**
   * Mifid2 service.
   * @module api/Mifid2Api
   * @version 1.0.1
   */

  /**
   * Constructs a new Mifid2Api. 
   * @alias module:api/Mifid2Api
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the createMifid2 operation.
     * @callback module:api/Mifid2Api~createMifid2Callback
     * @param {String} error Error message, if any.
     * @param {module:model/MIFID2ObjectSchema} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add a new mifid2template contract
     * 
     * @param {module:model/MIFID2Object} body Mifid2 contract that needs to be added to the BlockChain
     * @param {module:api/Mifid2Api~createMifid2Callback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/MIFID2ObjectSchema}
     */
    this.createMifid2 = function(body, callback) {
      var postBody = body;

      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling createMifid2");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = ['application/json', 'application/xml'];
      var accepts = ['application/json', 'application/xml'];
      var returnType = MIFID2ObjectSchema;

      return this.apiClient.callApi(
        '/mifid2', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getMifid2ObjAll operation.
     * @callback module:api/Mifid2Api~getMifid2ObjAllCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/MIFID2ObjectSchema>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Returns all mifid2 contract objects
     * 
     * @param {module:api/Mifid2Api~getMifid2ObjAllCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/MIFID2ObjectSchema>}
     */
    this.getMifid2ObjAll = function(callback) {
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = [];
      var accepts = ['application/json', 'application/xml'];
      var returnType = [MIFID2ObjectSchema];

      return this.apiClient.callApi(
        '/mifid2', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getMifid2ProvTemplMap operation.
     * @callback module:api/Mifid2Api~getMifid2ProvTemplMapCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/MIFID2ProvisionSchemaItem>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Returns mappings of provider address to template fields
     * 
     * @param {String} id contract whose mapping of provider address to template fields to get
     * @param {module:api/Mifid2Api~getMifid2ProvTemplMapCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/MIFID2ProvisionSchemaItem>}
     */
    this.getMifid2ProvTemplMap = function(id, callback) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling getMifid2ProvTemplMap");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = [];
      var accepts = ['application/json', 'application/xml'];
      var returnType = [MIFID2ProvisionSchemaItem];

      return this.apiClient.callApi(
        '/mifid2/{id}/provision', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the updateMifid2Provision operation.
     * @callback module:api/Mifid2Api~updateMifid2ProvisionCallback
     * @param {String} error Error message, if any.
     * @param {module:model/MIFID2ProvisionSchemaItem} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update contract with who is responsible for providing data for fields
     * 
     * @param {String} id contract to be updated with who is responsible for providing data for fields
     * @param {module:model/MIFID2ProvisionItem} body data to update in contract
     * @param {module:api/Mifid2Api~updateMifid2ProvisionCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/MIFID2ProvisionSchemaItem}
     */
    this.updateMifid2Provision = function(id, body, callback) {
      var postBody = body;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling updateMifid2Provision");
      }

      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling updateMifid2Provision");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = ['application/json', 'application/xml'];
      var accepts = ['application/json', 'application/xml'];
      var returnType = MIFID2ProvisionSchemaItem;

      return this.apiClient.callApi(
        '/mifid2/{id}/provision', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
