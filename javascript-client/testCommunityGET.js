var SwaggerComChain = require('swagger_com_chain');

var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure OAuth2 access token for authorization: petstore_auth
var petstore_auth = defaultClient.authentications['petstore_auth'];
petstore_auth.accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiMHhiNTNiMTM0NzllMjg5NTI0NWQyOTc4OGM4YWZlZWFhNzI5MDFhMjk5IiwicGFzc3BocmFzZSI6ImUzYTFkNzk4MmEyN2EyNTYxY2IyYTZmMTRmNDgyYzRhNTk2NzgzYTM0YjdkYmQ5NGNjMjgwYjIwYjViNjg4Y2ZlMjYzZjEyZmU2ODU1MDhmNWJkY2Q1ODIxZTEwYTkxZURZV3dqZmhKcXNtOURya0VVR3hvTWc9PSIsImlhdCI6MTUyMjkyODQ3NiwiZXhwIjoxNTU0NDg2MDc2fQ.eLlSeWqMmogsS5QgFriCABKUy58cNMX_w8sXBYOYa90";

var api = new SwaggerComChain.CommunityApi();

var callback = function(error, data, response) {
  if (error) {
  	console.error("an error has occurred");
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ', data);
    console.log('API called successfully. Returned data: ', JSON.stringify(data));
  }
};

api.getCommunityObjAll(callback); //get all community contract objects

var body = { body: {regulated: false} }; //because regulated is an optional parameter in the YAML
console.log ('body', body);
api.getCommunityGroups("0xa1c64f3b874fabd9dec442ddb0d106bdc0bc7ecd", body, callback); //get template

var body = { body: {regulated: false} }; //because regulated is an optional parameter in the YAML
console.log ('body', body);
api.getCommunityMembers("0xa1c64f3b874fabd9dec442ddb0d106bdc0bc7ecd", body, callback); //get template

