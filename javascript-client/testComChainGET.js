var SwaggerComChain = require('swagger_com_chain');

var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure OAuth2 access token for authorization: petstore_auth
var bearer = defaultClient.authentications['Bearer'];
bearer.apiKey = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZW1iZXIiOiJDQVMiLCJpYXQiOjE1MjgyMTE3MTEsImV4cCI6MTU1OTc2OTMxMX0.QDsDH7ZcY7PfnzQkgy72AiMBirguO42DHaAk-GMIZLg";

var api = new SwaggerComChain.ComchainApi()

var callback = function(error, data, response) {
  if (error) {
  	console.error("an error has occurred");
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' , data);
    console.log('API called successfully. Returned data: ' , JSON.stringify(data));
  }
};

//api.getComChainIdentifier("0xe941e6a8a653571a69c8fa551a90cda8eb4eebdf", callback); //get indentifier
//api.getComChainProperties("0xe941e6a8a653571a69c8fa551a90cda8eb4eebdf", callback); //get all properties
//api.getComChainMetadata("0xe941e6a8a653571a69c8fa551a90cda8eb4eebdf", 1, "0x2e0b8fea9376d8e72784c8ff266452863ab7be10", callback); //comchainContractAddress, index, providerAddress: get single metadata entry for provider using index
//api.getComChainAllMetadata("0xe941e6a8a653571a69c8fa551a90cda8eb4eebdf", "0x2e0b8fea9376d8e72784c8ff266452863ab7be10", callback); //comchainContractAddress, providerAddress: get all metadata entries for provider

api.getComChainObjAll(callback); //get all comchain contract objects