var SwaggerComChain = require('swagger_com_chain');

var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure JWT access token for authorization
var bearer = defaultClient.authentications['Bearer'];
bearer.apiKey = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZW1iZXIiOiJDQVMiLCJpYXQiOjE1MjgyMTE3MTEsImV4cCI6MTU1OTc2OTMxMX0.QDsDH7ZcY7PfnzQkgy72AiMBirguO42DHaAk-GMIZLg";

var api = new SwaggerComChain.ComchainApi()

var callback = function(error, data, response) {
  if (error) {
  	console.error("an error has occurred");
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ', data);
    console.log('API called successfully. Returned data: ', JSON.stringify(data));
  }
};

var body = new SwaggerComChain.ComChain("owns-value", "identifier-value", "category-value");
api.createComChain(body, callback);