var SwaggerComChain = require('swagger_com_chain');

var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure OAuth2 access token for authorization: petstore_auth
var petstore_auth = defaultClient.authentications['petstore_auth'];
petstore_auth.accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiMHhiNTNiMTM0NzllMjg5NTI0NWQyOTc4OGM4YWZlZWFhNzI5MDFhMjk5IiwicGFzc3BocmFzZSI6ImUzYTFkNzk4MmEyN2EyNTYxY2IyYTZmMTRmNDgyYzRhNTk2NzgzYTM0YjdkYmQ5NGNjMjgwYjIwYjViNjg4Y2ZlMjYzZjEyZmU2ODU1MDhmNWJkY2Q1ODIxZTEwYTkxZURZV3dqZmhKcXNtOURya0VVR3hvTWc9PSIsImlhdCI6MTUyMjkyODQ3NiwiZXhwIjoxNTU0NDg2MDc2fQ.eLlSeWqMmogsS5QgFriCABKUy58cNMX_w8sXBYOYa90";

var api = new SwaggerComChain.MemberApi()

var callback = function(error, data, response) {
  if (error) {
  	console.error("an error has occurred");
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ', data);
    console.log('API called successfully. Returned data: ', JSON.stringify(data));
  }
};

var groups = ['0xbb97ea5baceb0405915560c69706e1a6fd9bf4ab','0x0220ccbd80379bc39c8115723bc0495fb7a7bac3'];
var body = new SwaggerComChain.Member("0x2e0b8fea9376d8e72784c8ff266452863ab7be10", "internalId123", false, false, groups, "John Smith"); // Member contract that needs to be added to the BlockChain
api.createMember(body, callback);