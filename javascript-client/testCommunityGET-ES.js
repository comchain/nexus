var SwaggerComChain = require('swagger_com_chain');

var defaultClient = SwaggerComChain.ApiClient.instance;

// Configure JWT access token for authorization
var bearer = defaultClient.authentications['Bearer'];
bearer.apiKey = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZW1iZXIiOiJDQVMiLCJpYXQiOjE1MjgyMTE3MTEsImV4cCI6MTU1OTc2OTMxMX0.QDsDH7ZcY7PfnzQkgy72AiMBirguO42DHaAk-GMIZLg";

var api = new SwaggerComChain.CommunityApi();

var callback = function(error, data, response) {
  if (error) {
  	console.log('an error has occurred: ', error.response.error);
  } else {
    console.log('API called successfully. Returned data: ', data);
    console.log('API called successfully. Returned data: ', JSON.stringify(data));
  }
};

//api.getCommunityObj("9R5TR2QBOPQ8S4DNVb_b", callback); //get community obj using id
api.getCommunityObjAll(callback); //get all community objects


/*var opts = [];
var regulatedKey = "regulated";
var regulatedVal = true;
var paramObj = {};
paramObj[regulatedKey] = regulatedVal;
opts.push(paramObj);

api.getCommunityGroups("9R5TR2QBOPQ8S4DNVb_b", opts, callback); //Get an array of group objects in the specified Community
*/
