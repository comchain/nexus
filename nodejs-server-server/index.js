'use strict';

const fs        = require('fs'),
    path        = require('path'),
    passport    = require('passport');

const passportJWT   = require("passport-jwt");
const ExtractJWT    = passportJWT.ExtractJwt;
const JWTStrategy   = passportJWT.Strategy;

const app           = require('connect')();
const swaggerTools  = require('swagger-tools');
const jsyaml        = require('js-yaml');
const jwt           = require('./utils/jwt');
const serverPort    = 8080;

const storage       = require('config').get('Storage.type');
const key           = require('config').get('key');
const cert          = require('config').get('cert');

const jwtSecret   = require('./utils/jwt').jwtSecret;

const bodyParser  = require('body-parser');

const timeoutInMilli = 240000;  //used for GET /v1/product/metadata/fields and GET /v1/product/{id}/metadata

const apv           = require('appversion');
console.log("V:", apv.composePatternSync('M.m.p - n - d'));

const spec        = fs.readFileSync(path.join(__dirname,'api/swagger.yaml'), 'utf8');
const swaggerDoc  = jsyaml.safeLoad(spec);
const confProtocol = swaggerDoc.schemes[0];
const transProtcol = require(confProtocol);


let options = {
  swaggerUi: path.join(__dirname, '/swagger.json'),
  controllers: path.join(__dirname, './controllers'),
  useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
};

console.log("confProtocol=", confProtocol);
if (confProtocol === 'https') {
  options.key = fs.readFileSync(key);
  options.cert = fs.readFileSync(cert);
}

app.use(bodyParser.json({limit: 10 * 1024 * 1024}));
app.use(bodyParser.raw({limit: 10 * 1024 * 1024}));

/*
var r = jwt.decrypt({t:'0356ae4872e6c6e43fc5cff4fbb2651a1d0c78b2f9060d3773e6b8fca39c8c999d2abfe66c4b01309d716f88e76950485RFiLNtBU5a/fuSpYeupz4ZGScfffcplMSgS/obH/uZV1TQ/+2mhxJB4NCUAg9Lc'});
console.log(r);
return
*/
// |HBl"dA?(cJeTg:Q&FLM>?M0$8C&7{IEqUQTxt4vI@?^U@}A
passport.use(new JWTStrategy({
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey   : jwtSecret
    },
     (jwtPayload, cb) => {
      return cb(null, jwtPayload);
    }
));

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, (middleware) => {
 
  app.use(middleware.swaggerMetadata());

  app.use(middleware.swaggerSecurity({
      Bearer: (req, def, scopes, callback) => {
        return passport.authenticate('jwt', { session: false },  (err, user, info) => {
          if(err)
            return callback(new Error('Error in passport authenticate'), );
          
          if(!user){
            if (info && info.message)
              return callback( new Error( info.message ) );
            
            return callback(new Error('Failed to authenticate JWT') );
          };

          if (storage === "ElasticSearch") 
            req.user = user;
          else 
            req.user = jwt.decrypt(user);

          return callback(null, user);

        })(req, scopes, callback)
      }
    }));

  // for processing GET /v1/product/metadata/fields
  app.use('/v1/product/metadata/fields', (req, res, next) => {
    if (req.method.toUpperCase() === 'GET') {
      res.setTimeout(timeoutInMilli, () => { //extend request timeout from default 120000 millisecs to timeoutInMilli
        let err = new Error('Response Timeout');
        err.status = 408;
        return next(err);
      });
    }
    return next();
  });

  // for processing GET /v1/product/{id}/metadata
  app.use('/v1/product', (req, res, next) => {
    // Use regex to test the url matches
    if (req.method.toUpperCase() === 'GET' && /^\/v1\/product\/(.+?)\/metadata/i.test(req.originalUrl)) {
      res.setTimeout(timeoutInMilli, () => { //extend request timeout from default 120000 millisecs to timeoutInMilli
        let err = new Error('Response Timeout');
        err.status = 408;
        return next(err);
      });
    }
    return next();
  });

  app.use(function (req, res, next) {
      try {
        console.log(new Date().toISOString(), req.method, req.url );
        if(req.swagger && req.swagger.params.hasOwnProperty('body')){
          let body = req.swagger.params.body.value;
          for (let ind in body){
            if (typeof body[ind] === "undefined" || body[ind] === null){
              delete body[ind];
            }
          };
          if (typeof body['type'] === 'string'){
            body['type'] = body['type'].toLowerCase();
          };
        }
      }catch(error){
        console.log(error);
      }
      return next();
  });

  app.use(middleware.swaggerValidator({ validateResponse: false }))

  app.use(middleware.swaggerRouter(options));
  app.use(middleware.swaggerUi());

  if (confProtocol === 'https') {
    transProtcol.createServer(options, app).listen(serverPort, function() {
      console.log('Your server is listening on port %d (https://localhost:%d)', serverPort, serverPort);
      console.log('Swagger-ui is available on https://localhost:%d/docs', serverPort);
    });
  } else {
    transProtcol.createServer(app).listen(serverPort, function() {
      console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
      console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
    });
  }

});


/*
/// DEV 
const genToken = (member) => {

    let userToken = {
      member       : member, // << not regulated user id/adderss
    };

    let expiresIn = '1 y';
    return jwt.sign(userToken, jwtSecret, { expiresIn: expiresIn });
};

let cas_token   = genToken('CAS');
let rfb_token   = genToken('RFB');

console.log("cas_token", cas_token);
console.log("rfb_token", rfb_token);
*/
