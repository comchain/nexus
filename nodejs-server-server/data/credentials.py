import sys
import ms.version

print ("credentials.py is running...")
ms.version.addpkg(meta="sec", proj="python-scvlib", release="1.0.13")
from scvlib import SecureCredentialsVault

scv = SecureCredentialsVault(ms_env="genpop", scv_env="prod")

login = scv.tell_key(name_space="ENS-OPS/VOICE/vrtesttool", key_name="dev-es-login")
print ("login=" + login)

pwd = scv.tell_key(name_space="ENS-OPS/VOICE/vrtesttool", key_name="dev-es-pwd")
print ("pwd=" + pwd)