//time.js
let convert = ( inputDateString ) => {
	if (typeof inputDateString === 'undefined' || !inputDateString)
		return "";

	let outputDate;
	let convertedString;
	
	if (typeof inputDateString === 'string'){
		if (inputDateString.indexOf('BST') > -1){
		 	let tmp 			= inputDateString.replace('BST', 'GMT');
		 	convertedString	= Date.parse(tmp) - 3600000;
		} else if (inputDateString.indexOf('HKT') > -1){
		 	let tmp 			= inputDateString.replace('HKT', 'GMT');
		 	convertedString		= Date.parse(tmp) - 28800000; 
		} else if (inputDateString.indexOf('JST') > -1){
		 	let tmp 			= inputDateString.replace('JST', 'GMT');
		 	convertedString		= Date.parse(tmp) - 32400000; 
		} else {
			console.log('convert (else)',inputDateString );
			// if split " " === 1 - create utc 
			// var utcDate1 = new Date(Date.UTC(96, 1, 2, 3, 4, 5));
			// expected output: Fri, 02 Feb 1996 03:04:05 GMT
			if (inputDateString.indexOf(" ") === 10 && inputDateString.lastIndexOf(" ") === 10){
				//  2018-09-02 15:47:42.120
				let tmpP;
				let parts = inputDateString.split(" ");
			 	tmpP  	= parts[1].split('.');
			 	console.log('tmpP',tmpP);
				let t 	= tmpP[0].split(':');
				let ms 	= tmpP[1];
				console.log('t', t);
				let d 	= parts[0].split('-')
				console.log(' d[0], d[1], d[2], t[1], t[2], t[3]');
				console.log( d[0], d[1], d[2], t[0], t[1], t[2]);
				convertedString = Date.UTC( d[0], d[1]-1, d[2], t[0], t[1], t[2], ms )
			}else {
				convertedString = inputDateString;
			}
		};
	}else {
		convertedString 	= inputDateString;
	}

	console.log('convertedString:', convertedString);
	outputDate = new Date(convertedString);

	return outputDate.toISOString();
};


module.exports.convert = convert;

// convert("2018-09-02 15:47:42.120")  '2018-09-02T15:47:42.120Z'
// convert("Thu, 30 Aug 2018 15:32:00 GMT")
// convert("Sun Sep 02 16:47:35 BST 2018") // '2018-09-02T15:47:35.000Z'
