const ResponsePayload   = require('./writer').respondWithCode;
const doResponse = (controllerCallback, httpCode, bodyJSON) => {
  controllerCallback (new ResponsePayload(httpCode, bodyJSON));
};
module.exports.doResponse = doResponse;