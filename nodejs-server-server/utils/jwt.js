
const jsonwebtoken       = require('jsonwebtoken');
//const jwtSecret   		= "EQ9jYKU@IYQ*lI6*)1&WUbmQ&I73Q<gpCIIW%P*Bjg!VC:Dr_BLOCKCHAIN";
const jwtSecret   		= "EQ9jYKU@IYQ*lI6*)1&WUbmQ&I73Q<gpCIIW%P*Bjg!VC:Dr";
const storage   		= require('config').get('Storage.type');

const passPhraseSecret    = "cmffweeGfU6K]_6=dSGJa@9_Y2u?WayiuySNxr7`yMAbzE>VR$_8A`(Uoijioj&^*Z,/GEjqm%j"
const encryptor           = require('simple-encryptor')(passPhraseSecret);

const genToken = ( member, account, passphrase ) => {

	//console.log(' member, account, passphrase',  member, account, passphrase);

	let userToken;
	if (storage === "ElasticSearch") 
	    	userToken = {
	     	member       : member, //encryptor.encrypt(member), // << not regulated user id/adderss
	    	};
	else if (storage === "Quorum") 
		userToken = {
	     	member   	   : encryptor.encrypt(member), // << not regulated user id/adderss
	     	account 	   : encryptor.encrypt(account),
	     	passphrase   : encryptor.encrypt(passphrase)
	    	};

	let expiresIn = '100 y';
	return jsonwebtoken.sign(userToken, jwtSecret, { expiresIn: expiresIn });
};

const decrypt = (obj) => {
	for (let ind in obj){
		obj[ind] = encryptor.decrypt( obj[ind] );
	};
	return obj;
};



module.exports.genToken 		= genToken;
module.exports.jwtSecret 	= jwtSecret
module.exports.decrypt 		= decrypt;
