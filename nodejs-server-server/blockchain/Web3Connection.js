	'use strict';

	const Web3 			= require( 'web3-quorum' );
	const config 			= require('config');
	const jwt 			= require('../utils/jwt');

	let web3, storage, host, port;

	if (config.has('Storage.type')) {
		storage = config.get('Storage.type');
	}
	console.log('storage', storage);

	if (storage === "Quorum") {
		
		if (config.has('Storage.host')) {
			host = config.get('Storage.host');
		}
		console.log('host', host);

		if (config.has('Storage.port')) {
			port = config.get('Storage.port');
		}
		console.log('port', port);	

		var rpcHost = "http://" + host + ":" + port; //"http://192.168.3.47:22000"; "http://192.168.4.53:22000";
		web3 = new Web3(new Web3.providers.HttpProvider(rpcHost));

		console.log('Connected=' + web3.isConnected());
		console.log('BlockNumber=' + web3.eth.blockNumber);
		console.log('Mining=' + web3.eth.mining);
		console.log('Coinbase', web3.eth.coinbase);
		console.log('Accounts', web3.eth.accounts);


		let token = jwt.genToken(  null, web3.eth.coinbase, "" );
		console.log('\nADMIN TOKEN(COINBASE): ', token);


		token = jwt.genToken(  null, "0xa7ff963c0e7fd492ec6b3dd4e9e11dbd51c7bd0f", "Pa55w0rd" );
		console.log('\nADMIN TOKEN(TANGO): ', token);

		token = jwt.genToken(  null, "0x12902ad339523c1b9248631de072b6fe3830d7f5", "Pa55w0rd" );
		console.log('\nADMIN TOKEN(GLTD): ', token);

		token = jwt.genToken(  null, "0xaa98676d3507a31eb788f98f793a16b838075775", "Pa55w0rd" );
		console.log('\nADMIN TOKEN(COMIT): ', token);

		token = jwt.genToken(  null, "0xde31e7c99b2b7be3f249d621ca296641cfd6bb00", "Pa55w0rd" );
		console.log('\nADMIN TOKEN(EV): ', token);


		token = jwt.genToken(  "0x9bdd0b1723b14544d8be64a7ac0808efea979685", "0xfcc5c4691416136b631c5d7745742f91afd063e0", "Pa55w0rd" );
		console.log('\nProvider TOKEN(CAS): ', token);

		token = jwt.genToken(  "0x83d994f5430e1589a51af742b1760b8e3360a92a", "0xb3f9d6bdd9f1cf43d0a4772f6aac70c94fc49938", "Pa55w0rd" );
		console.log('\nProvider TOKEN(Verba): ', token);

		token = jwt.genToken(  "0x1e68aa5e862bd2df1c03cbf3294dcdbe8dee2a19", "0x234d1e2bd104e7d4d6965f1d14fa5a965cf3f0ea", "Pa55w0rd" );
		console.log('\nProvider TOKEN(CCC): ', token);


		

		//console.log('web3.eth',web3.eth);
		//console.log('web3.eth.personal',web3.eth.personal);
		//console.log('web3.personal',web3.personal);
	} else {
		console.log('Error trying to configure/create connection');
	}

	module.exports.web3 = web3;
