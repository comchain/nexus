'use strict'

var web3 = require('../blockchain/Web3Connection').web3;

////
const createAccout  = ( passphrase ) => new Promise((resolve, reject) => {
	if (typeof passphrase !== "string" || passphrase.length < 3){
		return reject('Invalid passphrase!');
	};
    web3.personal.newAccount(passphrase,function(error, result){
        if(!error)
            return  resolve(result);

        return reject(error)
    });

});

const unlockAccount = ( account, passphrase, time ) => new Promise( (resolve, reject) => { 	
	if (!time || isNaN(time))
        time = 30;
    else {
    	time = Math.max(1,		time);
   		time = Math.min(120,	time);
	};

    web3.personal.unlockAccount(account, passphrase, time, (error, result) => {
     	if (error){
     		console.log('unlockAccount', error);
     		reject(error);
     	}else {
     		console.log('unlockAccount success');
     		resolve(result);
     	}
    });
});

const getBalances = () => new Promise(function(resolve, reject) {	
    let i=0;
    let amts = [];
    web3.eth.accounts.forEach( (e) => {
    	var amt =  Number(web3.fromWei(web3.eth.getBalance(e), "ether" ) + '');
		amts.push({balance:amt, account: e});
        i++;
    });
    resolve(amts);
});


const getBalance = ( account ) => new Promise(function(resolve, reject) {
	web3.eth.getBalance(account, (err, amt) => {
		if(err)
			return reject(err);

		return resolve( Number(web3.fromWei(amt, "ether")+'') );
	});	
});

const shareEther 	= ( from, to, amountInEther ) => {
	return new Promise( (resolve, reject) => {
		var transaction = {
			from: from,
			to: to,
			value: web3.toWei(amountInEther, "ether")
		};
		web3.eth.sendTransaction( transaction, (error, result) => {
			if ( error )
				return reject( error );

			return resolve( result );
		});
	});
};

//////////////////////////////////////////////////////////
														//
	module.exports.createAccout		= createAccout; 	//
														//
	module.exports.unlockAccount	= unlockAccount;	//
														//
	module.exports.getBalances 		= getBalances; 		//
	module.exports.getBalance 		= getBalance;		//
														//
	module.exports.shareEther 		= shareEther;		//
														//
														//
//////////////////////////////////////////////////////////
