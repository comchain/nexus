'use strict';

const web3 = require('./Web3Connection').web3;

exports.getClearString  = function(value, toAscii) {
    if (toAscii)
        value =  web3.toAscii(value);

    let str = "";
    for (let i = 0; i < value.length; i++){
       if (value.charCodeAt(i) > 0)
        str += value[i];
    };

    return str;
};