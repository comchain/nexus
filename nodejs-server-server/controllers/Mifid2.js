'use strict';

var utils = require('../utils/writer.js');
var config = require('config');
var Mifid2;

var storage = config.get('Storage.type');
if (storage === "ElasticSearch") {
  Mifid2 = require('../service/Mifid2EsService');
} else if (storage === "Quorum") {
  Mifid2 = require('../service/Mifid2Service');
}

module.exports.createMiFID2 = function createMiFID2 (req, res, next) {
  var body = req.swagger.params['body'].value;
  Mifid2.createMiFID2(req.user,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getMiFID2ObjAll = function getMiFID2ObjAll (req, res, next) {
  Mifid2.getMiFID2ObjAll(req.user)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getMiFID2Obj = function getMiFID2Obj (req, res, next) {
  var id = req.swagger.params['id'].value;
  Mifid2.getMiFID2Obj(req.user, id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateMiFID2Provision = function updateMiFID2Provision (req, res, next) {
  var id = req.swagger.params['id'].value;
  var body = req.swagger.params['body'].value;
  Mifid2.updateMiFID2Provision(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};


module.exports.deleteMiFID2 = function deleteMiFID2 (req, res, next) {
  var id = req.swagger.params['id'].value;
  Mifid2.deleteMiFID2(req.user,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
