'use strict';

const utils   = require('../utils/writer.js');
const config  = require('config');
let ExtraData;

let storage   = config.get('Storage.type');
if (storage === "ElasticSearch") {
  ExtraData = require('../service/ExtraDataEsService');
} else if (storage === "Quorum") {
  ExtraData = require('../service/ExtraDataService');
}

module.exports.createExtraData = function createExtraData (req, res, next) {
  let body  = req.swagger.params['body'].value;
  ExtraData.createExtraData(req.user,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteExtraData = function deleteExtraData (req, res, next) {
  var id    = req.swagger.params['id'].value;
  ExtraData.deleteExtraData(req.user,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getExtraDataAll = function getExtraDataAll (req, res, next) {
  let body = req.swagger.params['identifier'].value;
  ExtraData.getExtraDataAll(req.user, body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateExtraData = function updateExtraData (req, res, next) {
  let id    = req.swagger.params['id'].value;
  let body  = req.swagger.params['body'].value;
  ExtraData.updateExtraData(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateExtraDataProperty = function updateExtraDataProperty (req, res, next) {
  let id    = req.swagger.params['id'].value;
  let body  = req.swagger.params['body'].value;
  ExtraData.updateExtraDataProperty(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

