'use strict';

var utils = require('../utils/writer.js');
var config = require('config');
var Subgroup;

var storage = config.get('Storage.type');
if (storage === "ElasticSearch") {
  Subgroup = require('../service/SubgroupEsService');
} else if (storage === "Quorum") {
  Subgroup = require('../service/SubgroupService');
}
 

 module.exports.updateGroup = function updateGroup (req, res, next) {
  let id 	 = req.swagger.params['id'].value;
  let body = req.swagger.params['body'].value;
  Subgroup.updateGroup(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getGroup = function getGroup (req, res, next) {
  let id    = req.swagger.params['id'].value;
  Subgroup.getGroup(req.user,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
