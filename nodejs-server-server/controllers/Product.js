'use strict';

const utils   = require('../utils/writer.js');
const config  = require('config');

let Product;
let storage = config.get('Storage.type');

if (storage === "ElasticSearch") {
  Product = require('../service/ProductEsService');
} else if (storage === "Quorum") {
  Product = require('../service/ProductService');
}

module.exports.createProduct = function createProduct (req, res, next) {
  let body = req.swagger.params['body'].value;
  Product.createProduct(req.user,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateProduct = function updateProduct (req, res, next) {
  let id = req.swagger.params['id'].value;
  let body = req.swagger.params['body'].value;
  Product.updateProduct(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};


module.exports.deleteProduct = function deleteProduct (req, res, next) {
  let id = req.swagger.params['id'].value;
  Product.deleteProduct(req.user,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getProductAllMetadata = function getProductAllMetadata (req, res, next) {
  let id         = req.swagger.params['id'].value;
  let provider   = req.swagger.params['provider'].value;
  let limit      = req.swagger.params['limit'].value;
  let offset     = req.swagger.params['offset'].value;
  let starttime  = req.swagger.params['starttime'].value;
  let endtime    = req.swagger.params['endtime'].value;
  let recordType;
  
  //console.log("req.swagger.params",req.swagger.params);

  if (req.swagger.params['type'])
    recordType = req.swagger.params['type'].value;
  Product.getProductAllMetadata(req.user,id,provider, limit, offset, starttime, endtime, recordType)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getProductObj = function getProductObj (req, res, next) {
  let id = req.swagger.params['id'].value;
  Product.getProductObj(req.user,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getProductObjAll = function getProductObjAll (req, res, next) {
  Product.getProductObjAll(req.user)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateProductMetadata = function updateProductMetadata (req, res, next) {
  let id = req.swagger.params['id'].value;
  let body = req.swagger.params['body'].value;
  Product.updateProductMetadata(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};


module.exports.addMetadata = function addMetadata (req, res, next) {
  var body = req.swagger.params['body'].value;
  Product.addMetadata(req.user,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.addMetadataMisc = function addMetadataMisc (req, res, next) {
  let site      = req.swagger.params['site'].value;
  let wav     = req.swagger.params['wav'].value;
  var body = req.swagger.params['body'].value;
  Product.addMetadataMisc(req.user,site,wav,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateProductProperty = function updateProductProperty (req, res, next) {
  let id = req.swagger.params['id'].value;
  let body = req.swagger.params['body'].value;
  Product.updateProductProperty(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateProductBulkMetadata = function updateProductBulkMetadata (req, res, next) {
  let body = req.swagger.params['body'].value;
  let regulaterule = req.swagger.params['regulaterule'].value;
  Product.updateProductBulkMetadata(req.user, regulaterule, body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateProductBulkMetadataMisc = function updateProductBulkMetadataMisc (req, res, next) {
  let body = req.swagger.params['body'].value;
  Product.updateProductBulkMetadataMisc(req.user, body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateProductOwner = function updateProductOwner (req, res, next) {
  let id = req.swagger.params['id'].value;
  let body = req.swagger.params['body'].value;
  Product.updateProductOwner(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};


module.exports.getProductBulkMetadata = function getProductBulkMetadata (req, res, next) {
  let provider   = req.swagger.params['provider'].value;
  let limit      = req.swagger.params['limit'].value;
  let offset     = req.swagger.params['offset'].value;
  let starttime  = req.swagger.params['starttime'].value;
  let endtime    = req.swagger.params['endtime'].value;
  Product.getProductBulkMetadata(req.user, provider, limit, offset, starttime, endtime)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getProductBulkMetadataFields = function getProductBulkMetadataFields (req, res, next) {
  let provider   = req.swagger.params['provider'].value;
  let size       = req.swagger.params['size'].value;
  let starttime  = req.swagger.params['starttime'].value;
  let endtime    = req.swagger.params['endtime'].value;
  let its        = req.swagger.params['its'].value;
  Product.getProductBulkMetadataFields(req.user, provider, size, starttime, endtime, its)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};


module.exports.findProductObj = function findProductObj (req, res, next) {
  let identifier = req.swagger.params['identifier'].value;
  Product.findProductObj(req.user, identifier)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

