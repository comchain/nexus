'use strict';

var utils = require('../utils/writer.js');
var config = require('config');
var Member;

var storage = config.get('Storage.type');
if (storage === "ElasticSearch") {
  Member = require('../service/MemberEsService');
} else if (storage === "Quorum") {
  Member = require('../service/MemberService');
}

module.exports.getMember = function getMember (req, res, next) {
  let id = req.swagger.params['id'].value;
  Member.getMember(req.user,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};


module.exports.createMember = function createMember (req, res, next) {
  let body = req.swagger.params['body'].value;
  Member.createMember(req.user,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getMemberRegulated = function getMemberRegulated (req, res, next) {
  let id = req.swagger.params['id'].value;
  Member.getMemberRegulated(req.user,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateCommunityMember = function updateCommunityMember (req, res, next) {
  let id = req.swagger.params['id'].value;
  let body = req.swagger.params['body'].value;
  Member.updateCommunityMember(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
