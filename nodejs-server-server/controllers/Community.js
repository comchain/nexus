'use strict';

const utils     = require('../utils/writer.js');
const config    = require('config');
const storage   = config.get('Storage.type');
let Community;


if (storage === "ElasticSearch") {
  Community = require('../service/CommunityEsService');
} else if (storage === "Quorum") {
  Community = require('../service/CommunityService');
}

module.exports.addCommunityMember = function addCommunityMember (req, res, next) {
  var id = req.swagger.params['id'].value;
  var body = req.swagger.params['body'].value;
  Community.addCommunityMember(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.createCommunity = function createCommunity (req, res, next) {
  var body = req.swagger.params['body'].value;
  Community.createCommunity(req.user,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteCommunity = function deleteCommunity (req, res, next) {
  var id = req.swagger.params['id'].value;
  Community.deleteCommunity(req.user,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getCommunityGroups = function getCommunityGroups (req, res, next) {
  var id = req.swagger.params['id'].value;
  var regulated = req.swagger.params['regulated'].value;
  Community.getCommunityGroups(req.user,id,regulated)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getCommunityMembers = function getCommunityMembers (req, res, next) {
  var id = req.swagger.params['id'].value;
  var regulated = req.swagger.params['regulated'].value;
  Community.getCommunityMembers(req.user,id,regulated)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getCommunityObj = function getCommunityObj (req, res, next) {
  var id = req.swagger.params['id'].value;
  Community.getCommunityObj(req.user,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getCommunityObjAll = function getCommunityObjAll (req, res, next) {
  Community.getCommunityObjAll(req.user)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getCommunityRegulatedGroup = function getCommunityRegulatedGroup (req, res, next) {
  var id = req.swagger.params['id'].value;
  var group = req.swagger.params['group'].value;
  Community.getCommunityRegulatedGroup(req.user,id,group)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateCommunityGroup = function updateCommunityGroup (req, res, next) {
  var id = req.swagger.params['id'].value;
  var body = req.swagger.params['body'].value;
  Community.updateCommunityGroup(req.user,id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};