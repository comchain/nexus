'use strict';

const esClient        = require('../elastic/ElasticSearchConnection').esClient;

const ccType          = require('../elastic/ElasticSearchConnection').ccType;
const mifid2Index     = require('../elastic/ElasticSearchConnection').mifid2Index;
const esUtils         = require('../elastic/EsUtils');

const ResponsePayload = require('../utils/writer').respondWithCode;
const _               = require('lodash');



/**
 * Add a new MiFID2 template
 * 
 *
 * body MIFID2Provision MiFID2 template
 * returns MIFID2ProvisionSchema
 **/

const MiFID2Fileds = {
  "identifier"                : null,
  "type"                      : [],
  "comid"                     : [],
  "cscid"                     : [],
  "profileid"                 : [],
  "interestid"                : [],
  "site"                      : [],
  "direction"                 : [],
  "starttimestamp"            : [],
  "endtimestamp"              : [],
  "sourcename"                : [],
  "sourceaddress"             : [],
  "destinationname"           : [],
  "destinationaddress"        : [],
  "state"                     : [],
  "duration"                  : [],
  "platform"                  : [],
  "universalcallid"           : [],
  "participants"              : [],
  "filestoragelocation"       : [],
  "archivedate"               : [],
  "filehash"                  : [],
  "additionalProperty"        : [],
  "variety"                   : [],
  "vrquality"                 : [],
  "vrchannel"                 : []
};

exports.createMiFID2 = function(user,body) {
  return new Promise(function(resolve, reject) {

  if ( _.isEmpty(body) ) 
     return esUtils.doResponse( reject, 422, { "success": false, "error": "ERROR: createMiFID2 failed - Bad input data"} );


  let MiFID2Obj = _.clone(MiFID2Fileds);
  
  for (let ind in MiFID2Obj){
    try {
      if (body.hasOwnProperty(ind))
        MiFID2Obj[ind] = body[ind];
      // TO DO - check if member/community exists
    }catch (error){
      console.log(error);
    }
  }
    
  esUtils.add(mifid2Index, ccType, MiFID2Obj)
    .then( resp => {
      console.log('createMiFID2 resp=', JSON.stringify(resp.body, null, 4));
      if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
        return esUtils.doResponse( reject, 400, { "success": false, "error": "ERROR: createMiFID2 failed - add response does not have body"} )
     
      let responseObject = {
        '@context'  : esUtils.contextSchema,
        '@type'     : esUtils.mifid2SchemeType,
        'id'        : resp._id,
      };

      try {
        for ( let ind in MiFID2Obj ){
          if ( !( MiFID2Obj[ind] instanceof Array ) ){
            responseObject[ind] = responseObject[ind];
            continue;
          }

          responseObject[ind] = [];

          for (let i = 0; i < MiFID2Obj[ind].length; i++){
            responseObject[ind][i] = {
              '@context': esUtils.contextSchema,
              '@type'   : esUtils.memberSchemeType,
              '@id'     : MiFID2Obj[ind][i]             
            };
          };
        };
      }catch(error){
        console.log(error);
      };
      return esUtils.doResponse( resolve, 200, responseObject );
    })
    .catch( error => {
      let errorMessage = ''
      if (typeof error === 'string')
        errorMessage = error;
      else if (error && error.message)
        errorMessage = error.message;

      return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: createMiFID2 failed: " + errorMessage} );
    });

  });
};


/**
 * Deletes a MiFID2
 * 
 *
 * id String MiFID2 to delete
 * returns GenericResponse
 **/
exports.deleteMiFID2 = function(user,id) {
  return new Promise(function(resolve, reject) {
      esUtils.deleteDoc ( mifid2Index, ccType, id )
        .then( resp => {
          if (resp === null || resp === undefined || resp.result !== "deleted") //failed to delete doc
            return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: deleteMiFID2 failed - delete response does not have body"} )

          return esUtils.doResponse( resolve, 200, { "success" : true  } );
        })
        .catch( error => {
          let errorMessage = ''
          if (typeof error === 'string')
            errorMessage = error;
          else if (error && error.message)
            errorMessage = error.message;

          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: deleteMiFID2 failed: " + errorMessage} );
        });
  });
}


/**
 * Returns MiFID2 template
 * 
 *
 * id String contract to be updated with who is responsible for providing data for fields
 * returns MIFID2ProvisionSchema
 **/
exports.getMiFID2Obj = function(user,id) {
  console.log('getMifid2Obj',  mifid2Index, ccType);
  return new Promise(function(resolve, reject) {
    let query = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
     };
    esUtils.checkIfExistInSearch( mifid2Index, ccType, query, false)
      .then( resp => {
          if (!resp)
            return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getMiFID2Obj no MiFID2 matched"} );
          console.log('resp', resp);

          let responseObject = {
            '@context'  : esUtils.contextSchema,
            '@type'     : esUtils.mifid2SchemeType,
            'id'        : resp._id,
          };

          try {
            for ( let ind in resp._source ){
              if ( !( resp._source[ind] instanceof Array ) ){
                responseObject[ind] = resp._source[ind];
                continue;
              };

              responseObject[ind] = [];

              for (let i = 0; i < resp._source[ind].length; i++){
                responseObject[ind][i] = {
                  '@context': esUtils.contextSchema,
                  '@type'   : esUtils.memberSchemeType,
                  '@id'     : resp._source[ind][i]             
                };
              };
            };
          }catch(error){
            console.log(error);
          };

          return esUtils.doResponse( resolve, 200, responseObject );
      })
      .catch( error => {
        let errorMessage = ''
        if (typeof error === 'string')
          errorMessage = error;
        else if (error && error.message)
          errorMessage = error.message;

        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getMiFID2Obj failed: " + errorMessage} );
      })
  });
}


/**
 * Returns all MiFID2 templates
 * 
 *
 * returns List
 **/
exports.getMiFID2ObjAll = function(user) {
  return new Promise(function(resolve, reject) {
    esUtils.checkIfExistInSearch( mifid2Index, ccType, { match_all: {} }, true)
      .then( resp => {
        //
         if (!resp)
            return esUtils.doResponse( resolve, 200, [] );
 
          let responseArray = [];

          for ( let mifid2 of resp){
            let responseObject = {
              '@context'  : esUtils.contextSchema,
              '@type'     : esUtils.mifid2SchemeType,
              'id'        : mifid2._id,
            };
            try {
              for ( let ind in mifid2._source ){
                if ( !( mifid2._source[ind] instanceof Array ) ){
                  responseObject[ind] = mifid2._source[ind];
                  continue;
                };

                responseObject[ind] = [];

                for (let i = 0; i < mifid2._source[ind].length; i++){
                  responseObject[ind][i] = {
                    '@context': esUtils.contextSchema,
                    '@type'   : esUtils.memberSchemeType,
                    '@id'     : mifid2._source[ind][i]             
                  };
                };
              };
            }catch(error){
              console.log(error);
            };

            responseArray.push( responseObject );

          }
          
          return esUtils.doResponse( resolve, 200, responseArray );
        //
      })
      .catch( error => {
        let errorMessage = ''
        if (typeof error === 'string')
          errorMessage = error;
        else if (error && error.message)
          errorMessage = error.message;

        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getMiFID2ObjAll failed: " + errorMessage} );
      })
  });
}


/**
 * Update MiFID2 with who is responsible for providing data for fields
 * 
 *
 * id String contract to be updated with who is responsible for providing data for fields
 * body MIFID2Provision data to update in contract
 * returns MIFID2ProvisionSchema
 **/
exports.updateMiFID2Provision = function(user,id,body) {

  
 
  return new Promise(function(resolve, reject) {

    if ( _.isEmpty(body) ) 
       return esUtils.doResponse( reject, 422, { "success": false, "error": "ERROR: updateMiFID2Provision failed - Bad input data"} );

    //update
    let query = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
     };
     esUtils.checkIfExistInSearch( mifid2Index, ccType, query, false)
      .then( resp => {

          if (!resp)
            return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateMiFID2Provision no MiFID2 matched"} );


          //
          let MiFID2Obj = _.clone(MiFID2Fileds);
          for (let ind in MiFID2Obj){
            try {
              if (body.hasOwnProperty(ind))
                MiFID2Obj[ind] = body[ind];
              // TO DO - check if member/community exists
            }catch (error){
              console.log(error);
            };
          };
          return esUtils.addWithId( mifid2Index, ccType, id, MiFID2Obj )
            .then( ( resp ) => {
              if (resp === null || resp === undefined || resp.result !== "updated") //failed to update existing doc
                return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateMiFID2Provision failed - update response does not have body"} )

              let responseObject = {
                '@context'  : esUtils.contextSchema,
                '@type'     : esUtils.mifid2SchemeType,
                'id'        : resp._id,
              };

              try {
                for ( let ind in MiFID2Obj ){
                  if ( !( MiFID2Obj[ind] instanceof Array ) ){
                    responseObject[ind] = responseObject[ind];
                    continue;
                  }

                  responseObject[ind] = [];

                  for (let i = 0; i < MiFID2Obj[ind].length; i++){
                    responseObject[ind][i] = {
                      '@context': esUtils.contextSchema,
                      '@type'   : esUtils.memberSchemeType,
                      '@id'     : MiFID2Obj[ind][i]             
                    };
                  };
                };
              }catch(error){
                console.log(error);
              };
              return esUtils.doResponse( resolve, 200, responseObject );
              //
            });
          //
      })
      .catch( error => {
        console.log('error',error);
        let errorMessage = ''
        if (typeof error === 'string')
          errorMessage = error;
        else if (error && error.message)
          errorMessage = error.message;

        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateMiFID2Provision failed: " + errorMessage} );
      })

  });
}

