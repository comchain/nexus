'use strict';

//const esClient = require('../elastic/ElasticSearchConnection').esClient;
const ccType          = require('../elastic/ElasticSearchConnection').ccType;
const communityIndex  = require('../elastic/ElasticSearchConnection').communityIndex;
const esUtils         = require('../elastic/EsUtils');
const groupIndex      = require('../elastic/ElasticSearchConnection').groupIndex;
const memberIndex     = require('../elastic/ElasticSearchConnection').memberIndex;
//const ResponsePayload = require('../utils/writer').respondWithCode;
const doResponse      = require('../utils/response').doResponse;
const schema          = require('../utils/schema');
const _               = require('lodash');

/**
 * Add new member to community
 * 
 *
 * id String contract to be updated with member
 * body Member data to update in contract
 * returns MemberSchema
 **/
exports.addCommunityMember = function(user, id, body) {
  // one new filed "email" - minor change to do in the future
  //console.log('addCommunityMember user=', user);
  console.log('addCommunityMember id=', id);
  console.log('addCommunityMember body=', body);
  return new Promise(function(resolve, reject) {

    if (!body.hasOwnProperty('identifier')) //input identifier is mandatory
      return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed - Member identifier not supplied"} )

    let communityQuery = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
     };
    return esUtils.checkIfExistInSearch(communityIndex, ccType, communityQuery) //search for community
      .then( community => {
        if (!community) // no such community 
          return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed - Community not found"} )

        var memberIdentifier = body.identifier;
        var groups = [];  //array of group ES ids

        //groups array has to be set before calling this function
        const createMember = function(){
         // console.log('addCommunityMember in createMember()');
         let memberQuery = {
            "constant_score" : { 
               "filter" : { 
                  "term" : {"identifier.keyword" : memberIdentifier}
               }
            }
         };
          return esUtils.checkIfExistInSearch(memberIndex, ccType, memberQuery) //search for member
            .then( member => {
              if (!member) {// no such member found, so create it

                let additionalProperty = [];
                for (let prop of body.additionalProperty){
                  if (prop.hasOwnProperty("name") && prop.hasOwnProperty("value") )
                    additionalProperty.push(prop);
                };
                body.additionalProperty = additionalProperty;

                member = esUtils.createNewMember(body, groups, id);
                //console.log('addCommunityMember new member=', member);
                return esUtils.add(memberIndex, ccType, member)  //create new member
                  .then( resp => {
                    //console.log('addCommunityMember resp=', JSON.stringify(resp, null, 4));
                    if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
                      return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed - New Member not created"} );

                    member.id = resp._id;  //add newly created Member's id to final JSON response
                    member["@context"] = schema.contextSchema;
                    member["@type"] =  schema.memberSchemeType;
                    return doResponse( resolve, 200, member ); //Success
                  }); //end create new member
              } else {// member already exist 
                  return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed - Member already exists", "id": member._id } );
                
              } //end else
            }); //end search for member
        }; //end createMember function

        if (body.hasOwnProperty('groups') && (body.groups.length > 0)) { //process input group(s)
          groups = body.groups;
          let j = 0;  //flag to wait for the async calls in for loop code to finish executing
          for (var i = 0; i < groups.length; i++){
            let groupId = groups[i];
            //console.log('groupId', groupId);
            let groupQuery = {
                "constant_score" : { 
                   "filter" : { 
                      "term" : {"_id" : groupId}
                   }
                }
             };
            esUtils.checkIfExistInSearch(groupIndex, ccType, groupQuery) //search for group
              .then ( group => {
                if (!group) // one of the input groups doesn't exist 
                  return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed - Group does not exist, groupId=", groupId} );

               // console.log("addCommunityMember j=", j, "groups.length=", groups.length);
                if (++j === groups.length) {
                  //console.log("addCommunityMember ++j=== groups.length");
                  return createMember();
                }
                  
              })
              .catch( error => {
                return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed while checking existence of group(s) - " + error.message } );
              }) //end search for group
          } //end for
        } else {
          return createMember();
        }

      } ) //end search for community
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Add a new community contract
 * 
 *
 * body Community Community contract that needs to be added to the BlockChain
 * returns CommunitySchema
 **/
exports.createCommunity = function(user, body) {
  //console.log('createCommunity user=', user);
  console.log('createCommunity body=', body);
  return new Promise(function(resolve, reject) {
    let communityQuery = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"name.keyword" : body.name}
           }
        }
     };
    return esUtils.checkIfExistInSearch(communityIndex, ccType, communityQuery) //search for community
      .then(community => {
        if (community) //community already exists
          return doResponse( reject, 400, { "success": false, "error": "ERROR: createCommunity failed - Community already exists", "id": community._id} )

        let organisation = {
          "funder": {
            "@id": user.member //  << should be just ID of user
          },
          "groups": [],  //holds ids as text, not as object
          "name": body.name,
        };

        console.log('createCommunity new community', organisation);
        return esUtils.add(communityIndex, ccType, organisation)  //create new community
          .then( resp => {
              //console.log('createCommunity resp=', JSON.stringify(resp, null, 4));
              if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
                return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - New Community not created"} );

              organisation.id           = resp._id;     //add new community id to final JSON response
              
              organisation["@type"]     = schema.communitySchemeType;
              organisation["@context"]  = schema.contextSchema;

              organisation.funder["@type"]     = schema.memberSchemeType;
              organisation.funder["@context"]  = schema.contextSchema;

              return doResponse( resolve, 200, organisation ); //Success
          }); //end create new community
      } ) //end search for community
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Deletes a community contract
 * 
 *
 * id String contract to delete
 * returns GenericResponse
 **/
exports.deleteCommunity = function(user, id) {
  console.log('deleteCommunity user=', user);
  console.log('deleteCommunity id=', id);
  return new Promise(function(resolve, reject) {
    return esUtils.deleteDoc(communityIndex, ccType, id) //delete community
      .then( resp => {
        if (resp === null || resp === undefined || resp.result !== "deleted") //failed to delete doc
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: deleteCommunity failed - delete response does not have body"} )

        console.log("deleteCommunity succeeded id=", id);
        return doResponse( resolve, 200, {"success": true} ); //Success
      } )
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: deleteCommunity failed - " + error.message } );
      })
  });// Promise close
}
//????????????????????????????????????????????????????? Delete Community - should it delete groups &/or members ??????????????????????????????????????????????????????????????


/**
 * Get an array of group objects in the specified Community
 * 
 *
 * id String contract whose groups to retrieve
 * regulated Boolean regulated flag (optional)
 * returns List
 **/
exports.getCommunityGroups = function(user, id, regulated) {
  console.log('getCommunityGroups user=', user);
  console.log('getCommunityGroups id=', id);
  console.log('getCommunityGroups regulated=', regulated);
  return new Promise(function(resolve, reject) {
    var query;
    if (regulated === undefined) {
      query = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"@community.keyword" : id}
           }
        }
      };
    } else {
      query = {
          "constant_score" : { 
             "filter" : {
                "bool" : {
                  "must" : [
                     { "term" : {"regulated" : regulated}}, 
                     { "term" : {"@community.keyword" : id}} 
                  ]
               }
             }
          }
       };
    }
    return esUtils.checkIfExistInSearch(groupIndex, ccType, query, true) //search for groups
      .then ( groups => {
        if (!groups) // no groups matched
          return doResponse( resolve, 200, [] ); //Success
          //return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityGroups no groups matched"} );

        //console.log("getCommunityGroups groups=", groups);
        var groupsObj = [];
        var groupObj = {};
        console.log("getCommunityGroups groups.length=", groups.length);
        for (let i = 0; i < groups.length; i++){
          let group = groups[i];
          //console.log("getCommunityGroups succeeded group=", group);
          let groupObj = {
            "@context": schema.contextSchema,
            "@type": schema.groupSchemeType,
            "name": group._source.name,
            "identifier": group._source.identifier,
            "regulated": group._source.regulated,
            "leiCode":group._source.leiCode,
            "id": group._id
          };
          //console.log("getCommunityGroups groupObj=", groupObj);
          groupsObj.push(groupObj);
        } //end for
        return doResponse( resolve, 200, groupsObj ); //Success
      })  //end search for groups
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityGroups failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Get an array of member objects in the specified Community
 * 
 *
 * id String contract whose members to retrieve
 * regulated Boolean returns only reguletad or not reguleted members (optional)
 * returns List
 **/
exports.getCommunityMembers = function(user, id, regulated) {
  // one new filed "email" - minor change to do in the future
  console.log('getCommunityMembers user=', user);
  console.log('getCommunityMembers id=', id);
  console.log('getCommunityMembers regulated=', regulated);
  return new Promise(function(resolve, reject) {
    let query = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"memberOf.keyword" : id}
           }
        }
      };

    return esUtils.checkIfExistInSearch(memberIndex, ccType, query, true) //search for members
      .then ( members => {
        if (!members) // no members matched
          return doResponse( resolve, 200, [] )
          //return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityMembers no members matched"} );

        //console.log("getCommunityMembers members=", members);
        let membersObj = [];
        console.log("getCommunityMembers members.length=", members.length);

        const processMember = function(member, finalRegulated){
          return new Promise(function(resolve, reject) {
            //console.log("getCommunityMembers processMember finalRegulated", finalRegulated);
            let include = false;
            if (regulated === undefined)
              include = true;
            else if (regulated && finalRegulated)
              include = true;
            else if ((!regulated) && (!finalRegulated))
              include = true;
            //console.log("getCommunityMembers processMember include", include);

            let memberObj = {};
            if (include) {
              memberObj = {
                "@context": schema.contextSchema,
                "@type": schema.memberSchemeType,
                "id": member._id,
                "address": {
                  "@context": "http://schema.org",
                  "@type": "Place",
                  "addressLocality": member._source.address.addressLocality,
                  "addressRegion": member._source.address.addressRegion,
                  "postalCode": member._source.address.postalCode,
                  "streetAddress": member._source.address.streetAddress
                },
                "identifier": member._source.identifier,
                "regulated": finalRegulated,
                //"familyName": member._source.familyName,
                //"givenName": member._source.givenName,
                "name": member._source.name,
                "memberOf": member._source.memberOf,
                "groups": member._source.groups,
                "retentionHold": member._source.retentionHold,
                "additionalProperty": member._source.additionalProperty
              };

              membersObj.push(memberObj);
              //console.log("getCommunityMembers processMember membersObj size ", membersObj.length, memberObj.id, memberObj.name);
            } //end if (include)

            return resolve(membersObj);

          }); //end promise
        } //end function

        let groupQuery = {
          "constant_score" : { 
             "filter" : { 
                "term" : {"@community.keyword" : id}
             }
          }
        };
        return esUtils.checkIfExistInSearchLimitResult(groupIndex, ccType, groupQuery, true, ["regulated"]) //search for groups and cache them
          .then ( groupsCache => {
            if (!groupsCache) // no groups matched
              return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityMembers no groups matched"} );

            console.log("getCommunityMembers groupsCache.length=", groupsCache.length);

            let groupsObj = {};
            for ( let g = 0; g < groupsCache.length; g++){
              groupsObj[ groupsCache[g]['_id'] ] = groupsCache[g]['_source'].regulated;
            };

            let j = 0;  //flag to wait for the async calls in for loop code to finish executing
            for (let i = 0; i < members.length; i++){
              let member = members[i];
              //console.log("getCommunityMembers member=", member);
              let finalRegulated = false;
              for ( let grID of member._source.groups ){
                if (groupsObj[grID] === true){
                  finalRegulated = true;
                  break
                }
              };

              processMember(member, finalRegulated)
                .then ( resp => {
                  //console.log("getCommunityMembers after processMember membersObj.length", membersObj.length);
                  if (++j === members.length) {
                    console.log("getCommunityMembers after last processMember membersObj.length", membersObj.length);
                    return doResponse( resolve, 200, membersObj ); //Success
                  }
                })
                .catch( error => {
                  console.log("getCommunityMembers ERROR: Error occurred so determining regulated as false for member ", member._id, member._source.name);
                  return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityMembers failed - " + error.message } );
                }) //end processMember

            } //end for

            //console.log("getCommunityMembers after for loop ***********************************");
            //console.log("getCommunityMembers membersObj.length", membersObj.length);
            //return doResponse( resolve, 200, membersObj ); //Success

          })  //end search for groups and cache them
          .catch( error => {
            return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityMembers failed - " + error.message } );
          })

      })  //end search for members
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityMembers failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Returns single community contract object at the supplied contract address
 * 
 *
 * id String contract to whose object data to get
 * returns CommunitySchema
 **/
exports.getCommunityObj = function(user, id) {
  console.log('getCommunityObj user=', user);
  console.log('getCommunityObj id=', id);
  return new Promise(function(resolve, reject) {
    let communityQuery = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
     };
    return esUtils.checkIfExistInSearch(communityIndex, ccType, communityQuery) //search for community
      .then( community => {
        if (!community) // no such community 
          return doResponse( reject, 404, { "success": false, "error": "ERROR: getCommunityObj failed - Community not found"} );

        console.log("getCommunityObj succeeded community=", community);
        let communityObj = {
          "funder": community._source.funder,
          "@context": schema.contextSchema,
          "@type": schema.communitySchemeType,
          "name": community._source.name,
          "groups": community._source.groups,
          "id": community._id
        };
        console.log("getCommunityObj communityObj=", communityObj);
        return doResponse( resolve, 200, communityObj ); //Success
      } )
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityObj failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Returns all community contract objects
 * 
 *
 * returns List
 **/
exports.getCommunityObjAll = function(user) {
  console.log('getCommunityObjAll user=', user);
  return new Promise(function(resolve, reject) {
    return esUtils.checkIfExistInSearch(communityIndex, ccType, { match_all: {} }, true) //search for community
      .then( communities => {
        if (!communities) // no communities 
          return doResponse( resolve, 200, [] )
          //return doResponse( reject, 404, { "success": false, "error": "ERROR: getCommunityObjAll failed - Communities not found"} );

        //console.log("getCommunityObjAll communities=", communities);
        var communitiesObj = [];
        var communityObj = {};
        console.log("getCommunityObjAll communities.length=", communities.length);
        for (let i = 0; i < communities.length; i++){
          let community = communities[i];
          //console.log("getCommunityObjAll succeeded community=", community);
          let communityObj = {
            "funder": community._source.funder,
            "@context": schema.contextSchema,
            "@type": schema.communitySchemeType,
            "name": community._source.name,
            "groups": community._source.groups,
            "id": community._id
          };
          //console.log("getCommunityObjAll communityObj=", communityObj);
          communitiesObj.push(communityObj);
        } //end for
        return doResponse( resolve, 200, communitiesObj ); //Success
      } )
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityObjAll failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Is group regulated
 * 
 *
 * id String contract whose regulated status to retrieve
 * group String contract whose regulated status to retrieve
 * returns inline_response_200
 **/
exports.getCommunityRegulatedGroup = function(user, id, group) {
  console.log('getCommunityRegulatedGroup user=', user);
  console.log('getCommunityRegulatedGroup id=', id);
  console.log('getCommunityRegulatedGroup group=', group);
  return new Promise(function(resolve, reject) {
    let query = {
          "constant_score" : { 
             "filter" : {
                "bool" : {
                  "must" : [
                     { "term" : {"_id" : group}}, 
                     { "term" : {"@community.keyword" : id}} 
                  ]
               }
             }
          }
       };
    return esUtils.checkIfExistInSearch(groupIndex, ccType, query) //search for group
      .then ( group => {
        if (!group) // no group matched
          return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityRegulatedGroup no group matched"} );

        //console.log("getCommunityRegulatedGroup group=", group);
        var ret = {"regulated": group._source.regulated};
        //console.log("getCommunityRegulatedGroup ret=", ret);
        return doResponse( resolve, 200, ret ); //Success
      })  //end search for group
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityRegulatedGroup failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Update contract with group
 * 
 *
 * id String contract to be updated with group
 * body Group data to update in contract
 * returns GroupSchema
 **/
exports.updateCommunityGroup = function(user, id, body) {
  //console.log('updateCommunityGroup user=', user);
  console.log('updateCommunityGroup id=', id);
  console.log('updateCommunityGroup body=', body);
  return new Promise(function(resolve, reject) {
    let communityQuery = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
    };

    return esUtils.checkIfExistInSearch(communityIndex, ccType, communityQuery) //search for community
      .then( community => {
        if (!community) // no such community 
          return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityGroup failed - Community not found"} )

        let query = {
          "constant_score" : { 
             "filter" : {
                "bool" : {
                  "must" : [
                     { "term" : {"identifier.keyword" : body.identifier}}, 
                     { "term" : {"@community.keyword" : id}} 
                  ]
               }
             }
          }
        };

        return esUtils.checkIfExistInSearch(groupIndex, ccType, query) //search for group
          .then ( group => {
            if (group) // group already exist 
              return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityGroup failed - Group already exists", "id": group._id } );

            let groupObj = {
              "name": body.name,
              "identifier": body.identifier,
              "leiCode": body.leiCode,
              "regulated": body.regulated,
              "@community": id
            };
            return esUtils.add(groupIndex, ccType, groupObj)  //create new group
              .then( resp => {
                //console.log('updateCommunityGroup resp=', JSON.stringify(resp, null, 4));
                if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
                  return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityGroup failed - New Group not created" } );

                const newGroupID = resp._id;
                const updateComBody =  {
                  script: {
                    source: "ctx._source.groups.add(params.group)",
                    params: {group: newGroupID},
                    lang: "painless"
                  }
                };
    
                groupObj.id           = newGroupID; //add new group id to final JSON response
                groupObj["@context"]  = schema.contextSchema;
                groupObj["@type"]     = schema.groupSchemeType;

                return esUtils.update( communityIndex, ccType, id,  updateComBody ) //link community to newly created group 
                  .then( respUpdComm  => {
                    //console.log('updateCommunityGroup resp=', JSON.stringify(respUpdComm, null, 4));
                    if (respUpdComm === null || respUpdComm === undefined || respUpdComm.result !== "updated") //failed to update existing doc
                      return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityGroup failed - Updating Community with New Group failed" } );

                    return doResponse( resolve, 200, groupObj ); //Success
                  })
              });
          });
      } )
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityGroup failed - " + error.message } );
      })
  });// Promise close
}
