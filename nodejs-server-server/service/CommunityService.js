'use strict';

//const esClient = require('../elastic/ElasticSearchConnection').esClient;
const web3              = require('../blockchain/Web3Connection').web3;
const unlockAccount     = require('../blockchain/Personal').unlockAccount;

const communityBytes    = require('../blockchain/CommunityDetails').communityBytes;
const communityAbi      = require('../blockchain/CommunityDetails').communityAbi;
const communityRegistry = require('../blockchain/CommunityDetails').communityRegistry

const registryBytes     = require('../blockchain/RegistryDetails').registryBytes;
const registryAbi       = require('../blockchain/RegistryDetails').registryAbi;

const subgroupBytes     = require('../blockchain/SubgroupDetails').subgroupBytes;
const subgroupAbi       = require('../blockchain/SubgroupDetails').subgroupAbi;

const memberBytes       = require('../blockchain/MemberDetails').memberBytes;
const memberAbi         = require('../blockchain/MemberDetails').memberAbi;

const utils             = require('../blockchain/Utils');

const startBlock        = require('config').get('Storage.startBlock');

//const ResponsePayload   = require('../utils/writer').respondWithCode;
const doResponse        = require('../utils/response').doResponse;
const schema            = require('../utils/schema')
const _                 = require('lodash');


const communityContract =  web3.eth.contract(communityAbi);
const communitySample   =  communityContract.at('0x0000000000000000000000000000000000000000');


const groupContract     =  web3.eth.contract(subgroupAbi);
const groupSample       =  groupContract.at('0x0000000000000000000000000000000000000000');

const memberContract    =  web3.eth.contract(memberAbi);
const memberSample      =  memberContract.at('0x0000000000000000000000000000000000000000');
/**
 * Update contract with member
 * 
 *
 * id String contract to be updated with member
 * body Member data to update in contract
 * returns MemberSchema
 **/
exports.addCommunityMember = (user, id, body) => new Promise( (resolve, reject) => {

  // one new filed "email" - minor change to do in the future
  console.log('addCommunityMember user=', user);
  console.log('addCommunityMember id=', id);
  console.log('addCommunityMember body=', body); 
  if (!body.hasOwnProperty('identifier')) //input identifier is mandatory
    return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed - Member identifier not supplied"} )
  try {
  const create = () => {
    const communityInstance  =  communityContract.at(id);
    if (communityInstance.exist() !== true)
       return doResponse( reject, 404, { "success": false, "error": "ERROR: getCommunityObj failed - Community not found"} );

   
    let _account        = body.account;
    let _groups         = body.groups;
    let _retentionHold  = body.retentionHold;
    let _identifier     = body.identifier;
   
    if (!_account)
      _account = user.account;// Create Account !!
    
    if (!_groups)
      _groups = [];

    if (_retentionHold === undefined)
      _retentionHold = false;

    console.log("  _account, _identifier, _retentionHold, _groups");
    console.log(  _account, _identifier, _retentionHold, _groups);
    communityInstance.addMember(
            _account,
            _identifier,
            _retentionHold,
            _groups,
            web3.fromAscii( body.name ),
            {
             from   : user.account, 
             data   : communityBytes, 
             gas    : '9700000'
            }, (e, transactionHash) => {
                //console.log(e, transactionHash);
                if (e)
                  return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed - " + e.message} );
      

                const receipt                    = web3.eth.getTransactionReceipt(transactionHash);
                const newMemberBlockNumber       = receipt.blockNumber;
                const newCommunityMemberTopic    = web3.sha3('MemberAdded(address)');
                const getCommunityMemberFilter   = web3.eth.filter({fromBlock: newMemberBlockNumber, address: communityInstance.address, toBlock: 'latest', topics:[ newCommunityMemberTopic ] });

                getCommunityMemberFilter.get( (error, results) => {
                  if (error)
                    return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed - " + error.message} );
      
                  if (results.length === 0)
                    return doResponse( reject, 500, { "success": false, "error": "ERROR: addCommunityMember failed" } );

                  const event          = communitySample.MemberAdded().formatter(results[0]);
                  const memberAddress  = event.args.member;
                  const memberInstance = memberContract.at(memberAddress);
                  const details        = memberInstance.getDetails();

                  const output = {
                    "@context"      : schema.contextSchema,
                    "@type"         : schema.memberSchemeType,
                    "id"            : memberInstance.address,
                    "name"          : utils.getClearString(details[1], true),
                    "identifier"    : details[2],
                    "regulated"     : details[5],
                    "retentionHold" : details[4],
                    "groups"        : []
                  };

                  const groups = memberInstance.getGroups();

                  for (let i = 0; i < groups.length; i++){
                    output.groups.push({
                      "@context"      : schema.contextSchema,
                      "@type"         : schema.groupSchemeType,
                      "@id"           : groups[i]
                    })
                  }
                  return doResponse( resolve, 200, output ); 
                })
            });
    };
    const unlock = () => {
      return unlockAccount(user.account, user.passphrase)
        .then( ( unlockResponse ) => {
          if (unlockResponse !== true)
            return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - " + unlockResponse} )

          return create();
        })
        .catch( ( unlockError ) => {
          return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - " + error.message} )
        });
    };

    if (user.account && user.passphrase && user.passphrase.length > 0)
      return unlock();
    else 
      return create();

  }catch(error){
    console.log(error)
    return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - " + error.message} )

  }
});// Promise close

/**
 * Add a new community contract
 * 
 *
 * body Community Community contract that needs to be added to the BlockChain
 * returns CommunitySchema
 **/
exports.createCommunity = (user, body) => {
  console.log('createCommunity user=', user);
  console.log('createCommunity body=', body);
  return new Promise( (resolve, reject) => {
      const create = () => {
        //
        const _name               = web3.fromAscii(body.name);
        const _registry           = '0x0000000000000000000000000000000000000000';

        let beenHere              = false;
        const _community          = communityContract.new(
           _name,
           _registry,
           {
             from   : user.account, 
             data   : communityBytes, 
             gas    : '9900000'
           },   (e, contract) => {
            console.log(e, contract);

            if (beenHere === true)
              return

            if (e){
                 return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - " + e.message} )
            };

            let receipt = contract._eth.getTransactionReceipt(contract.transactionHash);
            if (receipt.contractAddress){
              //
              beenHere = true
              contract = communityContract.at(receipt.contractAddress);
              const communityResponse = {
                  "@context"    : schema.contextSchema,
                  "@type"       : schema.communitySchemaType,
                  "id"          : contract.address,
                  "name"        : utils.getClearString(contract.getName(), true),
                  "funder"  : {
                    "@context"    : schema.contextSchema,
                    "@type"       : schema.memberSchemeType,
                    "@id"         : contract.getOwner(), //  << should be just ID of user
                  },
                  "groups"      : []  //holds ids as text, not as object  
                };

                const groups = contract.getGroups( false )
                for (let i = 0; i < groups.length; i++){
                  communityResponse.groups.push({
                      "@context"      : schema.contextSchema,
                      "@type"         : schema.groupSchemeType,
                      "@id"           : groups[i]
                    })
                };
                return doResponse( resolve, 200, communityResponse ); //Success // << TO DO GET MEMBERS 
              //
            };
         })
      };
      const unlock = () => {
        return unlockAccount(user.account, user.passphrase)
          .then( ( unlockResponse ) => {
            if (unlockResponse !== true)
              return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - " + unlockResponse} )

            return create();
          })
          .catch( ( unlockError ) => {
            return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - " + error.message} )
          });
      };
 
      if (user.account && user.passphrase && user.passphrase.length > 0)
        return unlock();
      else 
        return create();



  });// Promise close
}


/**
 * Deletes a community contract
 * 
 *
 * id String contract to delete
 * returns GenericResponse
 **/
exports.deleteCommunity = (user, id) => {
  console.log('deleteCommunity user=', user);
  console.log('deleteCommunity id=', id);
  return new Promise( (resolve, reject) => {
   
  });// Promise close
}

/**
 * Get an array of group objects in the specified Community
 * 
 *
 * id String contract whose groups to retrieve
 * regulated Boolean regulated flag (optional)
 * returns List
 **/
exports.getCommunityGroups = (user, id, regulated) => {
  console.log('getCommunityGroups user=', user);
  console.log('getCommunityGroups id=', id);
  console.log('getCommunityGroups regulated=', regulated);
  return new Promise((resolve, reject) =>  {

      const communityInstance   =  communityContract.at(id);
      if (communityInstance.exist() !== true)
         return doResponse( reject, 404, { "success": false, "error": "ERROR: getCommunityObj failed - Community not found"} );

      let output = []
      let groups;
      
      if (typeof regulated === 'undefined')
        groups  = communityInstance.getGroups(false)// groups  = [communityInstance.getDefaultGroup()];
      else 
        groups  =  communityInstance.getGroups( regulated === true );


       const getGroup = ( index ) => {
        if (index === groups.length)
             return doResponse( resolve, 200, output ); //Success // << TO DO GET MEMBERS 

            /// DEV DEV
        if (groups[index] === '0xcc6a2dc7c9086d3d4c77ffd07b37481cb86e763c')
          return  getGroup(++index);
        /// DEV END

        let groupInstance = groupContract.at( groups[index] );


        if (regulated === false && groupInstance.isRegulated() === true)
          return getGroup(++index);

        let group = {
            "@context"  : schema.contextSchema,
            "@type"     : schema.groupSchemeType,
            "id"        : groupInstance.address,
            "name"      : utils.getClearString( groupInstance.getName(), true ),
            "regulated" : groupInstance.isRegulated(),
            "leiCode"   : utils.getClearString( groupInstance.getLeiCode(), true )
           
        };

        output.push( group );

        getGroup(++index);
      };

      return getGroup( 0 );

  });// Promise close
}


/**
 * Get an array of member objects in the specified Community
 * 
 *
 * id String contract whose members to retrieve
 * regulated Boolean returns only reguletad or not reguleted members (optional)
 * returns List
 **/
exports.getCommunityMembers = (user, id, regulated) => {
  // one new filed "email" - minor change to do in the future
  console.log('getCommunityMembers user=', user);
  console.log('getCommunityMembers id=', id);
  console.log('getCommunityMembers regulated=', regulated);
  return new Promise( (resolve, reject) => {
     
      const communityInstance   =  communityContract.at(id);
      if (communityInstance.exist() !== true)
         return doResponse( reject, 404, { "success": false, "error": "ERROR: getCommunityObj failed - Community not found"} );

      let members = [];
      let groups;

      if (typeof regulated === 'undefined')
        groups  =  [communityInstance.getDefaultGroup()];
      else 
        groups  =  communityInstance.getGroups( regulated === true );

      const getGroup = ( index ) => {
        if (index === groups.length){
          let output = [];
          for (let i = 0; i < members.length; i++){
              members               = _.uniq(members); 
              const memberInstance  = memberContract.at(members[i]);
              const details         = memberInstance.getDetails();

              const item = {
                "@context"      : schema.contextSchema,
                "@type"         : schema.memberSchemeType,
                "id"            : memberInstance.address,
                "name"          : utils.getClearString(details[1], true),
                "identifier"    : details[2],
                "regulated"     : details[5],
                "retentionHold" : details[4],
                "groups"        : []
              };

              // if (memberInstance.address === '0x870632ebde959ef4ee850759d266d817ec906b23')
              //  item.regulated = true;
              
               if (item.identifier === 'okemmis')
                item.identifier = 'oliver'
               else if (item.identifier === 'psaini'){
                item.identifier = 'gary'
                item.name = "Gary Shergill";
               }else if (item.identifier === 'cas')
                item.name = 'CAS-MDE'
              

              

              const groups = memberInstance.getGroups();

              for (let i = 0; i < groups.length; i++){
                item.groups.push( groups[i]);
                /*item.groups.push({
                  "@context"      : schema.contextSchema,
                  "@type"         : schema.groupSchemeType,
                  "@id"           : groups[i]
                })*/
              };
              output.push(item);
          }

          return doResponse( resolve, 200, output ); 
        }
         

        let groupInstance = groupContract.at( groups[index] );
        if (regulated === false && groupInstance.isRegulated() === true)
          return getGroup(++index);


        console.log('groupInstance.getMembers()', groupInstance.getMembers());
        members = members.concat(groupInstance.getMembers()); 

        getGroup(++index);
      };

      return getGroup( 0 );

  });// Promise close
}


/**
 * Returns single community contract object at the supplied contract address
 * 
 *
 * id String contract to whose object data to get
 * returns CommunitySchema
 **/
exports.getCommunityObj = (user, id) => {
  console.log('getCommunityObj user=', user);
  console.log('getCommunityObj id=', id);
  return new Promise( (resolve, reject) => {
    //
      const communityInstance   =  communityContract.at(id);
      if (communityInstance.exist() !== true)
         return doResponse( reject, 404, { "success": false, "error": "ERROR: getCommunityObj failed - Community not found"} );

        const groups      =  communityInstance.getGroups( false );
        const community   = {
          "@context"   : schema.contextSchema,
          "@type"      : schema.communitySchemeType,
          "id"         : communityInstance.address,
          "name"       : utils.getClearString(communityInstance.getName(), true),
          "funder"     : {
             "@context"   : schema.contextSchema,
             "@type"      : schema.memberSchemeType,
             "@id"        : communityInstance.getOwner()//getOwner()
          },
          "groups"     : []
        };
        for (let i = 0; i < groups.length; i++){
            community.groups.push(groups[i])
          /*community.groups.push({
            "@context"      : schema.contextSchema,
            "@type"         : schema.groupSchemeType,
            "@id"           : groups[i]
          })*/
        };
        return doResponse( resolve, 200, community ); //Success
      //const groups              =  communityInstance.getGroups( false );

    //
  });// Promise close
}


/**
 * Returns all community contract objects
 * 
 *
 * returns List
 **/
exports.getCommunityObjAll = (user) => {
  console.log('getCommunityObjAll user=', user);
  return new Promise( (resolve, reject) => {
    //
    const toBlock       = 'latest';// 5956; //latest
    const topic         = web3.sha3('NewCommunity(address,bytes32)');
    const getFilter     = web3.eth.filter({fromBlock: startBlock, toBlock: toBlock, topics: [topic] });
  
    getFilter.get( (error, results) => {
        if (error)
             return doResponse( reject, 500, { "success": false, "error": "ERROR: getCommunityObjAll failed - " + error.message} );
      
        let output = [];
        if (results){
        
          for (let i = 0; i < results.length; i++){
            const event  = communitySample.NewCommunity().formatter(results[i]);

            const communityInstance   =  communityContract.at(event.address);
            const groups              =  communityInstance.getGroups( false );

            let item = {
              "@context"   : schema.contextSchema,
              "@type"      : schema.communitySchemeType,
              "id"         : event.address,
              "name"       : utils.getClearString(event.args.name, true),
              "funder"     : {
                 "@context"   : schema.contextSchema,
                 "@type"      : schema.memberSchemeType,
                 "@id"        : event.args.owner
              },
              "groups"     : []
            }

           for (let i = 0; i < groups.length; i++){
              /*item.groups.push({
                "@context"      : schema.contextSchema,
                "@type"         : schema.groupSchemeType,
                "@id"           : groups[i]
              })*/
              item.groups.push( groups[i] );

            };


            output.push(item);
          };

        };
        return doResponse( resolve, 200, output ); //Success
    });
    //

  });// Promise close
}


/**
 * Is group regulated
 * 
 *
 * id String contract whose regulated status to retrieve
 * group String contract whose regulated status to retrieve
 * returns inline_response_200
 **/
exports.getCommunityRegulatedGroup = (user, id, group) => {
  console.log('getCommunityRegulatedGroup user=', user);
  console.log('getCommunityRegulatedGroup id=', id);
  console.log('getCommunityRegulatedGroup group=', group);
  return new Promise( (resolve, reject) => {
   
  });// Promise close
}


/**
 * Update contract with group
 * 
 *
 * id String contract to be updated with group
 * body Group data to update in contract
 * returns GroupSchema
 **/
exports.updateCommunityGroup = (user, id, body) => {
  console.log('updateCommunityGroup user=', user);
  console.log('updateCommunityGroup id=', id);
  console.log('updateCommunityGroup body=', body);
  return new Promise( (resolve, reject) => {
    //
    if (!body.hasOwnProperty('identifier')) //input identifier is mandatory
      return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityGroup failed - Member identifier not supplied"} )

    const create = () => {
      const communityInstance  =  communityContract.at(id);
      if (communityInstance.exist() !== true)
         return doResponse( reject, 404, { "success": false, "error": "ERROR: updateCommunityGroup failed - Community not found"} );

      //( string _identifier, bytes32 _groupName,  bool _regulated ) onlyowner public returns ( address group ){
      communityInstance.addGroup(
              body.identifier,
              web3.fromAscii( body.name ),
              body.regulated,
              {
               from   : user.account, 
               data   : communityBytes, 
               gas    : '5700000'
              }, (e, transactionHash) => {
                  console.log(e, transactionHash);
                  if (e)
                    return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityGroup failed - " + e.message} );
                  //
                  const receipt                    = web3.eth.getTransactionReceipt(transactionHash);
                  const newGroupBlockNumber        = receipt.blockNumber;
                  const newGroupMemberTopic        = web3.sha3('NewCommunityGroup(address,string,bytes32,bool)');
                  const getCommunityGroupFilter    = web3.eth.filter({fromBlock: newGroupBlockNumber, address: communityInstance.address, toBlock: 'latest', topics:[ newGroupMemberTopic ] });
                  getCommunityGroupFilter.get( (error, results) => {
                    //console.log('error, results',error, results);
                    if (error)
                      return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityGroup failed - " + error.message} );
        
                    if (results.length === 0)
                      return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityGroup failed" } );

                    const event = communitySample.NewCommunityGroup().formatter(results[0]);
                    console.log('event',event);
                    let group = {
                        "@context"  : schema.contextSchema,
                        "@type"     : schema.groupSchemeType,
                        "id"        : event.address,
                        "identifier": event.args.identifier,
                        "name"      : utils.getClearString( event.args.name, true ),
                        "regulated" : event.args.regulated,
                        "leiCode"   : "",//utils.getClearString( groupInstance.getLeiCode(), true )
                       
                    };
                    return doResponse( resolve, 200, group ); //Success

                })
        })


    };
    const unlock = () => {
        return unlockAccount(user.account, user.passphrase)
          .then( ( unlockResponse ) => {
            if (unlockResponse !== true)
              return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - " + unlockResponse} )

            return create();
          })
          .catch( ( unlockError ) => {
            return doResponse( reject, 500, { "success": false, "error": "ERROR: createCommunity failed - " + error.message} )
          });
    };
 
    if ( user.account && user.passphrase && user.passphrase.length > 0 )
      return unlock();
    else 
      return create();
      
    //
  });// Promise close
}

















