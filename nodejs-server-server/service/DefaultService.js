'use strict';

const doResponse        = require('../utils/response').doResponse;
const apv         		= require('appversion');


/**
 * Get API version
 * 
 *
 * returns String
 **/
exports.getNexusVersion = function() {
  return new Promise(function(resolve, reject) {
  	try {
		let obj = {
			"version": apv.composePatternSync('M.m.p'),
			"build": {
				"date": apv.composePatternSync('d'),
        		"number": apv.composePatternSync('n'),
			}
		}

		return doResponse( resolve, 200,  obj); 
	}catch(error){
		console.log(error);
 		return doResponse( reject, 500, { "success": false, "error": "ERROR: getNexusVersion failed - " + error.message} );
	}
  });
}

