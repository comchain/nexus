'use strict';

const doResponse          = require('../utils/response').doResponse;
const schema              = require('../utils/schema');
const _                   = require('lodash');
let catchUpTime = 0;
/**
 * Add a new extraData object
 * 
 *
 * body ExtraData MiFID2 template
 * returns ExtraDataSchema
 **/
exports.createExtraData = function(user, body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
      "@context"            : "@context",
      "@type"               : "@type",
      "id"                  : "id",
      "storagePeriod"       : 0,
      "refreshPeriod"       : 60000,
      "audit"               : [ "telephony", "telephony" ]
     
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Deletes a ExtraData object
 * 
 *
 * id String ExtraData to delete
 * returns GenericResponse
 **/
exports.deleteExtraData = function(user, id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "success" : true
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Returns all MiFID2 objects
 * 
 *
 * returns List
 **/
exports.getExtraDataAll = function(user, identifier) {
  return new Promise(function(resolve, reject) {
    var output = [{
      "@context"      : schema.contextSchema,
      "@type"         : schema.extraDataSchemeType,
      "id"            : "0xdA35deE8EDDeAA556e4c26268463e26FB91ff749",
      "identifier"    : "TANGO",
      "storagePeriod" : 7,
      "refreshPeriod" : 1000 * 30,
      "catchUpTime"   : catchUpTime,
      "audit"         : [
        "telephony"
      ],
      "additionalProperty": [ ]
    } ];

    return doResponse( resolve, 200, output );

  });
}


/**
 * Update a new extraData object
 * 
 *
 * id String contract to be updated with who is responsible for providing data for fields
 * body ExtraData data to update in contract
 * returns ExtraDataSchema
 **/
exports.updateExtraData = function(user,id,body) {
  return new Promise(function(resolve, reject) {
    catchUpTime = body.catchUpTime;
    return doResponse( resolve, 200, { "success": true } );
  });
}

exports.updateExtraDataProperty = function(user,id,body) {

}

