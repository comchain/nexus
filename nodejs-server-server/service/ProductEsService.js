'use strict';

//const esClient = require('../elastic/ElasticSearchConnection').esClient;
const ccType              = require('../elastic/ElasticSearchConnection').ccType;
const esUtils             = require('../elastic/EsUtils');
const groupIndex          = require('../elastic/ElasticSearchConnection').groupIndex;
const memberIndex         = require('../elastic/ElasticSearchConnection').memberIndex;
const productIndex        = require('../elastic/ElasticSearchConnection').productIndex;
const MDERecordIndex      = require('../elastic/ElasticSearchConnection').MDERecordIndex;
const ITSRecordIndex      = require('../elastic/ElasticSearchConnection').ITSRecordIndex;
const ResponsePayload     = require('../utils/writer').respondWithCode;
const timeUtlis           = require('../utils/time');
const _                   = require('lodash');
const schema              = require('../utils/schema')
/**
 * Add a new Product contract
 * 
 *
 * body Product Product contract that needs to be added to the BlockChain
 * returns ProductSchema
 **/

exports.createProduct = function(user, body) {
  //console.log('createProduct user=', user);
  console.log('createProduct body=', body);
  return new Promise(function(resolve, reject) {

    if (!body.hasOwnProperty('identifier')) //input identifier is mandatory
      return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: createProduct failed - Product identifier not supplied"} )

    let productQuery = {
            "constant_score" : { 
               "filter" : { 
                  "term" : {"identifier.keyword" : body.identifier}
               }
            }
         };
    return esUtils.checkIfExistInSearch(productIndex, ccType, productQuery) //search for product
      .then(product => {
        if (product) //product already exists
          return esUtils.doResponse( reject, 400, { "success": false, "error": "ERROR: createProduct failed - Product already exists", "id": product._id} )

        var productObj = {
          "identifier" : body.identifier,
          "additionalProperty" : body.additionalProperty,
          "extraData" : body.extraData,
          "comchain" : body.comchain   || body.comChain,
          "owner" : body.owner, // MUST BE { "@id": "", regulated: bool }
          "category" : body.category
        };

        console.log('createProduct productObj=', productObj);
        return esUtils.add(productIndex, ccType, productObj)  //create new product
          .then( resp => {
              console.log('createProduct resp=', JSON.stringify(resp, null, 4));
              if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
                return esUtils.doResponse( reject, 400, { "success": false, "error": "ERROR: createProduct failed."} )

              productObj.id           = resp._id;     //add new product id to final JSON response
              productObj["@type"]     =  schema.productSchemaType
              productObj["@context"]  = schema.contextSchema


              let additionalProperty = body.additionalProperty; //add bits to additionalProperty for final JSON response
              for (var i = 0; i < productObj.additionalProperty.length; i++) {
                let addProp = additionalProperty[i];
                addProp["@context"] = "http://schema.org";
                addProp["@type"] = "PropertyValue";
                productObj.additionalProperty[i] = addProp;
              }

              let extraData = {"@id": body.extraData}; //add bits to extraData for final JSON response
              extraData["@context"] = schema.contextSchema;
              extraData["@type"] = schema.extraDataSchemeType;
              productObj.extraData = extraData;

              let comchain = {"@id": body.comchain || body.comChain}; //add bits to extraData for final JSON response
              comchain["@context"]  = schema.contextSchema;
              comchain["@type"] = schema.comChainSchemeType;
              productObj.comchain = comchain;

              let owner = [];
              for (let i = 0; i < body.owner.length; i++){
                owner[i] = { 
                  "@id"       : body.owner[i]['@id'],
                  "@context"  : schema.contextSchema,
                  "@type"     : schema.memberSchemeType,
                  'regulated' : body.owner[i]['regulated']
                };
              };

              productObj.owner = owner;

              return esUtils.doResponse( resolve, 200, productObj ); //Success
          }); //end create new product
      } ) //end search for product
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: createProduct failed - " + error.message } );
      })
  }); // Promise close
}


/**
 * Deletes a Product contract
 * 
 *
 * id String contract to delete
 * returns GenericResponse
 **/
exports.deleteProduct = function(user, id) {
  console.log('deleteProduct user=', user);
  console.log('deleteProduct id=', id);
  return new Promise(function(resolve, reject) {
    return esUtils.deleteDoc(productIndex, ccType, id) //delete product
      .then( resp => {
        if (resp === null || resp === undefined || resp.result !== "deleted") //failed to delete doc
          return esUtils.doResponse( reject, 400, { "success": false, "error": "ERROR: deleteProduct failed."} )

        console.log("deleteProduct succeeded id=", id);
        return esUtils.doResponse( resolve, 200, {"success": true} ); //Success
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: deleteProduct failed - " + error.message } );
      })
  });// Promise close
}
//????????????????????????????????????????????????????? Delete Product - should it delete ExtraData ??????????????????????????????????????????????????????????????


/**
 * Get all metadata from contract
 * 
 *
 * id String contract whose metadata to retrieve
 * provider String provider whose metadata to retrieve in contract (optional)
 * returns List
 **/
exports.getProductAllMetadata = function(user, id, provider, size, offset, starttime, endtime, recordType ){ //(user, id,provider) {

  console.log('getProductAllMetadata user=', user);
  console.log('getProductAllMetadata id=', id); //product id
  console.log('getProductAllMetadata provider=', provider);
  console.log(" size, offset, starttime, endtime ",  size, offset, starttime, endtime );
  return new Promise(async(resolve, reject) => {

    const termArray = [ { "term" : {"product.@id.keyword" : id}} ];
    if (typeof provider !== 'undefined')
      termArray.push(   { "term" : {"provider.@id.keyword" : provider}} )

    if (typeof recordType !== 'undefined')
      termArray.push(   { "term" : {"fields.type.keyword" : recordType}} )

    

    if (typeof starttime !== 'undefined' && typeof endtime !== 'undefined'){
      console.log("starttime && endtime",starttime && endtime);
        termArray.push( 
     {
            "range" : {
              "@timestamp" : {
                "gte": starttime,
                "lte": endtime,
                      //"format": "yyyy-MM-dd||yyyy",
                      //"time_zone": time_zone
              }
            }
         })
    }
    const query = {
          "constant_score" : { 
             "filter" : {
                "bool" : {
                  "must" :  termArray
                  
               }
             }
          }
       };

    console.log('query', JSON.stringify(query));
    console.log('typeof size',size, typeof size);

    const genResponse = (MDERecords) => {
      var MDERecordsObj = [];
        var MDERecordObj = {};
        console.log("getProductAllMetadata MDERecords.length=", MDERecords.length);
        for (let i = 0; i < MDERecords.length; i++){
          let MDERecord = MDERecords[i];
          //console.log("getProductAllMetadata MDERecord=", MDERecord);
          let MDERecordObj = {
            "fields": MDERecord._source.fields,
            "id": MDERecord._id,
            "product": MDERecord._source.product,
            "provider": MDERecord._source.provider
          };
          //console.log("getProductAllMetadata MDERecordObj=", MDERecordObj);

          MDERecordObj.provider["@type"] = schema.memberSchemeType;
          MDERecordObj.provider["@context"] = schema.contextSchema;
          MDERecordObj.product["@type"] = schema.productSchemeType;
          MDERecordObj.product["@context"] = schema.contextSchema;

          MDERecordsObj.push(MDERecordObj);
        } //end for
        return esUtils.doResponse( resolve, 200, MDERecordsObj ); //Success
    }

    if (typeof size === 'number' && size > 0){
      console.log('getProductAllMetadata before scrollSearch', new Date());
      let searchResponse =  await esUtils.scrollSearch(MDERecordIndex, ccType, query, size);
      console.log('getProductAllMetadata searchResponse length', searchResponse.length, new Date());
      return genResponse(searchResponse);

    }

    return esUtils.checkIfExistInSearch(MDERecordIndex, ccType, query, true) //search for MDERecords
      .then ( MDERecords => {
        if (!MDERecords) // no MDERecords matched
           return esUtils.doResponse( resolve, 200, [] );
          //return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata no MDERecords matched"} );

        //console.log("getProductAllMetadata MDERecords=", MDERecords);
        genResponse(MDERecords);
      })  //end search for MDERecords
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata failed - " + error.message } );
      })
  });// Promise close
}

/**
 * Get single Product contract object
 * 
 *
 * id String contract whose data to get
 * returns ProductSchema
 **/
exports.getProductObj = function(user, id) {
  console.log('getProductObj user=', user);
  console.log('getProductObj id=', id);
  return new Promise(function(resolve, reject) {
    let productQuery = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
     };
    return esUtils.checkIfExistInSearch(productIndex, ccType, productQuery) //search for product
      .then( product => {
        if (!product) // no such product 
          return esUtils.doResponse( reject, 404, { "success": false, "error": "ERROR: getProductObj failed - Product not found"} );

        //console.log("getProductObj product=", product);
        let productObj = {
          "identifier" : product._source.identifier,
          "additionalProperty" : product._source.additionalProperty,
          "extraData" : product._source.extraData,
          "comChain" : product._source.comChain,
          "@type": schema.productSchemeType,
          "owner" : product._source.owner,
          "category" : product._source.category,
          "@context" : schema.contextSchema,
          "id": product._id
        };
        console.log("getProductObj 1 productObj=", productObj);

        let additionalProperty = product._source.additionalProperty; //add bits to additionalProperty for final JSON response
        for (var i = 0; i < productObj.additionalProperty.length; i++) {
          let addProp = additionalProperty[i];
          addProp["@context"] = "http://schema.org";
          addProp["@type"] = "PropertyValue";
          productObj.additionalProperty[i] = addProp;
        };

        let extraData = {"@id": product._source.extraData}; //add bits to extraData for final JSON response
        extraData["@context"] = schema.contextSchema;
        extraData["@type"]    = schema.extraDataSchemeType;
        productObj.extraData  = extraData;

        let comChain = {"@id": product._source.comChain}; //add bits to extraData for final JSON response
        comChain["@context"]  = schema.contextSchema;
        comChain["@type"]     = schema.comChainSchemeType;
        productObj.comChain   = comChain;


         let owner;
          if ( product._source.owner instanceof Array === true ){
            owner = [];
            for (let i = 0; i < product._source.owner.length; i++){
              owner[i] = {
                "@id"          : product._source.owner[i]['@id'],
                "@context"     : schema.contextSchema,
                "@type"        : schema.memberSchemeType,
                'regulated'    : product._source.owner[i]['regulated']
              }; //add bits to owns for final JSON response
              productObj.owner = owner;
            };
          }else {
            owner = product._source.owner
          };

        console.log("getProductObj 2 productObj=", productObj);
        return esUtils.doResponse( resolve, 200, productObj ); //Success
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getProductObj failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Get all products contract objects
 * 
 *
 * returns List
 **/
exports.getProductObjAll = function(user) {
  console.log('getProductObjAll user=', user);
  return new Promise(function(resolve, reject) {
    return esUtils.checkIfExistInSearch(productIndex, ccType, { match_all: {} }, true) //search for product
      .then( products => {
        if (!products) // no products 
          return esUtils.doResponse( resolve, 200, [] );
         // return esUtils.doResponse( reject, 404, { "success": false, "error": "ERROR: getProductObjAll failed - Products not found"} );

        //console.log("getProductObjAll products=", products);
        var productsObj = [];
        var productObj = {};
        //console.log("getProductObjAll products.length=", products.length);
        for (let j = 0; j < products.length; j++){
          let product = products[j];
          //console.log("getProductObjAll succeeded product=", product);
          let productObj = {
            "identifier" : product._source.identifier,
            "additionalProperty" : product._source.additionalProperty,
            "extraData" : product._source.extraData,
            "comChain" : product._source.comChain,
            "@type": schema.productSchemeType,
            "owner" : product._source.owner, // << MUST contain {"@id": '', "regulated": bool }
            "category" : product._source.category,
            "@context" : schema.contextSchema,
            "id": product._id
          };
          //console.log("getProductObjAll productObj=", productObj);

          let additionalProperty = product._source.additionalProperty; //add bits to additionalProperty for final JSON response
          for (var i = 0; i < productObj.additionalProperty.length; i++) {
            let addProp         = additionalProperty[i];
            addProp["@context"] = "http://schema.org";
            addProp["@type"]    = "PropertyValue";
            productObj.additionalProperty[i] = addProp;
          };

          let extraData = {"@id": product._source.extraData}; //add bits to extraData for final JSON response
          extraData["@context"] = schema.contextSchema;
          extraData["@type"]    = schema.extraDataSchemeType;
          productObj.extraData  = extraData;
          
          let comChain = {"@id": product._source.comChain}; //add bits to extraData for final JSON response
          comChain["@context"]  = schema.contextSchema;
          comChain["@type"]     = schema.comChainSchemeType;
          productObj.comChain   = comChain;

          let owner;
          if ( product._source.owner instanceof Array === true ){
            owner = [];
            for (let i = 0; i < product._source.owner.length; i++){
              owner[i] = {
                "@id"          : product._source.owner[i]['@id'],
                "@context"     : schema.contextSchema,
                "@type"        : schema.memberSchemeType,
                "regulated"    : product._source.owner[i]['regulated']
              }; //add bits to owns for final JSON response
              productObj.owner = owner;
            };
          }else {
            owner = product._source.owner
          };

          //console.log("getProductObjAll productObj=", productObj);
          productsObj.push(productObj);
        } //end for
        return esUtils.doResponse( resolve, 200, productsObj ); //Success
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getProductObjAll failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Update contract with metadata
 * 
 *
 * id String contract to be updated with metadata
 * body MIFID2Schema data to update in contract
 * returns MetaDataSchema
 **/
exports.updateProductMetadata = function(user,id,body) {
  console.log('updateProductMetadata user=', user);
  console.log('updateProductMetadata id=', id);
  console.log('updateProductMetadata body=', body);
  return new Promise(function(resolve, reject) {
    let productQuery = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
     };
    return esUtils.checkIfExistInSearch(productIndex, ccType, productQuery) //search for product
      .then( product => {
        if (!product) // no such product 
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductMetadata failed - Product not found"} )


        let timestamp                   =  timeUtlis.convert(body.starttimestamp);
        let timestampend                =  timeUtlis.convert(body.endtimestamp);

        if (timestampend === "" && body.duration)
           timestampend = timestampend =  timeUtlis.convert( Date.parse(timestamp) + body.duration );

        body.starttimestamp        = timestamp;
        if (typeof timestampend === 'string' && timestampend.length > 0)
          body.endtimestamp          = timestampend;


        for (let i = 0; i < body.participants; i++){
          if (!body.participants[i].starttime)
            continue

          for (let j = 0; i < body.participants[i].starttime.length; j++)
            body.participants[i].starttime[j] = timeUtlis.convert( body.participants[i].starttime[j] );
    
        };


        let MDERecordObj                = {};
        MDERecordObj.fields             = body;
        MDERecordObj.comid              = body.comid;
        MDERecordObj.provider           = { "@id": user.member };
        MDERecordObj.product            = { "@id": id };

        MDERecordObj['@timestamp']      = timestamp;
        if (typeof timestampend === 'string' && timestampend.length > 0)
          MDERecordObj['@timestampend']   = timestampend;

        console.log('timestamp',        timestamp);
        console.log('timestampend',     timestampend);


        return esUtils.add(MDERecordIndex, ccType, MDERecordObj)  //create new MDERecord
          .then( resp => {
            //console.log('updateProductMetadata resp=', JSON.stringify(resp, null, 4));
            if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
              return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductMetadata failed - New MDERecord not created"} );


            /*const updateProdBody =  {
              script: {
                source: "ctx._source.fields = params.fields",
                params: {fields: body},
                lang: "painless"
              }
            };*/

            MDERecordObj.id                     = resp._id; //add new group id to final JSON response
            MDERecordObj.provider["@type"]      = esUtils.memberSchemeType;
            MDERecordObj.provider["@context"]   = esUtils.contextSchema;
            MDERecordObj.product["@type"]       = esUtils.productSchemeType;
            MDERecordObj.product["@context"]    = esUtils.contextSchema;

            //console.log('updateProductMetadata MDERecordObj=', MDERecordObj);
            return esUtils.doResponse( resolve, 200, MDERecordObj ); //Success
          })
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductMetadata failed - " + error.message } );
      })
  });// Promise close
}




/**
 * add metadata
 * 
 *
 * body MIFID2Schema data to update in contract
 * returns MIFID2Schema
 **/
exports.addMetadata = function(user,body) {

  // ITS Only !!!! 
  console.log('addMetadata user=', user);
  console.log('addMetadata body=', body);
  return new Promise(function(resolve, reject) {

    let timestamp                   =  timeUtlis.convert(body.starttimestamp);
    let timestampend                =  timeUtlis.convert(body.endtimestamp);

    if (timestampend === "" && body.duration)
       timestampend = timestampend =  timeUtlis.convert( Date.parse(timestamp) + body.duration );

    body.starttimestamp        = timestamp;
    if (typeof timestampend === 'string' && timestampend.length > 0)
      body.endtimestamp          = timestampend;


    for (let i = 0; i < body.participants; i++){
      if (!body.participants[i].starttime)
        continue

      for (let j = 0; i < body.participants[i].starttime.length; j++)
        body.participants[i].starttime[j] = timeUtlis.convert( body.participants[i].starttime[j] );

    };

    let MDERecordObj                = {};
    MDERecordObj.fields             = body;
    MDERecordObj.comid              = body.comid;
    MDERecordObj.provider           = { "@id": user.member };
    //MDERecordObj.product            = { "@id": id };


    MDERecordObj['@timestamp']      = timestamp;
    if (typeof timestampend === 'string' && timestampend.length > 0)
      MDERecordObj['@timestampend']   = timestampend;

    console.log('timestamp',        timestamp);
    console.log('timestampend',     timestampend);


    return esUtils.add(ITSRecordIndex, ccType, MDERecordObj)  //create new MDERecord
      .then( resp => {
        //console.log('addMetadata resp=', JSON.stringify(resp, null, 4));
        if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: addMetadata failed - New MDERecord not created"} );

        MDERecordObj.id                     = resp._id; //add new group id to final JSON response
        MDERecordObj.provider["@type"]      = esUtils.memberSchemeType;
        MDERecordObj.provider["@context"]   = esUtils.contextSchema;
        //MDERecordObj.product["@type"]       = esUtils.productSchemeType;
        //MDERecordObj.product["@context"]    = esUtils.contextSchema;

        //console.log('addMetadata MDERecordObj=', MDERecordObj);
        return esUtils.doResponse( resolve, 200, MDERecordObj ); //Success
      })
     
  });// Promise close
}

/**
 * add miscellaneous metadata
 * 
 *
 * site String VR site identifier, use with wav
 * wav String VR record identifier, use with site
 * body MIFID2Schema data to update in contract
 * returns MIFID2Schema
 **/
exports.addMetadataMisc = function(user, site, wav, body) {
  //This method should be used to create transcription & quality records linked to a VR MDErecord via site, wav, starttimestamp & endtimestamp
  //It can also be used to created unlinked MDErecords e.g. Zoom
  console.log('addMetadataMisc user=', user);
  console.log('addMetadataMisc site=', site);
  console.log('addMetadataMisc wav=', wav);
  console.log('addMetadataMisc body=', body);
  let MDERecordObj = {};
  return new Promise(function(resolve, reject) {
    if (wav === undefined || wav === null || site === undefined || site === null) { //create MDERecord without connecting it to anything
        console.log('addMetadataMisc site and/or wav have no value(s)');
        let timestamp =  timeUtlis.convert(body.starttimestamp);
        let timestampend =  timeUtlis.convert(body.endtimestamp);

        if (timestampend === "" && body.duration)
           timestampend = timestampend =  timeUtlis.convert( Date.parse(timestamp) + body.duration );

        body.starttimestamp = timestamp;
        if (typeof timestampend === 'string' && timestampend.length > 0)
          body.endtimestamp = timestampend;

        for (let i = 0; i < body.participants; i++){
          if (!body.participants[i].starttime)
            continue
          for (let j = 0; i < body.participants[i].starttime.length; j++)
            body.participants[i].starttime[j] = timeUtlis.convert( body.participants[i].starttime[j] );

        };

        MDERecordObj.fields             = body;
        MDERecordObj.comid              = body.comid;
        MDERecordObj.provider           = { "@id": user.member };

        MDERecordObj['@timestamp']      = timestamp;
        if (typeof timestampend === 'string' && timestampend.length > 0)
          MDERecordObj['@timestampend']   = timestampend;

        console.log('timestamp',        timestamp);
        console.log('timestampend',     timestampend);

        console.log('addMetadataMisc before adding MDERecordObj=', MDERecordObj);
        return esUtils.add(MDERecordIndex, ccType, MDERecordObj)  //create new MDERecord
          .then( resp => {
            //console.log('addMetadataMisc resp=', JSON.stringify(resp, null, 4));
            if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
              return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: addMetadataMisc failed - New MDERecord not created"} );

            MDERecordObj.id                     = resp._id; //add new id to final JSON response
            MDERecordObj.provider["@type"]      = esUtils.memberSchemeType;
            MDERecordObj.provider["@context"]   = esUtils.contextSchema;

            //console.log('addMetadataMisc MDERecordObj=', MDERecordObj);
            return esUtils.doResponse( resolve, 200, MDERecordObj ); //Success
          })

    } else { //create MDERecord and connect it to a VR using site and wav to find it
        console.log('addMetadataMisc site and wav have values');
        let mdeQuery = {
          "constant_score" : { 
            "filter" : {
              "bool" : {
                "must" : [
                  { "term" : {"wavs.keyword" : wav}}, 
                  { "term" : {"fields.site.keyword" : site}} 
                ]
              }
            }
          }
        };
        esUtils.checkIfExistInSearch(MDERecordIndex, ccType, mdeQuery) //search for VR MDErecord
          .then( vrMde => {
            if (!vrMde) // no such VR MDErecord 
              return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: addMetadataMisc failed - VR MDErecord not found"} )

            body.starttimestamp = vrMde._source.fields.starttimestamp;
            body.endtimestamp = vrMde._source.fields.endtimestamp;

            MDERecordObj.fields = body;
            MDERecordObj.comid = body.comid;
            MDERecordObj.provider = {"@id": user.member};
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //MDERecordObj.product = vrMde._source.product; //MAY NO WANT TO DO THIS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            MDERecordObj['@timestamp'] = vrMde._source['@timestamp'];
            if (vrMde._source.hasOwnProperty('@timestampend'))
              MDERecordObj['@timestampend'] = vrMde._source['@timestampend'];

            console.log('addMetadataMisc before adding MDERecordObj=', MDERecordObj);
            return esUtils.add(MDERecordIndex, ccType, MDERecordObj)  //create new MDERecord
              .then( resp => {
                //console.log('addMetadataMisc resp=', JSON.stringify(resp, null, 4));
                if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
                  return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: addMetadataMisc failed - New MDERecord not created"} );

                MDERecordObj.id                     = resp._id; //add new id to final JSON response
                MDERecordObj.provider["@type"]      = esUtils.memberSchemeType;
                MDERecordObj.provider["@context"]   = esUtils.contextSchema;

                //console.log('addMetadataMisc MDERecordObj=', MDERecordObj);
                return esUtils.doResponse( resolve, 200, MDERecordObj ); //Success
              })
            
          } )
          .catch( error => {
            return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: addMetadataMisc failed - " + error.message } );
          })
    } //end else

  });// Promise close
}


/**
 * Update contract with property
 * 
 *
 * id String contract to be updated with property
 * body List data to update in contract
 * returns GenericResponse
 **/
exports.updateProductProperty = function(user,id,body) {
  console.log('updateProductProperty user=', user);
  console.log('updateProductProperty id=', id);
  console.log('updateProductProperty body=', body);
  return new Promise(function(resolve, reject) {
    let productQuery = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
     };
    return esUtils.checkIfExistInSearch(productIndex, ccType, productQuery) //search for product
      .then( product => {
        if (!product) // no such product 
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductProperty failed - Product not found"} )

        let productSrc                = product._source;
        let additionalProperty        = productSrc.additionalProperty; //additionalProperty is an array
        productSrc.additionalProperty = _.unionBy(body, additionalProperty, 'name'); //does union of arrays by using 'name' and takes vals from 1st array i.e. body

        return esUtils.addWithId( productIndex, ccType, id, productSrc )  //add additionalProperty to Product
          .then( resp  => {
            if (resp === null || resp === undefined || resp.result !== "updated")
              return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductProperty failed - Existing Product not updated with input data"} );

            return esUtils.doResponse( resolve, 200, {"success": true} ); //Success
          })
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductProperty failed - " + error.message } );
      })
  });// Promise close
}

/**
 * Update a new Product contract
 * 
 *
 * id String contract whose data to get
 * body Product 
 * returns ProductSchema
 **/
exports.updateProduct = function(user,id,body) {
  //console.log('updateProduct user=', user);
  console.log('updateProduct id=', id);
  console.log('updateProduct body=', body);
   //
  return new Promise(function(resolve, reject) {

    if ( _.isEmpty(body) ) 
      return esUtils.doResponse( reject, 422, { "success": false, "error": "ERROR: updateProduct failed - Bad input data"} );

 
    let productQuery = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
     };
    return esUtils.checkIfExistInSearch(productIndex, ccType, productQuery) //search for product
      .then( product => {
        //console.log('updateProduct product=', JSON.stringify(product, null, 4));
        if (!product) // no such product 
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProduct failed - Product not found"} )

        let productSrc = product._source;
        for (let prop in body){
          if (prop === "id")
            continue;
          else if (prop === "owner"){
            //productSrc[prop] = _.union(productSrc.owner, body.owner); // << TODO  keyword > nasted 
            for ( let i = 0; i < body.owner.length; i++){
                let ex = false;
                for ( let j = 0; j < productSrc.owner.length; j++){
                  if ( body.owner[i]['@id'] === productSrc.owner[j]['@id'] ){
                    productSrc.owner[j] = body.owner[i];
                    ex = true
                    break
                  }
                }
                if (!ex)
                  productSrc.owner.push(body.owner[i]);

            }
          } else
            productSrc[prop] = body[prop];
        };

        return esUtils.addWithId( productIndex, ccType, id, productSrc )  //add additionalProperty to Product
          .then( resp  => {
            //console.log('updateProduct resp=', JSON.stringify(resp, null, 4));
            if (resp === null || resp === undefined || resp.result !== "updated")
              return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProduct failed - Existing Product not updated with input data"} );

            return esUtils.doResponse( resolve, 200, {"success": true} ); //Success
          })
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProduct failed - " + error.message } );
      })
  }); //Promise close
  //
}

/**
 * Replace contract owner
 * 
 *
 * id String contract to be updated with property
 * body List data to update in contract
 * returns GenericResponse
 **/
exports.updateProductOwner = function(user, id, body) {
 console.log('updateProductOwner id=', id);
 console.log('updateProductOwner body=', body);
 return new Promise(function(resolve, reject) {
    return esUtils.checkIfExistInSearch(productIndex, ccType, { match: {_id: id} }) //search for product
      .then( product => {
        if (!product) // no such product 
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductOwner failed - Product not found"} )

        let productSrc                = product._source;
        productSrc.owner              = body;

        //return console.log('productSrc',productSrc);

        return esUtils.addWithId( productIndex, ccType, id, productSrc )  //add owner to Product
          .then( resp  => {
            if (resp === null || resp === undefined || resp.result !== "updated")
              return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductOwner failed - Existing Product not updated with input data"} );

            return esUtils.doResponse( resolve, 200, {"success": true} ); //Success
          })
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductOwner failed - " + error.message } );
      })
  });// Promise close
}

exports.getProductBulkMetadata = function(user,  provider, limit, offset, starttime, endtime ) {
  return new Promise(function(resolve, reject) {
   

    if (!limit)
      limit = 10000;
    if (!offset)
      offset = 0;

     let query = {
        "constant_score" : { 
           "filter" : {
              "bool" : {
                "must" : [
                  {
                    "range" : {
                      "@timestamp" : {
                        "gte": starttime,
                        "lte": endtime,
                              //"format": "yyyy-MM-dd||yyyy",
                              //"time_zone": time_zone
                      }
                    }
                  }
                ]
              }
            }
          }
        }

    if (provider !== undefined) {
      query['constant_score'].filter.bool.must.push( { "term" : {"provider.@id.keyword" : provider}} )
    }

      /*
      var query = {
          "from": 0,
          "size": 10000,
            "query": {
              "bool" : {
              "must": {
                "range" : {
                      "fields.starttimestamp" : {
                          "gte": starttime,
                          "lte": endtime,
                          //"format": "yyyy-MM-dd||yyyy",
                          //"time_zone": time_zone
                      }
                    }
                  }
                }
            }
        };*/

      return esUtils.checkIfExistInSearch(MDERecordIndex, ccType, query, true) //search for MDERecords
      .then ( MDERecords => {
       if (!MDERecords) // no MDERecords matched
         return esUtils.doResponse( resolve, 200, [] ); 
          //return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getProductBulkMetadata no MDERecords matched"} );

        //console.log("getProductBulkMetadata MDERecords=", MDERecords);
        var MDERecordsObj = [];
        var MDERecordObj = {};
        console.log("getProductBulkMetadata MDERecords.length=", MDERecords.length);
        for (let i = 0; i < MDERecords.length; i++){
          let MDERecord = MDERecords[i];
          //console.log("getProductBulkMetadata MDERecord=", MDERecord);
          let MDERecordObj = {
            "fields": MDERecord._source.fields,
            "id": MDERecord._id,
            "provider": MDERecord._source.provider
          };
          if (MDERecord._source.hasOwnProperty('product')) {
            MDERecordObj.product = MDERecord._source.product;
            MDERecordObj.product["@type"] = schema.productSchemeType;
            MDERecordObj.product["@context"] = schema.contextSchema;
          }
          //console.log("getProductBulkMetadata MDERecordObj=", MDERecordObj);

          MDERecordObj.provider["@type"] = schema.memberSchemeType;
          MDERecordObj.provider["@context"] = schema.contextSchema;

          MDERecordsObj.push(MDERecordObj);
        } //end for
        return esUtils.doResponse( resolve, 200, MDERecordsObj ); //Success
      })  //end search for MDERecords
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getProductBulkMetadata failed - " + error.message } );
      })
  });// Promise close
}

/**
 * Get all metadata fields from across all contracts
 * 
 *
 * provider String provider whose metadata to retrieve in contract (optional)
 * size BigDecimal chunk size (optional)
 * starttime String use with endtime (optional)
 * endtime String use with starttime (optional)
 * returns List
 **/
exports.getProductBulkMetadataFields = function(user,  provider, size, starttime, endtime, its ) {
  return new Promise(async(resolve, reject) => {
    //console.log('getProductBulkMetadataFields user=', user);
    //console.log('getProductBulkMetadataFields provider=', provider);
    console.log("getProductBulkMetadataFields size, starttime, endtime, its ",  size, starttime, endtime, its );
  /*  let query = {
        "bool" : {
          "must": {
            "range" : {
              "@timestamp" : {
                "gte": starttime,
                "lte": endtime
              }
            }
          }
        }
      }

    console.log("query",query);
*/
     let query = {
        "constant_score" : { 
           "filter" : {
              "bool" : {
                "must" : [
                  {
                    "range" : {
                      "@timestamp" : {
                        "gte": starttime,
                        "lte": endtime,
                              //"format": "yyyy-MM-dd||yyyy",
                              //"time_zone": time_zone
                      }
                    }
                  }
                ]
              }
            }
          }
        }

    if (provider !== undefined) {
      query['constant_score'].filter.bool.must.push( { "term" : {"provider.@id.keyword" : provider}} )
    };

    let esIndex = MDERecordIndex;
    if (its === true)
      esIndex = ITSRecordIndex;

    //console.log('getProductBulkMetadataFields before scrollSearch', new Date());
    let searchResponse =  await esUtils.scrollSearch(esIndex, ccType, query, size) //search for MDERecords
    console.log('getProductBulkMetadataFields searchResponse length', searchResponse.length, new Date());
    if (searchResponse instanceof Error)
      return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getProductBulkMetadataFields failed - " + searchResponse.message } );
    
    return esUtils.doResponse( resolve, 200, searchResponse ); 
  });// Promise close
}
/**
 * Bulk Update contract with metadata
 * 
 *
 * body List data to update in contract
 * returns List
 **/
exports.updateProductBulkMetadata = function(user, regulaterule, body) {
  return new Promise(function(resolve, reject) {

    console.log("\n ---------- updateProductBulkMetadata ----------",  body.length);
    console.log('updateProductBulkMetadata regulaterule=', regulaterule);
    // body is an array
    // for each:
    // 1) check direction, get list of identifiers(extensionno) find product id
    // 2) check if mde exist by comid
    // 3) update 

    // "comid" 
    // "direction" : "inbound" / "outbound"
    // "destinationaddress"
    // "sourceaddress"
    let mdeRecords          = [];
    let productIdentifiers  = [];
    let productMDE_map      = {};
    let productIds          = [];

    for (let i = 0; i < body.length; i++){
      //console.log('body[i].destinationaddress', body[i].destinationaddress);
      //console.log('body[i].sourceaddress', body[i].sourceaddress);
      let no;
       if (body[i].hasOwnProperty('product')){
        no = body[i].product;

        if ( productIds.indexOf( body[i].product ) === -1 )
           productIds.push( body[i].product )
       }else {
        if(body[i].direction === 'inbound')
          no = body[i].destinationaddress;
        else 
          no = body[i].sourceaddress;

        no += '';
      }

      if ( !productMDE_map[ no ] ){
        productMDE_map[ no ] = [];
        if (productIdentifiers.indexOf(no) === -1)
          productIdentifiers[ productIdentifiers.length  ] =  no;
      };

      let wavs = [];
      if (body[i].hasOwnProperty('additionalProperty')) { //copy wavs from inside additionalProperty to make finding them easier in addMetadataMisc
        if (Array.isArray(body[i].additionalProperty)) {
          let vals = _.filter(body[i].additionalProperty, function(obj) {
            if (obj.name.startsWith('CVSLCT'))
              return true;
          });
          wavs = _.map(vals, 'value');
        }
      }
      console.log('wavs: ', wavs);

      productMDE_map[ no ].push ( { 
        fields        :  body[i],
        comid         :  body[i].comid,
        provider      :  { "@id": user.member },
        wavs          :  wavs
      });
      //console.log('productMDE_map[ no ]',productMDE_map[ no ].length);
    };

    console.log('productIdentifiers: ',productIdentifiers);
    console.log('productMDE_map: ',productMDE_map);
   
     let productQuery= {
        "constant_score" : { 
           "filter" : {
              "bool" : {
                "should" : [
                  {  "terms" : {"identifier.keyword" : productIdentifiers} },
                  { "terms" : {"_id" : productIds} }
                ]
              }
            }
          }
        }
        console.log("productQuery", JSON.stringify(productQuery) );
    return esUtils.checkIfExistInSearchLimitResult(productIndex, ccType, productQuery, true, ["owner", "identifier", "_id"])
      .then( products => {
        //console.log('updateProductBulkMetadata: products=', JSON.stringify(products, null, 4));
        if (!products)
          products = [];
        let ind;
        let i;
        //console.log('updateProductBulkMetadata products=', JSON.stringify(products, null, 4));

        let j = 0; //flag to wait for the async calls in for loop code to finish executing

        for (i = 0; i < products.length; i++){
          //console.log('updateProductBulkMetadata i=', i);
          let line = products[i]._source.identifier + '';
          let ownerArray = products[i]._source.owner;
          let productId = products[i]._id;

          let mapKey;
          if (productMDE_map[ line ])
            mapKey = line;
          else if (productMDE_map[ productId ])
            mapKey = productId
          


          const processMDERecord = ( idx ) => {
            //console.log('processMDERecord: productMDE_map[ line ].length=', productMDE_map[ line ].length );
            if (idx === productMDE_map[ mapKey ].length)
              return; //finish

              //console.log('processMDERecord: productMDE_map[ line ][ idx ]=', productMDE_map[ line ][ idx ] );
              let mde                         = productMDE_map[ mapKey ][ idx ];

             
              mde.fields.starttimestamp             =  timeUtlis.convert( mde.fields.starttimestamp);
              mde.fields.endtimestamp               =  timeUtlis.convert( mde.fields.endtimestamp);


              mde['@timestamp'] = mde.fields.starttimestamp;
              mde['@timestampend'] = mde.fields.endtimestamp;

              if (mde.fields.hasOwnProperty('product'))
                delete mde.fields.product
              mde[ 'product' ]                = { "@id": productId  }; 
              mdeRecords.push ( mde );
              processMDERecord(++idx);
          };



          //console.log('line', line);
          let regulate = false;
          if (regulaterule === undefined)
            regulate = true; //wasn't set, so store all the metadata records
          else if (regulaterule === 'line') {
            const checkRegulateLine = ( indLine ) => {
              if (indLine === ownerArray.length)
               return; //finish

              //console.log('checkRegulateLine:', ownerArray[indLine]);
              if (ownerArray[indLine].regulated) {
                regulate = true; //at least 1 owner of the product is regulated, so store all this products metadata records
                return; //finish
              } else 
                checkRegulateLine(++indLine);

            }
            checkRegulateLine(0);
          }
          else if (regulaterule === 'user') {
            const checkRegulateUser = ( indUser ) => {
              //console.log('checkRegulateUser: indUser=', indUser);
              return new Promise(function(resolve, reject) {
                if (indUser === ownerArray.length)
                  return resolve({}); //finish

                //console.log('checkRegulateUser: ownerArray[indUser]=', ownerArray[indUser]);
                let memberId = ownerArray[indUser]['@id'];
                esUtils.determineRegulated(groupIndex, memberIndex, ccType, memberId) //check member's groups to determine "regulated" value
                  .then ( calculatedRegulated => {
                    //console.log('checkRegulateUser: calculatedRegulated', JSON.stringify(calculatedRegulated, null, 4));
                    if (calculatedRegulated) { //at least one of the member's groups is regulated
                      regulate = true; //at least 1 owner of the product is regulated, so store all this products metadata records
                      //console.log('checkRegulateUser: regulate = true');
                      processMDERecord(0);
                      return resolve({}); //finish
                    } else
                      checkRegulateUser(++indUser);

                  })
                  .catch( error => {
                    checkRegulateUser(++indUser);
                  }) //end check member's groups to determine "regulated" value
              });
            }
            checkRegulateUser(0)
              .then ( resp => {
                  //console.log('checkRegulateUser: inside response to promise productId=', productId );
                  //console.log('checkRegulateUser: inside response to promise j=', j );
                  if (++j === products.length) {
                    //console.log('checkRegulateUser: inside response to promise before checkAndAdd for user, mdeRecords.length=', mdeRecords.length);
                    checkAndAdd( 0 );
                  }
                })
                .catch( error => {
                  //console.log("checkRegulateUser: inside catch error=", error);
                  //console.log('checkRegulateUser: inside catch error j=', j );
                  if (++j === products.length) {
                    //console.log('checkRegulateUser: inside catch error before checkAndAdd for user, mdeRecords.length=', mdeRecords.length);
                    checkAndAdd( 0 );
                  }
                })
          } //end else if (regulaterule === 'user')

          //console.log('updateProductBulkMetadata: final regulate=', regulate);
          if (regulate && !(regulaterule === 'user')) {
            processMDERecord(0);

          } //end if (regulated)
        }; //end for

        const checkAndAdd = ( index ) => {
          //console.log('checkAndAdd: index=', index );
          if (index === mdeRecords.length)
             return esUtils.doResponse( resolve, 200, { "success": true } ); //Success

          console.log('checkAndAdd(comid):', mdeRecords[ index ].comid );
          let mdeQuery = {
            "constant_score" : { 
              "filter" : {
                "bool" : {
                  "must" : [
                    { "term" : {"comid.keyword" : mdeRecords[ index ].comid}}, 
                    { "term" : {"fields.site.keyword" : mdeRecords[ index ].fields.site}} 
                  ]
                }
              }
            }
          };
          esUtils.search(MDERecordIndex, ccType, mdeQuery, 1)
            .then( searchResp => {
              //console.log('checkAndAdd: searchResp=', JSON.stringify(searchResp, null, 4));
              console.log(`total for ${mdeRecords[ index ].comid}:`, searchResp.hits.total.value);
              if (searchResp && searchResp.hits && searchResp.hits.total && searchResp.hits.total.value === 0){
                console.log("going to add", mdeRecords[ index ]);
                esUtils.add(MDERecordIndex, ccType, mdeRecords[ index ])  /// check if array is allowed
                  .then( resp => {
                    console.log('createProduct resp=', JSON.stringify(resp, null, 4));
                    if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
                      console.log("failed to add", mdeRecords[ index ].comid);
                    else
                      console.log('added', mdeRecords[ index ].comid);
                    checkAndAdd(++index);
                  }).catch( error => {
                     console.log(error)
                    checkAndAdd(++index);

                  })
              }else
                checkAndAdd(++index);

            })
            .catch( error => {
              checkAndAdd(++index);
            })
        }; //end const checkAndAdd = ...
        //console.log('updateProductBulkMetadata: mdeRecords=', mdeRecords);
        if (!(regulaterule === 'user')) {
          console.log('updateProductBulkMetadata before checkAndAdd for undefined and line, mdeRecords.length=', mdeRecords.length);
          checkAndAdd( 0 );
        }

      })
      .catch( error => {
        console.log('error',error);
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductBulkMetadata failed - " + error.message } );
      })
  })
}

/**
 * Bulk Update contract with miscellaneous metadata
 * 
 *
 * body List data to update in contract
 * returns GenericResponse
 **/
exports.updateProductBulkMetadataMisc = function(user, body) {
  return new Promise(function(resolve, reject) {
    console.log("\n ---------- updateProductBulkMetadataMisc ----------",  body.length);

    let mdeRecords          = [];
    let productIdentifiers  = [];
    let productMDE_map      = {};
    let productIds          = [];

    for (let i = 0; i < body.length; i++){
      //console.log('body[i].destinationaddress', body[i].destinationaddress);
      //console.log('body[i].sourceaddress', body[i].sourceaddress);
      let no;
       if (body[i].hasOwnProperty('product')){
        no = body[i].product;

        if ( productIds.indexOf( body[i].product ) === -1 )
           productIds.push( body[i].product )
       }else {
        if(body[i].direction === 'inbound')
          no = body[i].destinationaddress;
        else 
          no = body[i].sourceaddress;

        no += '';
      }

      if ( !productMDE_map[ no ] ){
        productMDE_map[ no ] = [];
        if (productIdentifiers.indexOf(no) === -1)
          productIdentifiers[ productIdentifiers.length  ] =  no;
      };

      productMDE_map[ no ].push ( { 
        fields        :  body[i],
        comid         :  body[i].comid,
        provider      :  { "@id": user.member }
      });
      //console.log('productMDE_map[ no ]',productMDE_map[ no ].length);
    };

    console.log('productIdentifiers: ',productIdentifiers);
    console.log('productIds: ',productIds);
    //console.log('productMDE_map: ', JSON.stringify(productMDE_map, null, 4));
   
    let productQuery= {
      "constant_score" : { 
         "filter" : {
            "bool" : {
              "should" : [
                {  "terms" : {"identifier.keyword" : productIdentifiers} },
                { "terms" : {"_id" : productIds} }
              ]
            }
          }
        }
      }
    //console.log("productQuery", JSON.stringify(productQuery) );
    return esUtils.checkIfExistInSearchLimitResult(productIndex, ccType, productQuery, true, ["owner", "identifier", "_id"])
      .then( products => {
        if (!products)
          products = [];
        let ind;
        let i;
        console.log('updateProductBulkMetadataMisc products matched=', products.length);

        for (i = 0; i < products.length; i++){
          //console.log('updateProductBulkMetadataMisc i=', i);
          let line = products[i]._source.identifier + '';
          let productId = products[i]._id;

          let mapKey;
          if (productMDE_map[ line ])
            mapKey = line;
          else if (productMDE_map[ productId ])
            mapKey = productId;
          console.log('updateProductBulkMetadataMisc mapKey=', mapKey);

          const processMDERecord = ( idx ) => {
            //console.log('processMDERecord: productMDE_map[ mapKey ].length=', productMDE_map[ mapKey ].length );
            if (idx === productMDE_map[ mapKey ].length)
              return; //finish

              //console.log('processMDERecord: productMDE_map[ mapKey ][ idx ]=', productMDE_map[ mapKey ][ idx ] );
              let mde                               = productMDE_map[ mapKey ][ idx ];
              mde.fields.starttimestamp             =  timeUtlis.convert( mde.fields.starttimestamp);
              mde.fields.endtimestamp               =  timeUtlis.convert( mde.fields.endtimestamp);

              mde['@timestamp'] = mde.fields.starttimestamp;
              mde['@timestampend'] = mde.fields.endtimestamp;

              if (mde.fields.hasOwnProperty('product'))
                delete mde.fields.product
              mde[ 'product' ]                = { "@id": productId  }; 
              mdeRecords.push ( mde );
              processMDERecord(++idx);
          };

          processMDERecord(0);
          //console.log('updateProductBulkMetadataMisc: in loop mdeRecords=', mdeRecords);
          //console.log('updateProductBulkMetadataMisc in loop, mdeRecords.length=', mdeRecords.length);
        }; //end for

        const checkAndAdd = ( index ) => {
          //console.log('checkAndAdd: index=', index );
          if (index === mdeRecords.length)
             return esUtils.doResponse( resolve, 200, { "success": true } ); //Success

          console.log('checkAndAdd(comid):', mdeRecords[ index ].comid );
          let mdeQuery = {
            "constant_score" : { 
              "filter" : {
                "bool" : {
                  "must" : [
                    { "term" : {"comid.keyword" : mdeRecords[ index ].comid}}, 
                    { "term" : {"fields.site.keyword" : mdeRecords[ index ].fields.site}} 
                  ]
                }
              }
            }
          };
          esUtils.search(MDERecordIndex, ccType, mdeQuery, 1)
            .then( searchResp => {
              //console.log('checkAndAdd: searchResp=', JSON.stringify(searchResp, null, 4));
              console.log(`number that already exists for ${mdeRecords[ index ].comid}:`, searchResp.hits.total.value);
              if (searchResp && searchResp.hits && searchResp.hits.total && searchResp.hits.total.value === 0){
                console.log("going to add", mdeRecords[ index ])
                esUtils.add(MDERecordIndex, ccType, mdeRecords[ index ])  /// check if array is allowed
                  .then( resp => { 
                    if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
                      console.log("failed to add", mdeRecords[ index ].comid);
                    else
                      console.log('added', mdeRecords[ index ].comid);
                    checkAndAdd(++index);
                  }).catch( error => {
                     console.log(error)
                    checkAndAdd(++index);

                  })
              }else
                checkAndAdd(++index);

            })
            .catch( error => {
              checkAndAdd(++index);
            })
        }; //end checkAndAdd

        //console.log('updateProductBulkMetadataMisc: final mdeRecords=', mdeRecords);
        console.log('updateProductBulkMetadataMisc before checkAndAdd, mdeRecords.length=', mdeRecords.length);
        checkAndAdd(0);

      })
      .catch( error => {
        console.log('error',error);
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductBulkMetadataMisc failed - " + error.message } );
      })

  })
}