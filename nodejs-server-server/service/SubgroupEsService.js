'use strict';

const ccType          = require('../elastic/ElasticSearchConnection').ccType;
const esUtils         = require('../elastic/EsUtils');
const groupIndex      = require('../elastic/ElasticSearchConnection').groupIndex;

const ResponsePayload = require('../utils/writer').respondWithCode;
const schema          = require('../utils/schema');
const _               = require('lodash');
/**
 * Update group
 * 
 *
 * id String contract to be updated with group
 * body Group data to update
 * returns GroupSchema
 **/
exports.updateGroup = function(user,id,body) {
  //console.log('updateGroup user=', user);
  console.log('updateGroup id=', id);
  console.log('updateGroup body=', body);
  
  return new Promise(function(resolve, reject) {
    let query = {
            "constant_score" : { 
               "filter" : { 
                  "term" : {"_id" : id}
               }
            }
         };
    return esUtils.checkIfExistInSearch( groupIndex, ccType, query) //search for group
      .then( group => {
        if (!group) // no such group 
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateGroup failed - Group not found"} )

        let groupSrc = group._source;
        for (let prop in body){
          if (prop === "id")
          	continue;
          groupSrc[prop] = body[prop];
        }; 

        return esUtils.addWithId( groupIndex, ccType, id, groupSrc )  //update existing group's data
          .then( resp  => {
            if (resp === null || resp === undefined || resp.result !== "updated") //failed to update existing doc
              return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateGroup failed - Existing Group not updated with input data"} );

            return esUtils.doResponse( resolve, 200, {"success": true} ); //Success
          })
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateGroup failed - " + error.message } );
      })
  }); //Promise close
}

/**
 * Get group
 * 
 *
 * id String group id
 * returns GroupSchema
 **/
exports.getGroup = function(user, id) {
  console.log('getGroup id=', id);
  return new Promise(function(resolve, reject) {
    //
    let query = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
        }
      }
    };
    return esUtils.checkIfExistInSearch(groupIndex, ccType, query, false) //search for group
      .then ( group => {
        if (!group) // no groups matched
          return group( reject, 500, { "success": false, "error": "ERROR: getGroup  - no group matched"} );

        let groupObj = {
          "@context": schema.contextSchema,
          "@type": schema.groupSchemeType,
          "name": group._source.name,
          "identifier": group._source.identifier,
          "regulated": group._source.regulated,
          "leiCode":group._source.leiCode,
          "id": group._id
        };

        return esUtils.doResponse( resolve, 200, groupObj ); //Success
      })
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getGroup failed - " + error.message } );
      })
  }); //Promise close
}