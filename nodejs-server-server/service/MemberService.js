'use strict';

const web3              = require('../blockchain/Web3Connection').web3;
const unlockAccount     = require('../blockchain/Personal').unlockAccount;

const communityBytes    = require('../blockchain/CommunityDetails').communityBytes;
const communityAbi      = require('../blockchain/CommunityDetails').communityAbi;
const communityRegistry = require('../blockchain/CommunityDetails').communityRegistry

const registryBytes     = require('../blockchain/RegistryDetails').registryBytes;
const registryAbi       = require('../blockchain/RegistryDetails').registryAbi;

const subgroupBytes     = require('../blockchain/SubgroupDetails').subgroupBytes;
const subgroupAbi       = require('../blockchain/SubgroupDetails').subgroupAbi;

const memberBytes       = require('../blockchain/MemberDetails').memberBytes;
const memberAbi         = require('../blockchain/MemberDetails').memberAbi;
const Utils             = require('../blockchain/Utils');

let tmp = 0; 

/**
 * Add a new member contract
 * 
 *
 * body Member Member contract that needs to be added to the BlockChain
 * returns ApiCreateResponse
 **/
exports.createMember = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
      "address" : "address",
      "block" : "block",
      "message" : "message",
      "transaction" : "transaction"
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Deletes a member contract
 * 
 *
 * contract String contract to delete
 * returns Boolean
 **/
exports.deleteMember = function(contract) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = true;
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all data from contract
 * 
 *
 * contract String contract whose data to retrieve
 * returns inline_response_200_8
 **/
exports.getMemberData = function(contract) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
      "internalId" : "internalId",
      "regulated" : true,
      "countryCode" : "countryCode",
      "memberName" : "memberName",
      "groups" : [ "groups", "groups" ],
      "exists" : true,
      "location" : "location",
      "account" : "account",
      "email" : "email",
      "retentionHold" : true
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get owner from contract
 * 
 *
 * contract String contract whose owner to retrieve
 * returns String
 **/
exports.getMemberOwner = function(contract) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Is member regulated
 * 
 *
 * contract String contract whose regulated status to retrieve
 * returns Boolean
 **/
exports.getMemberRegulated = function(contract) {
  console.log('contract=' + contract);
  return new Promise(function(resolve, reject) {
    const contractInstance  = web3.eth.contract(memberAbi).at(contract);
    const response = {};
    const grpRecords = [];
    const isRegulated = contractInstance.isRegulated();

    console.log('isRegulated', isRegulated);
    response['application/json'] = isRegulated;
    
    if (Object.keys(response).length > 0) {
      resolve(response[Object.keys(response)[0]]);
    } else {
      resolve(response);
    }
  });
}


/**
 * Update contract with member details
 * 
 *
 * contract String contract to be updated with member details
 * body Body_8 data to update in contract
 * returns ApiResponse
 **/
exports.updateMemberDetails = function(contract,body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "block" : "block",
  "message" : "message",
  "transaction" : "transaction"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Add group to contract's list of groups
 * 
 *
 * contract String contract to be updated with group
 * body String data to update in contract
 * returns ApiResponse
 **/
exports.updateMemberGroup = function(contract,body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "block" : "block",
  "message" : "message",
  "transaction" : "transaction"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Update contract with owner
 * 
 *
 * contract String contract to be updated with owner
 * body String data to update in contract
 * returns Boolean
 **/
exports.updateMemberOwner = function(contract,body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = true;
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Remove group in contract's list of groups
 * 
 *
 * contract String contract to have group removed in contract's list of groups
 * body String data to update in contract
 * returns ApiResponse
 **/
exports.updateMemberUngroup = function(contract,body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "block" : "block",
  "message" : "message",
  "transaction" : "transaction"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

