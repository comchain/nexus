'use strict';


const web3                = require('../blockchain/Web3Connection').web3;
const unlockAccount       = require('../blockchain/Personal').unlockAccount;

const ccBytes             = require('../blockchain/ComChainDetails').ccBytes;
const ccAbi               = require('../blockchain/ComChainDetails').ccAbi;
const ccRegistry          = require('../blockchain/ComChainDetails').ccRegistry

const registryBytes       = require('../blockchain/RegistryDetails').registryBytes;
const registryAbi         = require('../blockchain/RegistryDetails').registryAbi;

const utils               = require('../blockchain/Utils');

const startBlock          = require('config').get('Storage.startBlock');

//const ResponsePayload   = require('../utils/writer').respondWithCode;
const doResponse          = require('../utils/response').doResponse;
const schema              = require('../utils/schema')
const _                   = require('lodash');

const ccContract          = web3.eth.contract(ccAbi);
const ccSample            = ccContract.at('0x0000000000000000000000000000000000000000');
 


exports.createMiFID2  = exports.createComChain = (user,body) => new Promise( (resolve, reject) => {
    // 
    const create = () => {
      //

      const _identifier         = body.identifier;
      const _registry           = ccRegistry;

      let beenHere              = false;
      let from                  = user.account;
 

      const _community          = ccContract.new(
         _identifier,
         _registry,
         {
           from   : from, 
           data   : ccBytes, 
           gas    : '4700000'
         },   (e, contract) => {

          if (beenHere === true)
            return;

          if (e){
               return doResponse( reject, 500, { "success": false, "error": "ERROR: createComChain failed - " + e.message} )
          };

          let receipt = contract._eth.getTransactionReceipt(contract.transactionHash);
          if (receipt.contractAddress){
            //
            beenHere                = true;
            contract                = ccContract.at(receipt.contractAddress);
            const template          = contract.getTemplate();
            const provisionMap      = {};

            let k                   = 0;
            let providersCounter    = 0;
            let transactionsLength  = 1;
            let transactionCounter  = 0;

            for (let i = 0; i < template.length; i++){
              let field = utils.getClearString(template[i], true);
              if (!body[field])
                continue

              for (let j = 0; j < body[field].length; j++){
                let provider = body[field][j];
                if (!provisionMap[provider]){
                   provisionMap[provider] = [];
                   transactionsLength++;
                }
                provisionMap[provider].push(template[i]); 
                k++;
              }
            };

            const done = () => {
                if (++transactionCounter === transactionsLength){
                    
                    const responseObject = {
                      '@context'  : schema.contextSchema,
                      '@type'     : schema.comChainSchemeType,
                      'id'        : contract.address,
                      'identifier': contract.getIdentifier()
                    };

                    for (let i = 0; i < template.length; i++)
                      responseObject[  utils.getClearString(template[i],true) ] = contract.getProvider( template[i] );
                  

                    return doResponse( resolve, 200, responseObject ); 
                }
            };
            console.log('provisionMap', provisionMap);
          
            if (k > 0){
              for (let provider in provisionMap){
                contract.provision( provider, provisionMap[provider], {from: user.account, data: ccBytes, gas: '4700000' }, (e, pTransactionHash) => {
                  done();
                });
              };
            };
            done();
          };
       })
    };
    const unlock = () => {
      return unlockAccount(user.account, user.passphrase)
        .then( ( unlockResponse ) => {
          if (unlockResponse !== true)
            return doResponse( reject, 500, { "success": false, "error": "ERROR: createComChain failed - " + unlockResponse} )

          return create();
        })
        .catch( ( unlockError ) => {
          return doResponse( reject, 500, { "success": false, "error": "ERROR: createComChain failed - " + error.message} )
        });
    };

    if (user.account && user.passphrase && user.passphrase.length > 0)
      return unlock();
    else 
      return create();
 
});
/*

*/
/**
 * Deletes a MiFID2
 * 
 *
 * id String MiFID2 to delete
 * returns GenericResponse
 **/
exports.deleteMiFID2 = (user,id) => new Promise( (resolve, reject) => {
   
});


/**
 * Returns MiFID2 template
 * 
 *
 * id String contract to be updated with who is responsible for providing data for fields
 * returns MIFID2ProvisionSchema
 **/
exports.getMiFID2Obj = exports.getComChainObj = (user,id) => new Promise( (resolve, reject) => {
      try {
      const ccInstance  =  ccContract.at(id);
      const identifier  =  ccInstance.getIdentifier();
      //if ( identifier === '')
      //   return doResponse( reject, 404, { "success": false, "error": "ERROR: getComChainObj failed - comChain not found"} );


      const template          = ccInstance.getTemplate();
            //console.log('ccInstance', ccInstance);

      const cc = {
        '@context'  : schema.contextSchema,
        '@type'     : schema.comChainSchemeType,
        'id'        : ccInstance.address,
        'identifier': identifier
      };
      const toBlock       = 'latest';// 5956; //latest
      const topic         = web3.sha3('NewProvider(address,bytes32[])');
      const getFilter     = web3.eth.filter({fromBlock: startBlock, address: ccInstance.address, toBlock: toBlock, topics: [topic] });
  
      console.log('ccInstance.blockNumber', ccInstance.blockNumber);
      getFilter.get( (error, results) => {
        for (let i = 0; i < results.length; i++){
         const event    = ccSample.NewProvider().formatter(results[i]);
         console.log(i, event)
         //console.log('provider', event.args.provider);
         //console.log('fields', event.args.fields);
         const provider = event.args.provider;
         const fields   = event.args.fields;

         for (let j = 0; j < fields.length; j++){
            const field = utils.getClearString(fields[j], true);
            if (!cc[field])
              cc[field] = [];

            cc[field].push( provider );
         }
        }

        return doResponse( resolve, 200, cc );

      })
      /*for (let j = 0; j < template.length; j++){
        const field = utils.getClearString(template[j], true);
        cc[field]   = ccInstance.getProvider( template[j] );

        console.log('field', field);
      };
      return doResponse( resolve, 200, cc );*/

      }catch(error){
        console.log(error);
      }

});


/**
 * Returns all MiFID2 templates
 * 
 *
 * returns List
 **/
exports.getMiFID2ObjAll  = exports.getComChainObjAll  = (user) => new Promise( (resolve, reject) => {
    const toBlock       = 'latest';// 5956; //latest
    const topic         = web3.sha3('NewComChain(string)');
    const getFilter     = web3.eth.filter({fromBlock: startBlock, toBlock: toBlock, topics: [topic] });
  
    getFilter.get( (error, results) => {
        if (error)
             return doResponse( reject, 500, { "success": false, "error": "ERROR: getComChainObjAll failed - " + error.message} );
      
        let output = [];
        if (results){
        
          for (let i = 0; i < results.length; i++){
            const event             = ccSample.NewComChain().formatter(results[i]);
            if (event.address === '0x62dbcf9ffefc62af94ee7ea34505126997ce9570')
              continue
            
            const ccInstance        = ccContract.at(event.address);
            const template          = ccInstance.getTemplate();
            //console.log('ccInstance', ccInstance);

            const cc = {
              '@context'  : schema.contextSchema,
              '@type'     : schema.comChainSchemeType,
              'id'        : ccInstance.address,
              'identifier': event.args.identifier
            };

            /*for (let j = 0; j < template.length; j++){
              const field = utils.getClearString(template[j], true);
              console.log('field', field);
              cc[field] = ccInstance.getProvider( template[j] );
            };*/

            output.push( cc )
          };
        };
        return doResponse( resolve, 200, output );
     })
});


/**
 * Update MiFID2 with who is responsible for providing data for fields
 * 
 *
 * id String contract to be updated with who is responsible for providing data for fields
 * body MIFID2Provision data to update in contract
 * returns MIFID2ProvisionSchema
 **/
exports.updateMiFID2Provision = (user,id,body) => new Promise( (resolve, reject) => {
    //update
    
});

