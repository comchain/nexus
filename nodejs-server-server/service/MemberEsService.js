'use strict';

const ccType          = require('../elastic/ElasticSearchConnection').ccType;
const esUtils         = require('../elastic/EsUtils');
const memberIndex     = require('../elastic/ElasticSearchConnection').memberIndex;
const groupIndex      = require('../elastic/ElasticSearchConnection').groupIndex;
const ResponsePayload = require('../utils/writer').respondWithCode;
const doResponse      = require('../utils/response').doResponse;
const _               = require('lodash');
const schema          = require('../utils/schema');




/**
 * Get member
 * 
 *
 * id String member id
 * returns List
 **/
exports.getMember = function(user, id) {
  return new Promise(function(resolve, reject) {
    //
    let query = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
        }
      }
    };
    return esUtils.checkIfExistInSearch(memberIndex, ccType, query, false) 
      .then ( member => {
        if (!member) // no groups matched
          return doResponse( reject, 500, { "success": false, "error": "ERROR: getMember  - no member matched"} );


        const processMember = function(finalRegulated){

          let memberObj = {
            "@context": schema.contextSchema,
            "@type": schema.memberSchemeType,
            "id": member._id,
            "address": {
              "@context": "http://schema.org",
              "@type": "Place"
            },
            "identifier": member._source.identifier,
            "regulated": finalRegulated,
            //"familyName": member._source.familyName,
            //"givenName": member._source.givenName,
            "name": member._source.name,
            "memberOf": member._source.memberOf,
            "groups": member._source.groups,
            "retentionHold": member._source.retentionHold,
            "additionalProperty": member._source.additionalProperty
          };
          if (member._source.address){
            memberObj['address']['addressLocality'] = member._source.address.addressLocality,
            memberObj['address']['addressRegion'] =  member._source.address.addressRegion,
            memberObj['address']['postalCode'] = member._source.address.postalCode,
            memberObj['address']['streetAddress'] = member._source.address.streetAddress
          };
          return doResponse( resolve, 200, memberObj ); //Success
        }
         esUtils.determineRegulated(groupIndex, memberIndex, ccType, member._id) //check member's groups to determine "regulated" value
            .then ( calculatedRegulated => {
              //console.log('getMember: calculatedRegulated', JSON.stringify(calculatedRegulated, null, 4));
              let regulated;
              try {
                regulated = calculatedRegulated[0]._source.regulated;
              }catch(error){
                regulated = false;
              }
              processMember(regulated);
            })
            .catch ( error => {
               return doResponse( reject, 500, { "success": false, "error": "ERROR: getMember failed - " + error.message } );
            })
      })  //end search for groups
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: getMember failed - " + error.message } );
      })
    //
  })
};
/**
 * Add a new member contract
 * 
 *
 * body Member Member contract that needs to be added to the BlockChain
 * returns MemberSchema
 **/
exports.createMember = function(user,body) {
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!! --------------------------------------------------- !!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!! CURRENTLY NOT EXPOSED AS A REST METHOD, SO NOT USED !!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!! --------------------------------------------------- !!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //console.log('createMember user=', user);
  console.log('createMember body=', body);
  return new Promise(function(resolve, reject) {

    if ( _.isEmpty(body) )
      return doResponse( reject, 422, { "success": false, "error": "ERROR: createMember failed - Bad input data"} );

    console.log('createMember start');
    let memberIdentifier = body.identifier;
    let groups = [];  //array of group ES ids
    var id;
    let memberQuery = {
            "constant_score" : { 
               "filter" : { 
                  "term" : {"identifier.keyword" : memberIdentifier}
               }
            }
         };
    return esUtils.checkIfExistInSearch(memberIndex, ccType, memberQuery) //search for member
      .then( member => {
        console.log('createMember new member=', member);
        if (!member) {// no such member found, so create it

          let additionalProperty = [];
          for (let prop of body.additionalProperty){
            if (prop.hasOwnProperty("name") && prop.hasOwnProperty("value") )
              additionalProperty.push(prop);
          };
          body.additionalProperty = additionalProperty;

          member = esUtils.createNewMember(body, groups, id);
          console.log('createMember new member=', member);
          return esUtils.add(memberIndex, ccType, member)  //create new member
            .then( resp => {
              if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
                return doResponse( reject, 500, { "success": false, "error": "ERROR: createMember failed - New Member not created"} );

              member.id = resp._id;  //add newly created Member's id to final JSON response
              return doResponse( resolve, 200, member ); //Success
            }); //end create new member
        } else {// found existing member
          return doResponse( reject, 500, { "success": false, "error": "ERROR: createMember failed - Member with that Identifier already exists"} );
        } //end else
      } ) //end search for member
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: createMember failed - " + error.message } );
      })
  });// Promise close
}


/**
 * Is member regulated
 * 
 *
 * id String contract whose regulated status to retrieve
 * returns inline_response_200
 **/
exports.getMemberRegulated = function(user,id) {
  //console.log('getMemberRegulated user=', user);
  console.log('getMemberRegulated id=', id);
  return new Promise(function(resolve, reject) {
    let query = {
            "constant_score" : { 
               "filter" : { 
                  "term" : {"_id" : id}
               }
            }
         };
    return esUtils.checkIfExistInSearch(memberIndex, ccType, query) //search for member
      .then ( member => {
        if (!member) // no member matched
          return doResponse( reject, 500, { "success": false, "error": "ERROR: getMemberRegulated no member matched"} );

        let finalRegulated = false;
        return esUtils.determineRegulated(groupIndex, memberIndex, ccType, member._id) //check member's groups to determine "regulated" value
          .then ( calculatedRegulated => {
            if (calculatedRegulated) //at least one of the member's groups is regulated
              finalRegulated = true;

            //console.log('getMemberRegulated calculatedRegulated', calculatedRegulated);
            let ret = {"regulated": finalRegulated};
            console.log('getMemberRegulated ret', ret);
            return doResponse( resolve, 200, ret ); //Success

          })
          .catch( error => {
            return doResponse( reject, 500, { "success": false, "error": "ERROR: getMemberRegulated failed - " + error.message } );
          }) //end check member's groups to determine "regulated" value

      })  //end search for member
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: getMemberRegulated failed - " + error.message } );
      })
  });// Promise close
}



/**
 * Update member
 * 
 *
 * id String contract to be updated with member
 * body Member data to update in contract
 * returns MemberSchema
 **/
exports.updateCommunityMember = function(user,id,body) {
  //console.log('updateCommunityMember user=', user);
  console.log('?updateCommunityMember id=', id);
  console.log('?updateCommunityMember body=', body);


  //
  return new Promise(function(resolve, reject) {

    if ( _.isEmpty(body) )
       return doResponse( reject, 422, { "success": false, "error": "ERROR: updateCommunityMember failed - Bad input data"} );



    let query = {
            "constant_score" : { 
               "filter" : { 
                  "term" : {"_id" : id}
               }
            }
         };

    return esUtils.checkIfExistInSearch( memberIndex, ccType, query) //search for member
      .then( member => {
        if (!member) // no such member 
          return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityMember failed - Member not found"} )

        let memberSrc = member._source;
        for (let prop in body){
          if (prop === "id")
            continue;
          memberSrc[prop] = body[prop];
        }; 
        

        return esUtils.addWithId( memberIndex, ccType, id, memberSrc )  //update existing member's data
          .then( resp  => {
            if (resp === null || resp === undefined || resp.result !== "updated") //failed to update existing doc
              return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityMember failed - Existing Member not updated with input data"} );

            return doResponse( resolve, 200, {"success": true} ); //Success
          })
      } )
      .catch( error => {
        return doResponse( reject, 500, { "success": false, "error": "ERROR: updateCommunityMember failed - " + error.message } );
      })
  }); //Promise close
}


