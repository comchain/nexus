'use strict';

const web3                = require('../blockchain/Web3Connection').web3;
const unlockAccount       = require('../blockchain/Personal').unlockAccount;
const productBytes        = require('../blockchain/ProductDetails').productBytes;
const productAbi          = require('../blockchain/ProductDetails').productAbi;
const productRegistry     = require('../blockchain/ProductDetails').productRegistry

const registryBytes       = require('../blockchain/RegistryDetails').registryBytes;
const registryAbi         = require('../blockchain/RegistryDetails').registryAbi;

const metaDataStoreBytes  = require('../blockchain/MetaDataStoreDetails').metaDataStoreBytes;
const metaDataStoreAbi    = require('../blockchain/MetaDataStoreDetails').metaDataStoreAbi;

const utils               = require('../blockchain/Utils');

const startBlock          = require('config').get('Storage.startBlock');
const jsonPath            = require('config').get('Storage.jsonPath');


const jsonfile            = require('jsonfile');
const loadJsonFile        = require('load-json-file');


//const ResponsePayload   = require('../utils/writer').respondWithCode;
const doResponse          = require('../utils/response').doResponse;
const schema              = require('../utils/schema');
const _                   = require('lodash');

const productContract     = web3.eth.contract(productAbi);
const productSample       = productContract.at('0x0000000000000000000000000000000000000000');
 
const metaDataStoreContract     = web3.eth.contract(metaDataStoreAbi);
const metaDataStoreSample       = metaDataStoreContract.at('0x0000000000000000000000000000000000000000');
 
let mdsProductMap;

let productMigrationIN = {
  '0xf0deb0cb3a93a04b892ffe0410704167b5d53dc6': '0x76e2ed2e84e6c264d53786d7efa8294c02bb1f2b',
  '0x4a4a89c06857ae1d4cc51fc14ffd50845c3c1e11': '0xfed0c13065de8d7c46172e520a228bca06c3458f'
}
let productMigration = {
  '0x76e2ed2e84e6c264d53786d7efa8294c02bb1f2b': '0xf0deb0cb3a93a04b892ffe0410704167b5d53dc6',
  '0xfed0c13065de8d7c46172e520a228bca06c3458f': '0x4a4a89c06857ae1d4cc51fc14ffd50845c3c1e11'
}

const killed  = [];
/*
const killed = ['0xf79479e9bb5215a1aaaf22b72a8c55d9a9b6f79c',
  '0x8461ee6a04bbbf62a0b5a6c86b78cc8dc0f9cc01',
  '0x845a0a1fa524d262d0bdd6ebee1f43a8b31ca441',
  '0xefdcbad9d4a2a8348a164aed704592901fd049e2',
  '0x141a08796a0e3cc0d91bcdf6a8169e7ae5c950fb',
  '0x2b9489518945721f40dcbbf37011a2d3a0be479a',
  '0xcb9e8e36983b13c2e43f3389a1c2dc3481493bfc',
  '0xedfb07834737280593a13e7b22b15fde8f6b04fb',
  '0x88c747507eef4e0242e0341dfc225894683515b1',
  '0xb1373b3e5aa480826d59de8b1bc263f35e689b77',
  '0x974c641e3c06295ecc687d3a94c1a8c7d331c59b'];
*/ 


/**
 * Add a new Product contract
 * 
 *
 * body Product Product contract that needs to be added to the BlockChain
 * returns ProductSchema
 **/



exports.createProduct = (user, body) => {
  console.log('createProduct user=', user);
  console.log('createProduct body=', body);
  return new Promise( (resolve, reject) => {
    if (!body.hasOwnProperty('identifier')) //input identifier is mandatory
      return doResponse( reject, 500, { "success": false, "error": "ERROR: createProduct failed -  identifier not supplied"} )
    //
    const create = () => {
      //
      let _owner              = body.owner;
      let _identifier         = body.identifier;
      let _category           = body.category;
      let _extraData          = body.extradata  || body.extraData;
      let _comChain           = body.comchain   || body.comChain; // TO DO change mifid2 to comChain
      let _additionalProperty = body.additionalProperty;
      let _registry           = '0x0000000000000000000000000000000000000000';

      let beenHere            = false;

      if (!Array.isArray(_owner))
        _owner = [_owner];
      
      if (!_additionalProperty)
        _additionalProperty = [];

      if (!_comChain)
        _comChain = '0x0000000000000000000000000000000000000000';

      if (!_extraData)
        _extraData = '0x0000000000000000000000000000000000000000'
      
      //address _owner, string _identifier, bytes32 _category, address _registry 
      const _product          = productContract.new(
         _owner,
         _identifier,
         _category,
         _registry,
         {
           from   : user.account, 
           data   : productBytes, 
           gas    : '6700000'
         },   (e, contract) => {

          if (beenHere === true)
            return 

          if (e)
            return doResponse( reject, 500, { "success": false, "error": "ERROR: createProduct failed - " + e.message} );

          let receipt = contract._eth.getTransactionReceipt(contract.transactionHash);
          if (!receipt)
            return
          if (receipt.contractAddress){
            //
            beenHere               = true;
            let transactionCounter = 0;
            let transactionsLength = 1 + _additionalProperty.length;
            const done = () => {
              if (++transactionCounter === transactionsLength){
                //

                let i;

                const additionalProperty  = [];
                const propertiesKeys      = contract.getAdditionalPropertyKeys();

                for ( i = 0; i < propertiesKeys.length; i++ ){
                  const key   = utils.getClearString( propertiesKeys[i], true);
                  const value = contract.getAdditionalProperty( propertiesKeys[i] );
                  const ap    = {
                    "@context"    : "http://schema.org",
                    "@type"       : "PropertyValue",
                    "name"        : key,
                    "value"       : value
                  }
                  additionalProperty.push(ap);
                };

                const owners            = contract.getOwners();

                for ( i = 0; i < owners.length; i++ ){
                  owners[i] = {
                    "@context"    : schema.contextSchema,
                    "@type"       : schema.memberSchemeType,
                    "@id"         : owners[i]
                  }
                };

                const productResponse = {
                  "@context"            : schema.contextSchema,
                  "@type"               : schema.productSchemeType,
                  "id"                  : contract.address,
                  "identifier"          : contract.getIdentifier(),
                  "category"            : utils.getClearString(contract.getCategory(), true),
                  "additionalProperty"  : additionalProperty,
                  "owner"               : owners,
                  "comChain"            : {
                    "@context"      : schema.contextSchema,
                    "@type"         : schema.comChainSchemeType,
                    "@id"           : contract.getComChain()
                  },
                  "extraData"           : {
                    "@context"      : schema.contextSchema,
                    "@type"         : schema.extraDataSchemeType,
                    "@id"           : contract.getExtraData()
                  }
                };
                return doResponse( resolve, 200, productResponse ); //Success // << TO DO GET MEMBERS 
                //
              }
            };
            contract = productContract.at(receipt.contractAddress);
            if ( _extraData !== '0x0000000000000000000000000000000000000000' ){
              transactionsLength++
              contract.setExtraData( _extraData, {from: user.account, data: productBytes, gas: '4700000' }, (e, cctTransactionHash) => {
                done();
              })
            };
            if ( _comChain !== '0x0000000000000000000000000000000000000000' ){
              contract.setComChain( _comChain, {from: user.account, data: productBytes, gas: '4700000' }, (e, edtTransactionHash) => {
                done();
              })
            };

            for (let i = 0; i < _additionalProperty.length; i++){
              contract.addAdditionalProperty( _additionalProperty[i]['name'], _additionalProperty[i]['value'], {from: user.account, data: productBytes, gas: '4700000' }, (e, aptTransactionHash) => {
                done();
              })
            };

            done();
          };
       })
    };
    const unlock = () => {
      return unlockAccount(user.account, user.passphrase)
        .then( ( unlockResponse ) => {
          if (unlockResponse !== true)
            return doResponse( reject, 500, { "success": false, "error": "ERROR: createProduct failed - " + unlockResponse} )

          return create();
        })
        .catch( ( unlockError ) => {
          return doResponse( reject, 500, { "success": false, "error": "ERROR: createProduct failed - " + error.message} )
        });
    };

    if (user.account && user.passphrase && user.passphrase.length > 0)
      return unlock();
    else 
      return create();
    //
 
  }); // Promise close
}


/**
 * Deletes a Product contract
 * 
 *
 * id String contract to delete
 * returns GenericResponse
 **/
exports.deleteProduct = (user, id) => {
  console.log('deleteProduct user=', user);
  console.log('deleteProduct id=', id);
  return new Promise( (resolve, reject) => {
    
  });// Promise close
}
//????????????????????????????????????????????????????? Delete Product - should it delete ExtraData ??????????????????????????????????????????????????????????????


/**
 * Get all metadata from contract
 * 
 *
 * id String contract whose metadata to retrieve
 * provider String provider whose metadata to retrieve in contract (optional)
 * returns List
 **/


exports.getProductAllMetadata = (user, id, provider, limit, offset, starttime, endtime ) => {
  console.log('getProductAllMetadata user=', user);
  console.log('getProductAllMetadata id=', id); //product id
  console.log('getProductAllMetadata provider=', provider);
  return new Promise( (resolve, reject) => {
   
      let productAddress = id;
      if (productMigrationIN[productAddress]){
        productAddress = id = productMigrationIN[productAddress];
      };

    //offset=5&limit=5
    if (!web3.isAddress(id) || killed.indexOf(id) > -1)
        return doResponse( reject, 404, { "success": false, "error": "ERROR: getProductAllMetadata failed - Invalid product address"} );
    
    try {
      if (!provider) 
        provider = '0xa7ff963c0e7fd492ec6b3dd4e9e11dbd51c7bd0f'//user.account;
    
      const contract                = productContract.at(id);
      // to do check if not killed!!!!!
      const providers               = contract.getMetaDataProviders();
      console.log('providers', providers)
      const mdsContract             = metaDataStoreContract.at(  contract.getMetaDataStore( providers[0] ) );
      const metaLength              = Number( mdsContract.getMetaLength() );
      console.log('metaLength',metaLength);

      let output        = [];
      let itemsInScope  = [];

      function isValidDate(d) {
        return d instanceof Date && !isNaN(d);
      }

      if (typeof starttime  === 'string' ){
        try {
          starttime = new Date(starttime);
          if (!isValidDate(starttime))
            return doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata failed - incorrect starttime format use YYYY-MM-DDTHH:mm:ss.sssZ"} )
          else 
            starttime = starttime.getTime();
        }catch(error){
           return doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata failed - incorrect starttime format use YYYY-MM-DDTHH:mm:ss.sssZ"} )
        }

        try {
          if ( typeof endtime === 'string') {
           endtime = new Date(endtime);
           if (!isValidDate(endtime))
              return doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata failed - incorrect endtime format use YYYY-MM-DDTHH:mm:ss.sssZ"} )
           else 
              endtime = endtime.getTime();
          }else 
            endtime =  Date.now() + 3600000;



        }catch(error){
           return doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata failed - incorrect endtime format use YYYY-MM-DDTHH:mm:ss.sssZ"} )
        }

        console.log('starttime', starttime);
        console.log('endtime', endtime);


          for (let i = 0; i < metaLength; i++){
              let r = mdsContract.getMetaData(i);
              let hash  = r[0];
              let url   = r[1];
              let date  = url.split('_').pop();
              date      = date.slice(0, date.lastIndexOf('.'))
              date      = new Date( date ).getTime();

              if (date >= starttime && date <= endtime){
                itemsInScope.push( { url: url, hash: hash, provider: provider } );
               // console.log(i, new Date(date));
              };

              
          };

      }else {

        if (typeof limit === "undefined")
            limit = 20;
        else 
            limit = parseInt(limit);

        if (typeof offset === "undefined")
          offset = 0;
        else 
          offset = parseInt(offset);
        
        limit = Math.min(limit, metaLength);

        let limitPlusOffset = limit + offset;

        if (limitPlusOffset > metaLength)
          limitPlusOffset = metaLength-1;

        console.log('offset', offset);
        console.log('limit', limit);

   

        for (let i = offset; i < limitPlusOffset; i++){
          let r = mdsContract.getMetaData(i);
          let hash  = r[0];
          let url   = r[1];
          //console.log(i, 'r',  url );
          itemsInScope.push( { url: url, hash: hash, provider: provider } )
        };

      };

      const getMetaLog = (i) => {
          if ( i === itemsInScope.length)
              return doResponse( resolve, 200, output ); 

          loadJsonFile(itemsInScope[i].url)
              .then(json => {
                  let item = {
                    product:  {
                      '@context': schema.contextSchema,
                      '@type': schema.productSchemeType,
                      '@id': id
                    },
                    provider: {
                      '@type': schema.memberSchemeType,
                      '@context': schema.contextSchema,
                      '@id': itemsInScope[i].provider
                    },
                    fields: json,
                    hash: itemsInScope[i].hash,
                    isValid: web3.sha3(JSON.stringify(json)) === itemsInScope[i].hash
                  }
                  //console.log(i, json);
                  output.push(item);
                  getMetaLog( ++i );
              }).catch( error => {
                  console.log(error);
                  getMetaLog( ++i );
              });
      };
      getMetaLog(0)

      //return doResponse( resolve, 200, output ); 



    // console.log('contract', contract);
   
    }catch(error){
      console.log(error)
    }
    //


  });// Promise close
}


exports.getProductBulkMetadata = (user,  provider, limit, offset, starttime, endtime ) => {

  let ooP = [];
 //
  return new Promise( (resolve, reject) => {
   
    //offset=5&limit=5

    //console.log('user,  provider, limit, offset, starttime, endtime', user,  provider, limit, offset, starttime, endtime);

    try {
      if (!provider) 
        provider = user.account;

      let output        = [];
    


      //
 
       let productCounter = 0;
       let productsLengeth = 0;
       let _results = [];

      //
      function isValidDate(d) {
        return d instanceof Date && !isNaN(d);
      };

      if (typeof starttime  === 'string' ){
          try {
            starttime = new Date(starttime);
            if (!isValidDate(starttime))
              return doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata failed - incorrect starttime format use YYYY-MM-DDTHH:mm:ss.sssZ"} )
            else 
              starttime = starttime.getTime();
          }catch(error){
             return doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata failed - incorrect starttime format use YYYY-MM-DDTHH:mm:ss.sssZ"} )
          }

          try {
            if ( typeof endtime === 'string') {
             endtime = new Date(endtime);
             if (!isValidDate(endtime))
                return doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata failed - incorrect endtime format use YYYY-MM-DDTHH:mm:ss.sssZ"} )
             else 
                endtime = endtime.getTime();
            }else 
              endtime =  Date.now() + 3600000;
          }catch(error){
             return doResponse( reject, 500, { "success": false, "error": "ERROR: getProductAllMetadata failed - incorrect endtime format use YYYY-MM-DDTHH:mm:ss.sssZ"} )
          }
      }

      // filter way
//      const toBlock       = 'latest';// 5956; //latest
      const topicMDE         = web3.sha3('NewReference(address,string,string)');
      const getFilterMDE     = web3.eth.filter({fromBlock: startBlock, toBlock: 'latest', topics: [topicMDE] });
      
      return getFilterMDE.get( (error, results) => {
        let itemsInScope = [];
        const getMetaLog = ( index ) => {
          if (index === itemsInScope.length)
            return doResponse( resolve, 200, output ); 
          //****
          loadJsonFile(itemsInScope[index].url)
                .then(json => {
                    try {
                      let localHash = web3.sha3(JSON.stringify(json));
                      let productAddress = itemsInScope[index].address;
                      
                      if (productMigration[productAddress]){
                        productAddress = productMigration[productAddress]
                      };


                      let item = {
                        product:  {
                          '@context': schema.contextSchema,
                          '@type': schema.productSchemeType,
                          '@id': productAddress
                        },
                        provider: {
                          '@type': schema.memberSchemeType,
                          '@context': schema.contextSchema,
                          '@id': itemsInScope[index].provider
                        },
                        fields: json,
                        transactionHash: itemsInScope[index].th,
                        hash: itemsInScope[index].hash,
                        localHash: localHash,
                        jsonPath: itemsInScope[index].url,
                        isValid: localHash === itemsInScope[index].hash
                      };
                     
                      output.push(item);
                    }catch(error){
                      console.log(error);
                    }
                    getMetaLog( ++index );


                }).catch( error => {
                    console.log(error);
                    getMetaLog( ++index  );
                });
          //****
        };
    

         //
        for (let i = 0; i < results.length; i++){
            const event  = metaDataStoreSample.NewReference().formatter(results[i]);
           // console.log('\n', event);
            let r      = event.args;
            let url    = r.path;
            let hash   = r.hash
            if (typeof starttime  === 'number' ){
                //console.log('starttime',  starttime);
               // console.log('endtime',    endtime);
                
                let date  = url.split('_').pop();
                date      = date.slice(0, date.lastIndexOf('.'))
                date      = new Date( date ).getTime();
                if (date >= starttime && date <= endtime){
                  itemsInScope.push( { url: url, hash: hash, provider: r.provider, address: mdsProductMap[results[i].address],  th: event.transactionHash } );
                 // console.log(i, new Date(date));
                };
            }else 
               itemsInScope.push( { url: url, hash: hash, provider: r.provider, address: mdsProductMap[results[i].address], th: event.transactionHash } );

        };
        getMetaLog(0);
       // console.log('itemsInScope',itemsInScope);

      });

    }catch(error){
      console.log(error)
    }
    //


  });// Promise close
 //
}

/**
 * Get single Product contract object
 * 
 *
 * id String contract whose data to get
 * returns ProductSchema
 **/
exports.getProductObj = (user, id) => {
  console.log('getProductObj user=', user);
  console.log('getProductObj id=', id);
  return new Promise( (resolve, reject) => {
     if (!web3.isAddress(id))
        return doResponse( reject, 404, { "success": false, "error": "ERROR: getProductObj failed - Invalid product address"} );

      const contract   =  productContract.at(id);
      // to do check if not killed

      if (contract.getIdentifier() === "" || killed.indexOf(id) > -1)
         return doResponse( reject, 404, { "success": false, "error": "ERROR: getProductObj failed - Product not found"} );

      //
      let i;
 
      const additionalProperty  = [];
      const propertiesKeys      = contract.getAdditionalPropertyKeys();

      for ( i = 0; i < propertiesKeys.length; i++ ){
          const key   = utils.getClearString( propertiesKeys[i], true);
          const value = contract.getAdditionalProperty( propertiesKeys[i] );
          const ap    = {
            "@context"    : "http://schema.org",
            "@type"       : "PropertyValue",
            "name"        : key,
            "value"       : value
          }
          additionalProperty.push(ap);
      };

      const owners            = contract.getOwners();

      for ( i = 0; i < owners.length; i++ ){
        owners[i] = {
          "@context"    : schema.contextSchema,
          "@type"       : schema.memberSchemeType,
          "@id"         : owners[i]
        }
      };

      let productAddress = contract.address;
      if (productMigration[productAddress]){
        productAddress = productMigration[productAddress]
      };

      const product = {
        "@context"            : schema.contextSchema,
        "@type"               : schema.productSchemeType,
        "oAddress"            : contract.address,
        "id"                  : productAddress, 
        "identifier"          : contract.getIdentifier(),
        "category"            : utils.getClearString(contract.getCategory(), true),
        "additionalProperty"  : additionalProperty,
        "owner"               : owners,
        "comChain"            : {
          "@context"      : schema.contextSchema,
          "@type"         : schema.comChainSchemeType,
          "@id"           : contract.getComChain()
        },
        "extraData"           : {
          "@context"      : schema.contextSchema,
          "@type"         : schema.extraDataSchemeType,
          "@id"           : contract.getExtraData()
        }
      };

      return doResponse( resolve, 200, product ); //Success // << TO DO GET MEMBERS 
      //
  });// Promise close
}


/**
 * Get all products contract objects
 * 
 *
 * returns List
 **/
exports.getProductObjAll = (user) => {
  console.log('getProductObjAll user=', user);
  return new Promise( (resolve, reject) => {
    //
      const toBlock       = 'latest';// 5956; //latest
      const topic         = web3.sha3('NewProduct(address,string,bytes32)');
      const getFilter     = web3.eth.filter({fromBlock: startBlock, toBlock: toBlock, topics: [topic] });
    
      getFilter.get( (error, results) => {
          if (error)
            return doResponse( reject, 500, { "success": false, "error": "ERROR: getProductObjAll failed - " + error.message} );
        
          let output = [];
          if (results){

            const pOne = ( index ) => {
              if (index === results.length)
                  return doResponse( resolve, 200, output ); 
              //
              if (killed.indexOf(results[index].address) > -1)
                return pOne(++index);

              let i;
              const contract            = productContract.at(results[index].address);
              // to do check if not killed
             // console.log('contract.blockNumber', contract);
              //const killtopic     = web3.sha3('Killed()');
              //const getFilter     = web3.eth.filter( { fromBlock: 0, toBlock: 'latest',  topics: [killtopic] } );//address: results[index].address,
              
                //
                
                const additionalProperty  = [];
                const propertiesKeys      = contract.getAdditionalPropertyKeys();

                for ( i = 0; i < propertiesKeys.length; i++ ){
                    const key   = utils.getClearString( propertiesKeys[i], true);
                    const value = contract.getAdditionalProperty( propertiesKeys[i] );
                    const ap    = {
                      "@context"    : "http://schema.org",
                      "@type"       : "PropertyValue",
                      "name"        : key,
                      "value"       : value
                    }
                    additionalProperty.push(ap);
                };

                const owners            = contract.getOwners();

                for ( i = 0; i < owners.length; i++ ){
                  owners[i] = {
                    "@context"    : schema.contextSchema,
                    "@type"       : schema.memberSchemeType,
                    "@id"         : owners[i]
                  }
                };

                let productAddress = contract.address;
                if (productMigration[productAddress]){
                  productAddress = productMigration[productAddress]
                };

                const product = {
                  "@context"            : schema.contextSchema,
                  "@type"               : schema.productSchemeType,
                  "oAddress"            : contract.address,
                  "id"                  : productAddress,//contract.address,
                  "identifier"          : contract.getIdentifier(),
                  "category"            : utils.getClearString(contract.getCategory(), true),
                  "additionalProperty"  : additionalProperty,
                  "owner"               : owners,
                  "comChain"            : {
                    "@context"      : schema.contextSchema,
                    "@type"         : schema.comChainSchemeType,
                    "@id"           : contract.getComChain()
                  },
                  "extraData"           : {
                    "@context"      : schema.contextSchema,
                    "@type"         : schema.extraDataSchemeType,
                    "@id"           : contract.getExtraData()
                  }
                };

                output.push(product);  

                pOne(++index);

              //
              //
            }
            pOne(0);
          
           // for (let j = 0; j < results.length; j++){
           //   pOne(results[j].address)

           // };
          }else 
            return doResponse( resolve, 200, output ); //Success // << TO DO GET MEMBERS 
      });
    //
  });// Promise close
}


/**
 * Update contract with metadata
 * 
 *
 * id String contract to be updated with metadata
 * body MIFID2Schema data to update in contract
 * returns MetaDataSchema
 **/
exports.updateProductMetadata = (user,id,body) => new Promise( (resolve, reject) => {
  console.log('updateProductMetadata user=', user);
  console.log('updateProductMetadata id=', id);
  console.log('updateProductMetadata body=', body); 
    //

  try {
  if (!web3.isAddress(id))
      return doResponse( reject, 404, { "success": false, "error": "ERROR: updateProductMetadata failed - Invalid product address"} );

  let productAddress = id;
  if (productMigrationIN[productAddress]){
     productAddress = id = productMigrationIN[productAddress]
  };

  const contract   =  productContract.at(id);
  if (contract.getIdentifier() === "" || killed.indexOf(id) > -1)
    return doResponse( reject, 404, { "success": false, "error": "ERROR: updateProductMetadata failed - Product not found"} );


  //
  const fileName  = contract.address+"_"+new Date().toISOString();
  const file      = `${jsonPath}${fileName}.json`;
   
  jsonfile.writeFile(file, body, function (err) {
 
      if (err)
        return doResponse( reject, 404, { "success": false, "error": "ERROR: updateProductMetadata failed - " + err.message} );

      const create = () => {
        //
        const hash    = web3.sha3(JSON.stringify(body));
        contract.pushMetaData( hash, file,
          { from   : user.account, gas: '4700000', data: productBytes },
          (err, transactionHash) => {
            if (err)
             return doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductMetadata failed - " + err.message} )


            

           let output = {
            "@context"            : schema.contextSchema,
            "@type"               : schema.MDERecordSchemeType,
            "id"                  : transactionHash,
            "product"             : {
              "@context"            : schema.contextSchema,
              "@type"               : schema.productSchemeType,
              "@id"                 : productAddress,//contract.address
             },
             "provider"           : {
                "@context"            : schema.contextSchema,
                "@type"               : schema.productSchemeType,
                "@id"                 : user.member
             },
             "fields"             : {
                "@context"            : schema.contextSchema,
                "@type"               : schema.comChainSchemeType,
             }
           }
           for (let ind in body)
              output["fields"][ind] = body[ind];
          
           return doResponse( resolve, 200, output ); 
          });
        //
      };
      const unlock = () => {
        return unlockAccount(user.account, user.passphrase)
          .then( ( unlockResponse ) => {
            if (unlockResponse !== true)
              return doResponse( reject, 500, { "success": false, "error": "ERROR: createProduct failed - " + unlockResponse} )

            return create();
          })
          .catch( ( unlockError ) => {
            return doResponse( reject, 500, { "success": false, "error": "ERROR: createProduct failed - " + error.message} )
          });
      };

      if (user.account && user.passphrase && user.passphrase.length > 0)
        return unlock();
      else 
        return create();
    })
    }catch(error){
      console.log(error);
    }
});// Promise close


/**
 * add metadata
 * 
 *
 * body MIFID2Schema data to update in contract
 * returns MIFID2Schema
 **/
exports.addMetadata = function(user,body) {

};

/**
 * Update contract with property
 * 
 *
 * id String contract to be updated with property
 * body List data to update in contract
 * returns GenericResponse
 **/
exports.updateProductProperty = (user,id,body) => {
  console.log('updateProductProperty user=', user);
  console.log('updateProductProperty id=', id);
  console.log('updateProductProperty body=', body);
  return new Promise( (resolve, reject) => {
    //
    try {
      if (!web3.isAddress(id))
        return doResponse( reject, 404, { "success": false, "error": "ERROR: updateProductProperty failed - Invalid product address"} );

      const contract   =  productContract.at(id);
      if (contract.getIdentifier() === "")
        return doResponse( reject, 404, { "success": false, "error": "ERROR: updateProductProperty failed - Product not found"} );


      let transactionCounter = 0;
      let transactionsLength = 1 + body.length;
      const done = () => {
        if (++transactionCounter === transactionsLength)
          return doResponse( resolve, 200, { "success": true } ); 
      };
      //
      for (let i = 0; i < body.length; i++){
        contract.addAdditionalProperty( body[i]['name'], body[i]['value'], {from: user.account, data: productBytes, gas: '4700000' }, ( e, aptTransactionHash ) => {
          done();
        });
      };
      done();
    }catch(error){
      console.log(error);
    }
    //
  });// Promise close
}

/**
 * Add contract owner
 * 
 *
 * id String contract to be updated with property
 * body List data to update in contract
 * returns GenericResponse
 **/
exports.updateProductOwner = (id,body) => {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "success" : true
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

/**
 * Update a new Product contract
 * 
 *
 * id String contract whose data to get
 * body Product 
 * returns ProductSchema
 **/
exports.updateProduct = (user,id,body) => {
  console.log('updateProduct user=', user);
  console.log('updateProduct id=', id);
  console.log('updateProduct body=', body);
  //
  return new Promise( (resolve, reject) => {
    //
    try {
      if (!web3.isAddress(id))
        return doResponse( reject, 404, { "success": false, "error": "ERROR: updateProduct failed - Invalid product address"} );

      const contract   =  productContract.at(id);
      if (contract.getIdentifier() === "")
        return doResponse( reject, 404, { "success": false, "error": "ERROR: updateProduct failed - Product not found"} );


      let transactionCounter = 0;
      let transactionsLength = 1 + body.length;
      const done = () => {
        if (++transactionCounter === transactionsLength)
          return doResponse( resolve, 200, { "success": true } ); 
      };

      if (body.hasOwnProperty('identifier')){
        transactionsLength++;
        contract.setIdentifier( body.identifier, {from: user.account, data: productBytes, gas: '4700000' }, (e, edtTransactionHash) => {
          done();
        })
      };
      if (body.hasOwnProperty('comChain')){
        transactionsLength++;
        contract.setComChain( body.comChain, {from: user.account, data: productBytes, gas: '4700000' }, (e, edtTransactionHash) => {
          done();
        })
      };
      if (body.hasOwnProperty('extraData')){
        transactionsLength++;
        contract.setExtraData( body.extraData, {from: user.account, data: productBytes, gas: '4700000' }, (e, edtTransactionHash) => {
          done();
        })
      };
      if (body.hasOwnProperty('category')){
        transactionsLength++;
        contract.setCategory( web3.fromAscii(body.category), {from: user.account, data: productBytes, gas: '4700000' }, (e, edtTransactionHash) => {
          done();
        })
      };
      if (body.hasOwnProperty('owner')){
        transactionsLength++;
        let owners;
        if (!Array.isArray(body.owner))
          owners = [body.owner];
        else 
          owners = body.owner;
        contract.setOwners( owners, {from: user.account, data: productBytes, gas: '4700000' }, (e, edtTransactionHash) => {
          done();
        })
      };
      //
      for (let i = 0; i < body.length; i++){
        contract.addAdditionalProperty( body[i]['name'], body[i]['value'], {from: user.account, data: productBytes, gas: '4700000' }, ( e, aptTransactionHash ) => {
          done();
        });
      };
      done();
    }catch(error){
      console.log(error);
    }

    //
  }); //Promise close
  //
}


/**
 * Bulk Update contract with metadata
 * 
 *
 * body List data to update in contract
 * returns List
 **/
exports.updateProductBulkMetadata = (user, body) => {
  return new Promise( (resolve, reject) => {

  })
}



exports.findProductObj = (user, identifier) => new Promise( (resolve, reject) => {

  console.log(">>>>>>>>>", identifier);
      try {
        const toBlock       = 'latest';// 5956; //latest
        const topic         = web3.sha3('NewProduct(address,string,bytes32)');
        const getFilter     = web3.eth.filter({fromBlock: startBlock, toBlock: toBlock, topics: [topic] });
        getFilter.get( (error, results) => {
          let address;
          for (let result of results){
               const event  = productSample.NewProduct().formatter(result); 
               if ( event.args.identifier === identifier){
                address = event.address;
                break
               };
          }
          console.log(address);
          if (!address)
            return doResponse( reject, 404, { "success": false, "error": "ERROR: findProductObj failed - Invalid product identifier"} )


          const pContract         = productContract.at( address );
          const owners            = pContract.getOwners();
          let i;

          for ( i = 0; i < owners.length; i++ ){
            owners[i] = {
              "@context"    : schema.contextSchema,
              "@type"       : schema.memberSchemeType,
              "@id"         : owners[i]
            }
          };


          const additionalProperty  = [];
          const propertiesKeys      = pContract.getAdditionalPropertyKeys();

          for ( i = 0; i < propertiesKeys.length; i++ ){
              const key   = utils.getClearString( propertiesKeys[i], true);
              const value = pContract.getAdditionalProperty( propertiesKeys[i] );
              const ap    = {
                "@context"    : "http://schema.org",
                "@type"       : "PropertyValue",
                "name"        : key,
                "value"       : value
              }
              additionalProperty.push(ap);
          };

          const product = {
            "@context"            : schema.contextSchema,
            "@type"               : schema.productSchemeType,
            "oAddress"            : pContract.address,
            "id"                  : pContract.address, 
            "identifier"          : pContract.getIdentifier(),
            "category"            : utils.getClearString(pContract.getCategory(), true),
            "additionalProperty"  : additionalProperty,
            "owner"               : owners,
            "comChain"            : {
              "@context"      : schema.contextSchema,
              "@type"         : schema.comChainSchemeType,
              "@id"           : pContract.getComChain()
            },
            "extraData"           : {
              "@context"      : schema.contextSchema,
              "@type"         : schema.extraDataSchemeType,
              "@id"           : pContract.getExtraData()
            }
          };

          return doResponse( resolve, 200, product ); //Success // << TO DO GET MEMBERS 


        })
      }catch(error){
        console.log(error);
      }
});

const mdeStores = () => {
      mdsProductMap = {};
      const toBlock       = 'latest';// 5956; //latest
      const topic         = web3.sha3('NewProduct(address,string,bytes32)');
      const getFilter     = web3.eth.filter({fromBlock: startBlock, toBlock: toBlock, topics: [topic] });
      getFilter.get( (error, results) => {
          const ch = (i) => {
            const pContract       = productContract.at(results[i].address);
            const providers       = pContract.getMetaDataProviders();
            for (let j = 0; j < providers.length; j++){
              let c = pContract.getMetaDataStore( providers[j] );
              mdsProductMap[c] = results[i].address;
            }
          };
          for (let i = 0; i < results.length; i++){
             ch(i);
          };
          console.log('mdsProductMap',mdsProductMap);
      })
};

setTimeout(()=> { mdeStores() }, 2000);

/*

const killThemAll = () => {


const contractsToKill = ['0xf79479e9bb5215a1aaaf22b72a8c55d9a9b6f79c',
  '0x8461ee6a04bbbf62a0b5a6c86b78cc8dc0f9cc01',
  '0x845a0a1fa524d262d0bdd6ebee1f43a8b31ca441',
  '0xefdcbad9d4a2a8348a164aed704592901fd049e2',
  '0x141a08796a0e3cc0d91bcdf6a8169e7ae5c950fb',
  '0x2b9489518945721f40dcbbf37011a2d3a0be479a',
  '0xcb9e8e36983b13c2e43f3389a1c2dc3481493bfc',
  '0xedfb07834737280593a13e7b22b15fde8f6b04fb',
  '0x88c747507eef4e0242e0341dfc225894683515b1',
  '0xb1373b3e5aa480826d59de8b1bc263f35e689b77'];

let ac = {}
ac['0xf79479e9bb5215a1aaaf22b72a8c55d9a9b6f79c'] = '0x0e666f3c0f7b5d07d5b8de2e52ac300bd3e85912';
ac['0x8461ee6a04bbbf62a0b5a6c86b78cc8dc0f9cc01'] = '0x0e666f3c0f7b5d07d5b8de2e52ac300bd3e85912';
ac['0x845a0a1fa524d262d0bdd6ebee1f43a8b31ca441'] = '0x0e666f3c0f7b5d07d5b8de2e52ac300bd3e85912';
ac['0xefdcbad9d4a2a8348a164aed704592901fd049e2'] = '0x0e666f3c0f7b5d07d5b8de2e52ac300bd3e85912';
ac['0x141a08796a0e3cc0d91bcdf6a8169e7ae5c950fb'] = '0x6600a582c5752375efffdf336b054a79f27f5986';
ac['0x2b9489518945721f40dcbbf37011a2d3a0be479a'] = '0x6600a582c5752375efffdf336b054a79f27f5986';
ac['0xcb9e8e36983b13c2e43f3389a1c2dc3481493bfc'] = '0x6600a582c5752375efffdf336b054a79f27f5986';
ac['0xedfb07834737280593a13e7b22b15fde8f6b04fb'] = '0x6600a582c5752375efffdf336b054a79f27f5986';
ac['0x88c747507eef4e0242e0341dfc225894683515b1'] = '0x6600a582c5752375efffdf336b054a79f27f5986';
ac['0xb1373b3e5aa480826d59de8b1bc263f35e689b77'] = '0x6600a582c5752375efffdf336b054a79f27f5986';


var jwt = require('./../utils/jwt');
var n = jwt.decrypt({pass:  '0c9a20294f172da3ee851c0d81c1b6b88b28abcd06c0dccdfe0729039cda9c9dff79080a4ec6d87e806f5c62ba523f62YYIR7jWtUHrzEl4D3WuxkQ==', acc: '0850726b5369c152f0c168e982cb34a3a664bc752d522dbf3d63f3de895259bfab8c4c3964573a82a9cc229fefd1594dF3RSI0Q403x37xiCDEIzy+9vCYUFMxJsh6TSmUXcAVkxAf8Ce4/cpJhN5Aj2UyiQ' })

console.log('n', n)

//unlockAccount('0x0e666f3c0f7b5d07d5b8de2e52ac300bd3e85912', n.pass)
//unlockAccount('0x6600a582c5752375efffdf336b054a79f27f5986', n.pass) 
unlockAccount(n.acc, n.pass) 

  setTimeout(()=> {
    for (let i = 0; i < contractsToKill.length; i++){  
      const contract  = productContract.at(contractsToKill[i]);
      console.log('contractsToKill[i]', contractsToKill[i], contract.getInitiator());
      contract.kill( {from: n.acc, gas: '5700000' }, (e, th) => {
        console.log('kill', e, th);
      });
    };
  },500);

}

setTimeout(() => {
  killThemAll()
}, 30000)

*/

