'use strict';

const ccType          = require('../elastic/ElasticSearchConnection').ccType;
const esUtils         = require('../elastic/EsUtils');
const extraDataIndex  = require('../elastic/ElasticSearchConnection').extraDataIndex;
const ResponsePayload = require('../utils/writer').respondWithCode;
const _               = require('lodash');


/**
 * Add a new extraData object
 * 
 *
 * body ExtraData MiFID2 template
 * returns ExtraDataSchema
 **/
exports.createExtraData = (user,body) => new Promise( (resolve, reject) => {
  console.log('createExtraData body=', body);

     if ( _.isEmpty(body) ) 
       return esUtils.doResponse( reject, 422, { "success": false, "error": "ERROR: updateExtraData failed - Bad input data"} );



    let extraData = {
      "identifier"          : body.identifier,
      "storagePeriod"       : body.storagePeriod,
      "refreshPeriod"       : body.refreshPeriod,
      "catchUpTime"         : body.catchUpTime,
      "audit"               : body.audit,
      "additionalProperty"  : body.additionalProperty
    };

    return esUtils.add(extraDataIndex, ccType, extraData)  //create new ExtraData
      .then( resp => {
        //console.log('createExtraData resp=', JSON.stringify(resp, null, 4));
        if (resp === null || resp === undefined || resp.result !== "created") //failed to create new doc
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: createExtraData failed - New ExtraData not created"} );

        extraData.id          = resp._id;  //add bits to extraData for final JSON response
        extraData["@type"]    = esUtils.extraDataSchemeType;
        extraData["@context"] = esUtils.contextSchema;
        return esUtils.doResponse( resolve, 200, extraData ); //Success
      }) //end create new member
    .catch( error => {
      return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: createExtraData failed - " + error.message } );
    })
});


/**
 * Deletes a ExtraData object
 * 
 *
 * id String ExtraData to delete
 * returns GenericResponse
 **/
exports.deleteExtraData = (user,id) => new Promise( (resolve, reject) => {
  console.log('deleteExtraData user=', user);
  console.log('deleteExtraData id=', id); 
    return esUtils.deleteDoc(extraDataIndex, ccType, id) //delete ExtraData
      .then( resp => {
        if (resp === null || resp === undefined || resp.result !== "deleted") //failed to delete doc
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: deleteExtraData failed - delete response does not have body"} )

        console.log("deleteExtraData succeeded id=", id);
        return esUtils.doResponse( resolve, 200, {"success": true} ); //Success
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: deleteExtraData failed - " + error.message } );
      })
});

/**
 * Returns all MiFID2 objects
 * 
 *
 * returns List
 **/
exports.getExtraDataAll = (user, identifier) => new Promise(function(resolve, reject) {
  console.log('getExtraDataAll user=', user);
    let query = {};
    try {
    
      if (typeof identifier === "string" && identifier.length > 0) {
          //query   =  { match:  { identifier:  identifier } };
          query = {
              "constant_score" : { 
                 "filter" : { 
                    "term" : {"identifier.keyword" : identifier}
                 }
              }
           };
      } else 
          query   = { match_all: {} };


      return esUtils.checkIfExistInSearch(extraDataIndex, ccType, query, true) //search for ExtraData
        .then( extraDatas => {
          if (!extraDatas) // no extraDatas 
            return doResponse( resolve, 200, [] )
            //return esUtils.doResponse( reject, 404, { "success": false, "error": "ERROR: getExtraDataAll failed - ExtraDatas not found"} );

          //console.log("getExtraDataAll extraDatas=", extraDatas);
          let extraDatasObj = [];
          let extraDataObj = {};
          for (let i = 0; i < extraDatas.length; i++){
            let extraData = extraDatas[i];
            //console.log("getExtraDataAll succeeded extraData=", extraDatas);
            let extraDataObj = extraData._source;
            extraDataObj.id = extraData._id;  //add bits to extraData for final JSON response
            extraDataObj["@type"] = esUtils.extraDataSchemeType;
            extraDataObj["@context"] = esUtils.contextSchema;
            //console.log("getExtraDataAll extraDataObj=", extraDataObj);
            extraDatasObj.push(extraDataObj);
          } //end for
          return esUtils.doResponse( resolve, 200, extraDatasObj ); //Success
        } )
        .catch( error => {
          return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: getExtraDataAll failed - " + error.message } );
        })
    }catch(error){
      console.log(error);
    }
})


/**
 * Update a new extraData object
 * 
 *
 * id String contract to be updated with who is responsible for providing data for fields
 * body ExtraData data to update in contract
 * returns ExtraDataSchema
 **/
exports.updateExtraData = (user,id,body) => new Promise( (resolve, reject) => {
  console.log('updateExtraData user=', user);
  console.log('updateExtraData id=', id);
  console.log('updateExtraData body=', body);
     
    if ( _.isEmpty(body) ) 
       return esUtils.doResponse( reject, 422, { "success": false, "error": "ERROR: updateExtraData failed - Bad input data"} );

    let query = {
        "constant_score" : { 
          "filter"    : { 
              "term"  : { "_id" : id }
          }
        }
    }; 


    return esUtils.checkIfExistInSearch(extraDataIndex, ccType, query) //search for ExtraData
      .then( extraData => {
        if (!extraData) // no such extraData
          return esUtils.doResponse( reject, 404, { "success": false, "error": "ERROR: updateExtraData failed - ExtraData not found"} );

        let extraDataObj = {};

        const updateExDatBody =  {
          script: {
            source: "ctx._source = params.pBody",
            params: {pBody: body},
            lang: "painless"
          }
        };

        return esUtils.update( extraDataIndex, ccType, id,  updateExDatBody ) //update passed in body into existing extraData 
          .then( resp  => {
            if (resp === null || resp === undefined || resp.result !== "updated") //failed to update existing doc
                return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateExtraData failed - Failed to update ExtraData"} );

            let extraDataObj = body;
            extraDataObj.id = extraData._id;  //add bits to extraData for final JSON response
            extraDataObj["@type"] = esUtils.extraDataSchemeType;
            extraDataObj["@context"] = esUtils.contextSchema;

            return esUtils.doResponse( resolve, 200, extraDataObj ); //Success
          });
      } )
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateExtraData failed - " + error.message } );
      })
})


exports.updateExtraDataProperty = (user,id,body) => new Promise( (resolve, reject) => {
    let query = {
        "constant_score" : { 
           "filter" : { 
              "term" : {"_id" : id}
           }
        }
     };
    return esUtils.checkIfExistInSearch(extraDataIndex, ccType, query) //search for ExtraData
      .then( extraData => {
        if (!extraData) // no such extraData
          return esUtils.doResponse( reject, 404, { "success": false, "error": "ERROR: updateExtraDataProperty failed - ExtraData not found"} );

        //
        let edSrc                     = extraData._source;
        let additionalProperty        = edSrc.additionalProperty; //additionalProperty is an array
        edSrc.additionalProperty      = _.unionBy(body, additionalProperty, 'name'); //does union of arrays by using 'name' and takes vals from 1st array i.e. body

        return esUtils.addWithId( extraDataIndex, ccType, id, edSrc )  //add additionalProperty to Product
          .then( resp  => {
            if (resp === null || resp === undefined || resp.result !== "updated") //failed to update existing doc
              return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateProductProperty failed - Existing Product not updated with input data"} );

            return esUtils.doResponse( resolve, 200, {"success": true} ); //Success
        })
        //

      })
      .catch( error => {
        return esUtils.doResponse( reject, 500, { "success": false, "error": "ERROR: updateExtraDataProperty failed - " + error.message } );
      })
})
