'use strict';

const fs        		= require('fs');
const path        		= require('path');
const {Client} 	= require('@elastic/elasticsearch');
const { errors } = require('@elastic/elasticsearch');
const { events } = require('@elastic/elasticsearch')
//const esUtils = require('./EsUtils');
const jwt 				= require('../utils/jwt');
const config 			= require('config');
const mappings 			= require('../config/mappings');

let {PythonShell} = require('python-shell');

const ccType 			= '_doc';


let esClient, storage, username, password, host, port, prefix, esCert;
let credentialsFromService, credentialsPythonScriptPath, credentialsPythonScriptName;

if (config.has('Storage.type')) 
	storage = config.get('Storage.type');

if (config.has('Storage.prefix'))
	prefix = config.get('Storage.prefix');

console.log('ES prefix', prefix);

let productIndex 		= prefix ? prefix + 'comchain-product' 		: 'comchain-product';
let extraDataIndex 		= prefix ? prefix + 'comchain-extradata' 	: 'comchain-extradata';
let communityIndex 		= prefix ? prefix + 'comchain-community' 	: 'comchain-community';
let memberIndex 		= prefix ? prefix + 'comchain-member' 		: 'comchain-member';
let mifid2Index 		= prefix ? prefix + 'comchain-mifid2' 		: 'comchain-mifid2';
let groupIndex 			= prefix ? prefix + 'comchain-group' 		: 'comchain-group';
let MDERecordIndex 		= prefix ? prefix + 'comchain-mderecord' 	: 'comchain-mderecord';
let ITSRecordIndex 		= prefix ? prefix + 'comchain-itsrecord' 	: 'comchain-itsrecord';


//MDERecordIndex = "mapping-tests"+MDERecordIndex;
//ITSRecordIndex = "mapping-tests"+ITSRecordIndex;

if (storage === "ElasticSearch") {
	if (config.has('Storage.username')) {
		username = config.get('Storage.username');
	}
	console.log('username', username);

	if (config.has('Storage.password')) 
		password = config.get('Storage.password');
	
	//console.log('password', password);

	if (config.has('Storage.host')) 
		host = config.get('Storage.host');

	console.log('host', host);

	if (config.has('Storage.port')) 
		port = config.get('Storage.port');
	
	console.log('port', port);

	const establishEs = async () => {
		let url 	= host + ":" + port;
		console.log('ElasticSearch url:', url);
		if (host.toLowerCase().startsWith('https')) {
			esCert = config.get('escert');
			console.log('escert', esCert);
			esClient 	= new Client( {
				node: url,
				requestTimeout: 60000,
				auth: {
					username: username,
					password: password
				},
				ssl: {
					ca: fs.readFileSync(esCert),
					rejectUnauthorized: true
				}
			});
		} else { //http
			console.log('ElasticSearch before creating new http client:');
			esClient 	= new Client( {
				node: url,
				requestTimeout: 60000,
				auth: {
					username: username,
					password: password
				}
			});
			console.log('ElasticSearch after creating new http client:');
		}
	 
		//test connection
		console.log('ElasticSearch before ping');
		const pingSuccess = await esClient.ping({}, {
			requestTimeout: 20000 // ping usually has a 3000ms timeout
		});
		console.log('ElasticSearch after ping pingSuccess:', JSON.stringify(pingSuccess, null, 4));
		if (!pingSuccess) {
			console.log('ERROR: ElasticSearch ping failed: ', error);
		} else {
			console.log('ElasticSearch ping succeeded');
			createIndex(productIndex, mappings.Product, console.log);
			createIndex(extraDataIndex, mappings.ExtraData,  console.log);
			createIndex(communityIndex, mappings.Community, console.log);
			createIndex(mifid2Index, null, console.log);
			createIndex(groupIndex, mappings.Group, console.log);
			createIndex(MDERecordIndex, mappings.MDERecord, console.log);
			createIndex(ITSRecordIndex, mappings.ITSRecord, console.log);
			createIndex(memberIndex, mappings.Member, console.log);
			/*createAdminMember( 'audit', false);
			createAdminMember( 'cas', false);
			createAdminMember( 'nice', false);
			createAdminMember( 'ITS', false);
			setTimeout(() => {
				createAdminMember( 'api_admin', true);
				createAdminMember( 'onboard', true);
				createAdminMember( 'VQTS', false);
				createAdminMember( 'zoom', false);
				createAdminMember( 'direct_cti', false);
			}, 2000);*/
		}

		//console.log(errors);
		//console.log(events);
		/*esClient.diagnostic.on('request', (err, result) => {
		  if (err) {
		    console.log("request err:", JSON.stringify(err, null, 4))
		  } else {
		    console.log("request result:", JSON.stringify(result, null, 4))
		  }
		});*/
		esClient.diagnostic.on('response', (err, result) => {
		  if (err) {
		    console.log("response err:", JSON.stringify(err, null, 4))
		  } else {
		    //console.log("response result:", JSON.stringify(result.body, null, 4))
		  }
		})
	}


	credentialsFromService = false;
	if (config.has('Storage.credentialsFromService')) 
		credentialsFromService = config.get('Storage.credentialsFromService');
	console.log('credentialsFromService', credentialsFromService);
	if (credentialsFromService) {
		//get credentials from REST calls & set username & password used for building the ES Client connection
		if (config.has('Storage.credentialsPythonScriptPath')) 
			credentialsPythonScriptPath = config.get('Storage.credentialsPythonScriptPath');
		console.log('credentialsPythonScriptPath', credentialsPythonScriptPath);

		if (config.has('Storage.credentialsPythonScriptName')) 
			credentialsPythonScriptName = config.get('Storage.credentialsPythonScriptName');
		console.log('credentialsPythonScriptName', credentialsPythonScriptName);

		let options = {
			mode: 'text',
			pythonPath: 'python',
			pythonOptions: ['-u'], // get print results in real-time
			scriptPath: credentialsPythonScriptPath,
			args: []
		};

		/*PythonShell.run(credentialsPythonScriptName, options, function (err, results) {
			if (err) throw err;
			// results is an array consisting of messages collected during execution
			console.log('python script done, results: ', results);
			establishEs();
			});*/
		let pyshell = new PythonShell(credentialsPythonScriptName, options);
		let pythonData = []
		pyshell.on('message', function (message) {
			pythonData.push(message);
  			//console.log(message);
			});
 
		pyshell.end(function (err,code,signal) {
  			if (err) throw err;
  			//console.log('pythonData=' + pythonData);
  			console.log('python script completed. The exit code was:', code);
  			if (pythonData.length === 3) {
  				if (pythonData[1].indexOf("=") > -1) {
  					username = pythonData[1].split("=")[1];
  				}
  				if (pythonData[2].indexOf("=") > -1) {
  					password = pythonData[2].split("=")[1];
  				}
  				//console.log('REST call via python script updated username', username);
  				//console.log('REST call via python script updated password', password);
  			}
			establishEs();
			});
		console.log('after PythonShell.run');
	} else {
		establishEs();
	}

	 // 
} else {
	console.log('Error trying to configure/create connection');
}


const createAdminMember = async function( identifier, isSuperAdmin ){
	//console.log("createAdminMember start. identifier:", identifier);
	const result = await esClient.search({
	 	index: memberIndex,
	 	body 	: {
      query: { match: { identifier: identifier } }
    }
	});
	//console.log("createAdminMember result:", JSON.stringify(result, null, 4));
	console.log("createAdminMember result.hits.total.value:", result.hits.total.value);
	if (result.hits.total.value > 0) {
		console.log("createAdminMember member already exists, identifier:", identifier);
		let token = jwt.genToken( result.hits.hits[0]['_id'] );
		console.log(identifier,' TOKEN: ', token);
		return
	}

	console.log("createAdminMember member doesn't exist, so create it. identifier:", identifier);
	let member = {
		identifier 	:  identifier,
		userType 		: 'admin'
	}

	await esClient.index({
    index: memberIndex,
    body: member
  });

	let token = jwt.genToken( newMember['_id'] );
	console.log(identifier,' TOKEN: ', token);
	/*esClient.search({
        index 	: memberIndex,
        body 	: {
          query: { match: { identifier: identifier } }
        }
     })
	.then( resp => {
		//console.log('createAdminMember', identifier, resp.body.hits.total.value );
		if (resp.body.hits.total.value > 0){
			let token = jwt.genToken( resp.body.hits.hits[0]['_id'] );
			console.log(identifier,' TOKEN: ', token);
			return
		}

		let member = {
	  		identifier 	:  identifier,
	  		userType 		: 'admin'
		}

		esClient.index({
		      index: memberIndex,
		      body: member
		    })
			.then( newMember => {
				//console.log(' new member response ', newMember);

				let token = jwt.genToken( newMember['_id'] );
				console.log(identifier,' TOKEN: ', token);

			})
			.catch( err => {
				console.log( 'new member ERRR', err);
			});
	})
	.catch( err => {
		console.log( 'createAdminMember ERRR', err);
	})*/
};

const createIndex = async function( indexName, mapping, callback ){
	console.log('createIndex indexName', indexName);
	//console.log('esClient.indices.putMapping', esClient.indices.putMapping);
	const indexExists = await esClient.indices.exists({
	 	index: indexName
	});
	if (indexExists) {
		console.log("createIndex index already exists indexName:", indexName);
	} else {
		console.log("createIndex index doesn't exist, so create it. indexName:", indexName);
		const createdIndex = await esClient.indices.create({
		  index: indexName
		});

		if (createdIndex) {
			if (mapping){
				//console.log('mapping', mapping);
				const addedMapping = await esClient.indices.putMapping({  
				  index: indexName,
				  body: {
				  	properties: mapping
				  }
				});
				if (addedMapping) {
					console.log("Mapping added for index. indexName:", indexName);
				} else {
					console.log("Failed to add mapping for index. indexName:", indexName);
				}
			}else {
				console.log("No mapping provided for index. indexName:", indexName);
			}
		} else {
				console.log("Failed to created indexName:", indexName);
		}
	}

	if (indexName === memberIndex) {
		createAdminMember( 'audit', false);
		createAdminMember( 'cas', false);
		createAdminMember( 'nice', false);
		createAdminMember( 'ITS', false);
		setTimeout(() => {
			createAdminMember( 'api_admin', true);
			createAdminMember( 'onboard', true);
			createAdminMember( 'VQTS', false);
			createAdminMember( 'zoom', false);
			createAdminMember( 'direct_cti', false);
		}, 2000);
	}
	/*function(err,resp) {
		//console.log('createIndex 1', indexName, JSON.stringify(resp.body, null, 4) );
		if(err) {
			if (typeof callback === 'function')
		    	callback(err, null);
		} else {
		    if (resp.body === false){
				esClient.indices.create({
				  index: indexName
				}, function(err,resp) {
					//console.log('createIndex 2', indexName, JSON.stringify(resp.body, null, 4) );
					if (mapping){
						//console.log('mapping', mapping);
						esClient.indices.putMapping({  
						  index: indexName,
						  body: {
						  	properties: mapping
						  }
						},function(err,resp){
							console.log('createIndex', indexName );
							//console.log("mapping err=" , err);
						   	if (typeof callback === 'function')
								callback(err, resp.body);
						});
						//
					}else {
						if (typeof callback === 'function')
							callback(err, resp.body);
					}
				});
		    } else {
		    	if (typeof callback === 'function')
		    		callback('Index ' + indexName + ' already exists', JSON.stringify(resp.body, null, 4));
		    };
		};
	}*/
};

const removeIndex = function( indexName, callback ){
	esClient.indices.delete ({
	  index: indexName
	},callback);
}

module.exports.productIndex 	= productIndex;
module.exports.extraDataIndex = extraDataIndex;
module.exports.communityIndex = communityIndex;
module.exports.memberIndex 	= memberIndex;
module.exports.mifid2Index 	= mifid2Index;
module.exports.groupIndex 	= groupIndex;
module.exports.MDERecordIndex = MDERecordIndex;
module.exports.ITSRecordIndex = ITSRecordIndex;

module.exports.esClient 		= esClient;
module.exports.ccType 		= ccType;
