'use strict'

const axios 	= require('axios');
const _ 		= require('lodash');

const axiosInstance = axios.create({
	  timeout: 1000 * 60
	});

module.exports.getCredentialDatum = (url) => new Promise( ( resolve, reject ) => {
	console.log('getCredentialDatum start, url=', url);
	axiosInstance.get(url)
	.then( response => {
	    //console.log('response.data', response.data);
	    console.log('response.status', response.status);
	    console.log('response.statusText', response.statusText);
	    //console.log('response.headers', response.headers);
	    //console.log('response.config', response.config);
		resolve (response.data) 
	})
	.catch( error => {
		if (error.response) {
	      // The request was made and the server responded with a status code
	      // that falls out of the range of 2xx
	      //console.log('getCredentialDatum', error.response.data);
	      //console.log('getCredentialDatum', error.response.status);
	      //console.log('getCredentialDatum', error.response.headers);
	    } else if (error.request) {
	      // The request was made but no response was received
	      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
	      // http.ClientRequest in node.js
	      //console.log('getCredentialDatum', error.request);
	    } else {
	      // Something happened in setting up the request that triggered an Error
	      //console.log('getCredentialDatum Error ', error.message);
	    }

		reject(error);
	})
});