'use strict';

const esClient = require('./ElasticSearchConnection').esClient;
const ccType = require('./ElasticSearchConnection').ccType;
const ResponsePayload = require('../utils/writer').respondWithCode;

// TO DO - moved to utlis/schema
const contextSchema = "http://comchainalliance.org/schema/";
const memberSchemeType = "Member";
const communitySchemeType = "Community";
const groupSchemeType = "Group";
const productSchemeType = "Product";
const extraDataSchemeType = "ExtraData";
const mifid2SchemeType = "MiFID2";


const defaultGroup = "default";


var product = {
  "@context" : "http://schema.org/",
  "@type" : "Product",
  "id" : "id",
  "identifier" : "identifier",
  "category" : "category",
  "additionalProperty" : [ {
    "@context" : "http://schema.org/",
    "@type" : "PropertyValue",
    "name" : "string",
    "value" : "string"
  }, {
    "@context" : "http://schema.org/",
    "@type" : "PropertyValue",
    "name" : "string",
    "value" : "string"
  } ],
  "owns" : {
    "@context" : "http://schema.org/",
    "@type" : "Person",
    "@id" : "string"
  },
  "extraData" : {
    "@context": "http://comitfs.com",
      "@type": "ExtraData",
    "@id" : "@id"
  }
};
module.exports.product = product;

var extraData = {
  "@context": "http://comitfs.com",
    "@type": "ExtraData",
  "id" : "id",
    "identifier": "string",

    "template": {
      "@context": "http://comitfs.com",
      "@type": "MIFiD2",
      "@id": "$templateRef"
    },

    "storagePeriod": 0,       
    "refreshPeriod": "PerCall",   
};
module.exports.extraData = extraData;

var mifid2 = {
  /* .... */
};
module.exports.mifid2 = mifid2;

var mdeRecord = {
  "@context": "http://comitfs.com",
    "@type": "MDERecord",

    "product": {
    "@context" : "http://schema.org/",
    "@type" : "Product",
    "@id": "$ref"
    },

    "provider": {
      "@context": "http://schema.org/",
      "@type": "Person",
      "@id": "$ref"
    },

    "fields": {
      "@context": "http://comitfs.com",
      "@type": "MIFiD2",
        "@id": "$ref"
    }
};
module.exports.mdeRecord = mdeRecord;

var member = {
  "@context": contextSchema,
  "@type": "Member",
  "address": {
    "@context": "http://schema.org",
    "@type": "Place",
    "addressLocality": "string",
    "addressRegion": "string",
    "postalCode": "string",
    "streetAddress": "string"
  },
  "identifier": "identifier",
  "regulated": true,
  "familyName": "familyName",
  "givenName": "givenName",
  "id": "id",
  "groups": [], //just group Ids as text
  "memberOf": [], //just community Ids as text
  "retentionHold": false
}
module.exports.member = member;

var organisation = {
  "@context" : "http://schema.org/",
  "@type" : "Organization",
  "id" : "id",
  "name" : "name",
  "funder" : {
    "@context" : "http://schema.org/",
    "@type" : "Person",
    "@id" : "string"
  },
  "groups" : [ {
    "@context" : "http://schema.org/",
    "@type" : "Brand",
    "@id" : "string"
  }, {
    "@context" : "http://schema.org/",
    "@type" : "Brand",
    "@id" : "string"
  } ],
  
};
module.exports.organisation = organisation;

var brand = {
  "@context" : "http://schema.org/",
  "@type" : "Brand",
  "id" : "string",
  "name" : "string",
  "regulated" : true
};
module.exports.brand = brand;

//------------------------------------------------------------------------
// TO DO - require utils/response !!
//Generic JSON response back to client 
var doResponse = function(controllerCallback, httpCode, bodyJSON) {
  controllerCallback (new ResponsePayload(httpCode, bodyJSON));
};
module.exports.doResponse = doResponse;

// checks whether element is true
var ifTrue = function(element) {
  return element === true;
};
module.exports.ifTrue = ifTrue;

var createNewMember = function(body, groups, id) {
  member = {
   // "@context": contextSchema,
   // "@type": memberSchemeType,
    "address": {
      "@context": "http://schema.org",
      "@type": "Place",
      "addressLocality": body.address.addressLocality,
      "addressRegion": body.address.addressRegion,
      "postalCode": body.address.postalCode,
      "streetAddress": body.address.streetAddress
    },
    "additionalProperty": body.additionalProperty,
    "identifier": body.identifier,
    "name": body.name,
    //"familyName": body.familyName,
    //"givenName": body.givenName,
    "groups": groups, //just group Ids as text
    "memberOf": id ? [id] : [], //just community Ids as text
    "retentionHold": body.retentionHold,
    "email": body.email
  };
  return member;
}
module.exports.createNewMember = createNewMember;
//------------------------------------------------------------------------
var add = function( index, type, body ) {
    return esClient.index({
      index: index,
      refresh: "wait_for",
      body: body
    })
};

var addWithId = function( index, type, id, body ) {
    return esClient.index({
      index: index,
      id: id,
      refresh: "wait_for",
      body: body
    })
};

var update = function( index, type, id, body ) {
  return esClient.update({
      index: index,
      id: id,
      refresh: "wait_for",
      body: body
    })
};

var deleteDoc = function( index, type, id ) {
  return esClient.delete({
      index: index,
      refresh: "wait_for",
      id: id
    })
};

var search = function( index, type, query,  size) {
   if (!size)
     size = 10000// << test it

   return esClient.search({
        index: index,
        body: {
          from : 0, 
          size: size,
          query: query
        }
      })
};

var searchLimitResult = function( index, type, query, sourceFields, size) {
   if (!size)
     size = 10000// << test it
   return esClient.search({
        index: index,
        _source: sourceFields,
        body: {
          from : 0, 
          size: size,
          query: query
        }
      })
};

var checkIfExistInSearch = function( index, type, query, returnAll) {
   return new Promise(function(resolve, reject) {
      //console.log(">>>>>>>>>>>>>>",index,"<<<<<<<<<<<<<<")
      search( index, type, query)
        .then( function( resp ) {
            //console.log('checkIfExistInSearch: resp=', JSON.stringify(resp, null, 4));
            if (resp && resp.hits && resp.hits.total && resp.hits.total.value > 0)
              if (returnAll)
                return resolve(resp.hits.hits);
              else
                return resolve(resp.hits.hits[0])
            else
              return resolve( null );
            
        })
        .catch( reject )
   });
};

var checkIfExistInSearchLimitResult = function( index, type, query, returnAll, sourceFields) {
   return new Promise(function(resolve, reject) {
      //console.log(">>>>>>>>>>>>>>",index,"<<<<<<<<<<<<<<")
      searchLimitResult( index, type, query, sourceFields)
        .then( function( resp ) {
            //console.log('checkIfExistInSearchLimitResult: resp=', JSON.stringify(resp, null, 4));
            if (resp && resp.hits && resp.hits.total && resp.hits.total.value > 0)
              if (returnAll)
                return resolve(resp.hits.hits);
              else
                return resolve(resp.hits.hits[0])
            else
              return resolve( null );
            
        })
        .catch( reject )
   });
};

var searchDetermineRegulated = function( indexG, indexM, type, id, size) {
  let sourceFields = ["regulated"]; //limit Group fields output from the below query
  let query = {
    "terms" : {
      "_id" : { //match the _id of Group doc _id with below
        "index" : indexM, //Member doc index
        "id" : id, // Member doc to work on 
        "path" : "groups" //Member doc field to match with Group doc _id
      }
    }
  };
  let postFilter = { 
    "term": { "regulated": true } //filter out false ones from above query
  }

  if (!size)
    size = 10000// << test it

  return esClient.search({
    index: indexG,
    _source: sourceFields,
    body: {
      from : 0, 
      size: size,
      query: query,
      post_filter: postFilter
    }
  })
};

var determineRegulated = function( indexG, indexM, type, id) {
   return new Promise(function(resolve, reject) {
      //console.log(">>>>>>>>>>>>>>determineRegulated<<<<<<<<<<<<<<")
      searchDetermineRegulated( indexG, indexM, type, id)
        .then( function( resp ) {
            //console.log('determineRegulated:resp=', JSON.stringify(resp, null, 4));
            if (resp && resp.hits && resp.hits.total && resp.hits.total.value > 0)
              return resolve(resp.hits.hits);
            else
              return resolve( null );
            
        })
        .catch( reject )
   });
};



var count = function( index, type, query) {
   return esClient.count({
        index: index,
        body: {
          query: query
        }
      })
};

var checkIfExistInCount = function( index, type, query) {
   return new Promise(function(resolve, reject) {
      count( index, type, query)
        .then( function( resp ) {
          console.log('---------- checkIfExistInCount -------------');
          console.log('resp', resp);
          console.log('---------- /// -------------');
            if (resp.count > 0) 
              return resolve(resp.count)
            else
              return resolve( null );
            
        })
        .catch( reject )
   });
};

var deleteIndex = function( indexName){
  esClient.indices.delete({
    index: indexName,
    ignore: [404]
  }).then(function (body) {
    console.log('Index ' + indexName + ' was deleted or never existed');
  }, function (error) {
    console.log('Error in deleting index ' + indexName, error);
  });
};

/// NEW ASYNC METHODS
const scrollSearch = async (index, type, query, size) => {
  let allRecords;
  let scrId;
  try {
    let querySize = Math.min(isNaN(size) ? 10000 : size, 10000);
    //console.log("scrollSearch using size", querySize, new Date());

    const response = await esClient.search({
      index: index,
      scroll: '30s',
      body: {
        size: querySize,
        query: query
      }
    })

    if (response.error)
      return response.error

    scrId = response._scroll_id;
    allRecords = response.hits.hits;

    if (typeof size === 'undefined')
      size = response.hits.total.value;

    let scrollQuery;
    let i = 0;
    while (allRecords.length < size) {
      scrollQuery = await esClient.scroll({
        scrollId: response._scroll_id,
        scroll: '30s'
      })

      if (scrollQuery.error) {
        await esClient.clearScroll({scroll_id: response._scroll_id});
        return scrollQuery.error;
      }

      if (scrollQuery.hits.hits.length === 0) // Cehck it
        break

      allRecords = allRecords.concat(scrollQuery.hits.hits);
      //console.log("scrollSearch allRecords scrolling", allRecords.length, new Date());
    }

    await esClient.clearScroll({scroll_id: response._scroll_id});

  } catch (error) {
    console.log("scrollSearch ERROR _scroll_id= ", scrId, " ", error);
    if (scrId !== 'undefined')
      await esClient.clearScroll({scroll_id: scrId});
    return error;
  }
  //console.log("scrollSearch final allRecords", allRecords.length);
  return allRecords;
};

module.exports.deleteIndex = deleteIndex;

//
module.exports.contextSchema = contextSchema;
module.exports.memberSchemeType = memberSchemeType;
module.exports.communitySchemeType = communitySchemeType;
module.exports.groupSchemeType = groupSchemeType;
module.exports.productSchemeType = productSchemeType;
module.exports.extraDataSchemeType = extraDataSchemeType;
module.exports.mifid2SchemeType = mifid2SchemeType;
module.exports.defaultGroup = defaultGroup;

module.exports.add = add;
module.exports.addWithId = addWithId;
module.exports.update = update;
module.exports.search = search;
module.exports.searchLimitResult = searchLimitResult;
module.exports.deleteDoc = deleteDoc;
module.exports.checkIfExistInSearch = checkIfExistInSearch;
module.exports.checkIfExistInSearchLimitResult = checkIfExistInSearchLimitResult;
module.exports.checkIfExistInCount = checkIfExistInCount;
module.exports.determineRegulated = determineRegulated;

module.exports.scrollSearch     = scrollSearch;


