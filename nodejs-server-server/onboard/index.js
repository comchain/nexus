const _       				= require('lodash');

const communityService 		= require('../service/CommunityEsService');
const productService 		= require('../service/ProductEsService');
const memberService 		= require('../service/MemberEsService');
const groupService 			= require('../service/SubgroupEsService');
const doOnboard = ( adminMember ) => {
	//
	 //
	 if (process.env.ONBOARD!=="yes")
	 	return

	 let sampleData = require('../sample/ms');
	 console.log('\n\n-----------------START DATA ONBOARD--------------------', new Date());
	 console.log('\n----------Reading data----------', new Date());
	 //console.log('sampleData',sampleData);

	 let adminUser = { member: adminMember } // Creator

	 let regulatedGroup = { regulated: true, name: "Recorded Employees", identifier: "regulated"};
	 let groupsToAdd 	= [ regulatedGroup ];
	 let membersToAdd 	= [];
	 let productsToAdd 	= [];

	 for (let employee of sampleData.recordedEmployees){

	 	let memberGroups = []
	 	
	 	for ( let group in sampleData.map.groups ){
	 		let grSource 		= {};
	 		let fields 		=  sampleData.map.groups[group];
	 		
	 		for ( let field in fields ) {
	 			if (field === 'identifier')
	 				grSource[ field ] =   employee[group][ fields[field] ] + ':' + group;
	 			else
	 				grSource[ field ] =   employee[group][ fields[field] ];
	 		}
	 		
	 		// TO DO - do not push group to array if alrady in
	 		let exist = _.find(groupsToAdd, grSource);
	 		if (!exist)
	 			groupsToAdd.push( grSource );

	 		memberGroups.push(grSource);
	 	};

	 	if (employee.activePurpose)
	 		if (!(employee.override) || (employee.override === null))
	 			memberGroups.push( regulatedGroup );

	 	let memberSource 	= {};

	 	for ( let memberField in sampleData.map.member ){
	 		//console.log('\nmemberField:',memberField);
	 		//console.log('sampleData.map.member[memberField]:', sampleData.map.member[memberField] );
	 		if ( memberField === 'additionalProperty'){
	 			let additionalProperty 	= [];
	 			for (let i = 0; i < sampleData.map.member[ memberField ].length; i++){
	 				let objPath = sampleData.map.member[memberField][i].value.split('.');
	 				//console.log('objPath',objPath);
	 				let tmp 	= employee;
	 				for (let ind of objPath){
	 					//console.log('\nind', ind);
	 					if (Array.isArray(tmp)){
	 						for (let ap of tmp)
	 							additionalProperty.push(ap[ind]);

	 						tmp = ind;

	 					}else {
	 						if (additionalProperty.length > 0){
	 							for (let ap = 0; ap <  additionalProperty.length; ap++)
	 								additionalProperty[ap]  = { name: sampleData.map.member[memberField][i].name, value: additionalProperty[ap][ind] };
	 							
	 						}	
	 						tmp = tmp[ind];
	 					};
	 				};
	 			};
	 			memberSource[ memberField ] = additionalProperty;
	 		}else 
				memberSource[ memberField ] = employee[ sampleData.map.member[memberField] ];

	 	};

	 	memberSource.groups = memberGroups;
	 	membersToAdd.push(memberSource);

	 	for ( let product of employee.recentlyRecordedPhones ){
	 		let productSource = {};
	 		for ( let productFiled in sampleData.map.product ){

				let value;
	 			let valueObj 	= employee;

	 			if ( productFiled === 'additionalProperty'){
	 				productSource[productFiled] = []
	 				for (let ad of sampleData.map.product[productFiled]){
	 					if (ad.value.indexOf('.') > -1){
	 						value = ad.value.split('.');
		 					for (let ind of value){
		 						if (ind === "{current}")
		 							valueObj = product;
		 						else 
		 							valueObj = valueObj[ind];
	 						};
							productSource[productFiled].push( {"name": ad.name, "value": valueObj} )
		 				};
	 				};
	 			}else {
	 				value = sampleData.map.product[productFiled];
	 				if (value.indexOf('.') > -1){
	 					value 		= sampleData.map.product[productFiled].split('.'); 
	 					for (let ind of value){
	 						if (ind === "{current}")
	 							valueObj = product;
	 						else 
	 							valueObj = valueObj[ind];
	 					};
	 					productSource[productFiled] = valueObj;
	 				}else {
	 					if (value.indexOf("'") === 0)
	 						value = value.replace(/'/g, '');
	 					productSource[productFiled] = value;
	 				};
	 			}
	 		};
	 		productSource.owner = memberSource.identifier;
	 		productsToAdd.push(productSource);
	 	}
	 };

	 console.log("\n*****groupsToAdd*****", new Date());
	 console.log("groupsToAdd: length=", groupsToAdd.length, "\n", JSON.stringify(groupsToAdd, null, 4));
	 console.log("\n*****membersToAdd*****", new Date());
	 console.log("membersToAdd: length=", membersToAdd.length, "\n", JSON.stringify(membersToAdd, null, 4));
	 console.log("\n*****productsToAdd*****", new Date());
	 console.log("productsToAdd: length=", productsToAdd.length, "\n", JSON.stringify(productsToAdd, null, 4));
	 // add Community if not exist
	 // add groups if not exist 
	 // add members --||--
	 // add products --||--

	 console.log('\n----------Add/Update data----------', new Date(), "\n");
	 const createProducts = () => {
	 	console.log("\n*****createProducts*****", new Date(), "\n");
	 	const createProducts = ( i ) => {
	 		if (i === productsToAdd.length) {
	 			console.log("productsToAdd: length=", productsToAdd.length, "\n", JSON.stringify(productsToAdd, null, 4));
	 			return console.log('\n\n-----------------END DATA ONBOARD--------------------', new Date());
	 		}

	 		productsToAdd[i].owner = [_.find(membersToAdd, {identifier: productsToAdd[i].owner}).id];
	 		
	 		productService.createProduct(adminUser, productsToAdd[i])
	 			.then( response => {
	 				console.log('createProducts new product created', response.payload.id);
	 				productsToAdd[i].id = response.payload.id; //done for debug, as it's not used
	 				createProducts(++i);
	 			})
	 			.catch( error => {
	 				console.log('createProducts error', error);
	 				if (error.payload && error.payload.id) {
	 					console.log('createProducts trying to updateProduct ', error.payload.id);
	 					productService.updateProduct(adminUser, error.payload.id, productsToAdd[i])
				 			.then(response => {
				 				console.log('createProducts succeded in updateProduct ', response);
				 			})
				 			.catch(error => {
				 				console.log('createProducts Error trying to updateProduct ', error.payload.id, error);
				 			})

	 					productsToAdd[i].id = error.payload.id; //done for debug, as it's not used
	 				} else {
	 					console.log('createProducts Error trying to updateProduct Id not found');
	 				}
	 				createProducts(++i); //may need to move this into above if/else branches, making processing synchronous
	 			})
	 		
	 	};
	 	createProducts(0);
	 }

	 const createMembers = ( communityID ) => {
	 	console.log("\n*****createMembers*****", new Date(), "\n");
		const createMember = ( i ) => {
			if (i === membersToAdd.length) {
				console.log("membersToAdd: length=", membersToAdd.length, "\n", JSON.stringify(membersToAdd, null, 4));
				return createProducts();
	 			//return  setTimeout( () => { createProducts() }, 2300 );//next()
	 		}
	 		
	 		for (let j = 0; j <  membersToAdd[i].groups.length; j++){
	 			let groupWithID = _.find( groupsToAdd, { "identifier": membersToAdd[i].groups[j].identifier, "name":  membersToAdd[i].groups[j].name});
	 			membersToAdd[i].groups[j] = groupWithID.id;
	 		};
	 		membersToAdd[i].address = {};// TO DO?

	 		communityService.addCommunityMember(adminUser, communityID, membersToAdd[i])
	 			.then(response => {
	 				console.log('addCommunityMember new member created', response.payload.id);
	 				membersToAdd[i].id = response.payload.id;
	 				createMember(++i);
	 			})
	 			.catch(error => {
	 				console.log('addCommunityMember error', error);
	 				if (error.payload && error.payload.id) {
	 					console.log('addCommunityMember trying to updateCommunityMember ', error.payload.id);
	 					memberService.updateCommunityMember(adminUser, error.payload.id, membersToAdd[i])
				 			.then(response => {
				 				console.log('addCommunityMember succeded in updateCommunityMember ', response);
				 			})
				 			.catch(error => {
				 				console.log('addCommunityMember Error trying to updateCommunityMember ', error.payload.id, error);
				 			})

	 					membersToAdd[i].id = error.payload.id;
	 				} else {
	 					console.log('addCommunityMember Error trying to updateCommunityMember Id not found');
	 				}
	 				createMember(++i); //may need to move this into above if/else branches, making processing synchronous
	 			})
		};
		createMember( 0 );
	 };

	 const createGoups = ( communityID ) => {
	 	//for (let i = 0; i < groupsToAdd.length; i++){
	 	console.log("\n*****createGoups*****", new Date(), "\n");
	 	const createGroup = ( i ) => {
	 		if (i === groupsToAdd.length) {
	 			console.log("groupsToAdd: length=", groupsToAdd.length, "\n", JSON.stringify(groupsToAdd, null, 4));
	 			return createMembers( communityID );
	 			//return setTimeout( () => { createMembers( communityID ) }, 1200);
	 		}

	 		if ( !groupsToAdd[i].hasOwnProperty('regulated' ))
	 			 groupsToAdd[i].regulated = false;

	 		communityService.updateCommunityGroup(adminUser, communityID, groupsToAdd[i])
	 			.then(response => {
	 				console.log('updateCommunityGroup new group created', response.payload.id);
	 				groupsToAdd[i].id = response.payload.id;
	 				createGroup(++i);
	 			})
	 			.catch(error => {
	 				console.log('updateCommunityGroup error', error);
	 				if (error.payload && error.payload.id) {
	 					console.log('updateCommunityGroup trying to updateGroup ', error.payload.id);
	 					groupService.updateGroup(adminUser, error.payload.id, groupsToAdd[i])
				 			.then(response => {
				 				console.log('updateCommunityGroup succeded in updateGroup ', response);
				 			})
				 			.catch(error => {
				 				console.log('updateCommunityGroup Error trying to updateGroup ', error.payload.id, error);
				 			})

	 					groupsToAdd[i].id = error.payload.id;
	 				} else {
	 					console.log('updateCommunityGroup Error trying to updateGroup Id not found');
	 				}
	 				createGroup(++i); //may need to move this into above if/else branches, making processing synchronous
	 			})
	 	};
	 	createGroup(0);
	 };

	 communityService.createCommunity(adminUser, sampleData.community)
		 .then( response => {
		 	createGoups(response.payload.id);
		 })
		 .catch(error => {
		 	//console.log('createCommunity', error);
		 	if (error.payload && error.payload.id)
		 		createGoups(error.payload.id);
		 		/*setTimeout(()=> {
			 		createGoups(error.payload.id);
			 	}, 2000)*/

		 });

};

exports.doOnboard  = doOnboard;

